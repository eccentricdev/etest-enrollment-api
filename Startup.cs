using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Any;
using Microsoft.OpenApi.Models;
using SeventyOneDev.Core;
using SeventyOneDev.Core.Context;
using SeventyOneDev.Core.Security.Models;
using SeventyOneDev.Utilities;
using etest_enrollment_api.Databases;
using etest_enrollment_api.Modules.Commons.Configures;
using etest_enrollment_api.Modules.Commons.Initials;
using etest_enrollment_api.Modules.Commons.Models;
using etest_enrollment_api.Modules.Enrollments;
using etest_enrollment_api.Modules.Enrollments.Configures;
using etest_enrollment_api.Modules.Enrollments.Models;
using Swashbuckle.AspNetCore.SwaggerUI;


namespace etest_enrollment_api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
          services.AddDbContext<Db>(options =>
            {
              options.UseNpgsql(Configuration["DatabaseConnections:postgres"]);
            },
            ServiceLifetime.Transient
          );

          services.AddDbContext<AdminDb>(options =>
            {
              options.UseNpgsql(Configuration["DatabaseConnections:postgres1"]);
            },
            ServiceLifetime.Transient
          );
          
          services.AddDbContext<TestDb>(
            options =>
            {
              options.UseNpgsql(Configuration["DatabaseConnections:Postgres_test"]);
            },
            ServiceLifetime.Transient);

          services.AddDbContext<SeventyOneDevCoreContext>(
            options =>
            {
              options.UseNpgsql(Configuration["DatabaseConnections:postgres"], b => b.MigrationsAssembly("etest_enrollment_api"));
            }, ServiceLifetime.Transient);

          services.AddHttpClient("open_id", client =>
          {
            client.BaseAddress = new Uri(Configuration["OpenID:authority"]);
            client.DefaultRequestHeaders.Add("Accept", "application/json");
            client.Timeout=TimeSpan.FromSeconds(10);

          });
          
          services.AddHttpClient("report", client =>
          {
            client.BaseAddress = new Uri("https://ru.71dev.com");
            // client.BaseAddress = new Uri("http://10.10.100.105");
            client.DefaultRequestHeaders.Add("Accept", "application/json");
            client.Timeout = TimeSpan.FromSeconds(120);

          });
          var reportSp = ServicePointManager.FindServicePoint(new Uri("https://ru.71dev.com"));
          // var reportSp = ServicePointManager.FindServicePoint(new Uri("http://10.10.100.105"));
          reportSp.ConnectionLimit = 200;

            services.AddControllers().AddJsonOptions(options=>
              options.JsonSerializerOptions.Converters.Add(new TimeSpanToStringConverter()));
            services.AddCors();

            services.AddCommonServices();
            services.AddEnrollmentServices();
            services.Configure<Setting>(Configuration.GetSection("SeventyOneDev.Core"));
            services.Configure<application_setting>(Configuration.GetSection("application_setting"));
            services.AddSecurities();
            services.AddDocuments();
            services.AddNotifications();
            
            var mappingConfig = new MapperConfiguration(mc =>
            {
              mc.AddProfile(new TestingResultMap());
            });

            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);
            
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("common", new OpenApiInfo { Title = "Common API", Version = "v1" });
                c.SwaggerDoc("enrollment", new OpenApiInfo { Title = "Enrollment API", Version = "v1" });
                c.SwaggerDoc("security", new OpenApiInfo { Title = "Security API", Version = "v1" });
                c.SwaggerDoc("reports", new OpenApiInfo { Title = "Reports API", Version = "v1" });
                c.SwaggerDoc("etest_paypal_integrate", new OpenApiInfo { Title = "etest_paypal_integrate", Version = "v1" });
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath, includeControllerXmlComments: true);
                c.MapType(typeof(TimeSpan?), () => new OpenApiSchema
                {
                  Type = "string",
                  Example = new OpenApiString("09:30:00")
                });
                c.MapType(typeof(TimeSpan), () => new OpenApiSchema
                {
                  Type = "string",
                  Example = new OpenApiString("09:30:00")
                });
                c.EnableAnnotations();
            });
            services.AddAuthentication("Zerpa")//JwtBearerDefaults.AuthenticationScheme)

              .AddJwtBearer("OpenID",options =>
              {
                options.Authority = Configuration["OpenID:Authority"];
                options.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters
                {
                  ValidateIssuer = true,
                  ValidateAudience = false

                };

              })
              .AddJwtBearer("Zerpa", options =>
              {
                options.RequireHttpsMetadata = false;
                options.SaveToken = true;
                options.TokenValidationParameters = new TokenValidationParameters
                {
                  ValidateIssuer = true,
                  ValidIssuer = Configuration["jwt_config:issuer"],
                  ValidateIssuerSigningKey = true,
                  IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Configuration["jwt_config:secret"])),
                  ValidAudience = Configuration["jwt_config:audience"],
                  ValidateAudience = true,
                  ValidateLifetime = true,
                  ClockSkew = TimeSpan.FromMinutes(1)
                };
              });
            services.AddAuthorization(options =>
            {
              options.DefaultPolicy = new AuthorizationPolicyBuilder().RequireAuthenticatedUser()
                .AddAuthenticationSchemes("Zerpa", "OpenID").Build();
              options.AddPolicy("BILL-READ", policy =>
              {
                policy.RequireAuthenticatedUser();
                policy.AddAuthenticationSchemes("OpenID");
                policy.RequireClaim("scope", "bill.read");
              });
            });

            var setting = new Settings(){
            
              smtp_server = Configuration["Settings:SMTPServer"],
              smtp_user_name = Configuration["Settings:SMTPUserName"],
              smtp_password = Configuration["Settings:SMTPPassword"],
              smtp_port = int.Parse(Configuration["Settings:SMTPPort"]),
              smtp_use_ssl = bool.Parse(Configuration["Settings:SMTPUseSSL"])
            };
            services.AddSingleton<Settings>(setting);

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
          InitializeDatabase(app);
          // if (env.IsDevelopment())
          // {
          //     app.UseDeveloperExceptionPage();
          // }
          //
          // app.UseHttpsRedirection();
          app.UseForwardedHeaders(new ForwardedHeadersOptions
          {
            ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto,
            KnownNetworks =
            {
              new IPNetwork(IPAddress.Any, 0)
            }
          });
          app.UseCors(builder => builder
            .AllowAnyOrigin()
            .AllowAnyMethod()
            .AllowAnyHeader());


          
                app.UseDeveloperExceptionPage();
                app.UseSwagger(c => {
                  c.RouteTemplate = "swagger/{documentName}/swagger.json";
                  c.PreSerializeFilters.Add((swaggerDoc, httpReq) =>
                  {
                    if (!httpReq.Headers.ContainsKey("X-Forwarded-Host")) return;

                    var serverUrl = $"{httpReq.Headers["X-Scheme"]}://" +
                                    $"{httpReq.Headers["X-Forwarded-Host"]}" +
                                    $"{httpReq.Headers["X-Forwarded-Prefix"]}";
                    swaggerDoc.Servers = new List<OpenApiServer> { new OpenApiServer { Url =serverUrl}};// $"{httpReq.Scheme}://{httpReq.Host.Value}{swaggerBasePath}" } };
                  });
                });
                app.UseSwaggerUI(c =>
                {
                  c.SwaggerEndpoint("common/swagger.json", "Common API v1");
                  c.SwaggerEndpoint("enrollment/swagger.json", "Enrollment API v1");
                  c.SwaggerEndpoint("security/swagger.json", "Security API v1");
                  c.SwaggerEndpoint("reports/swagger.json", "Report API v1");
                  c.SwaggerEndpoint("etest_paypal_integrate/swagger.json", "etest_paypal_integrate v1");
                  c.DefaultModelExpandDepth(0);
                  c.DefaultModelsExpandDepth(-1);
                  c.DocExpansion(DocExpansion.None);
                });
            

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
        private void InitializeDatabase(IApplicationBuilder app)
        {
          using var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>()?.CreateScope();
          if (serviceScope == null) throw new Exception("Cannot create IServiceScopeFactory");
          var coreContext = serviceScope.ServiceProvider.GetRequiredService<SeventyOneDevCoreContext>();
          var dbContext = serviceScope.ServiceProvider.GetRequiredService<Db>();
          var setting = serviceScope.ServiceProvider.GetRequiredService<IOptions<Setting>>();
          CommonInit.Run(dbContext);
          CreateViews(coreContext.Database,setting.Value.default_schema);
        }

        private static void CreateViews(DatabaseFacade db, string schema)
        {
          //InjectView(db, "app_data.sql", "v_app_data",schema);
          var assembly = Assembly.GetAssembly(typeof(SeventyOneDev.Core.ServiceCollectionExtension));
          string[] names = assembly.GetManifestResourceNames();
          foreach (var name in names)
          {
            var resource = assembly.GetManifestResourceStream(name);
            var sqlQuery = new StreamReader(resource).ReadToEnd();
            //we always delete the old view, in case the sql query has changed
            //db.ExecuteSqlRaw($"IF OBJECT_ID('{viewName}') IS NOT NULL BEGIN DROP VIEW {viewName} END");
            //creating a view based on the sql query
            db.ExecuteSqlRaw(string.Format(sqlQuery, schema));
          }
        }
    }
}
