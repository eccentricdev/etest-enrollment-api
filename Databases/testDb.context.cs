using System;
using System.Linq;
using etest_enrollment_api.Modules.Enrollments.Databases.TestModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using SeventyOneDev.Core.Security.Models;

namespace SeventyOneDev.Utilities
{
  public partial class TestDb : DbContext
  {
    private string _schema = "test";
    //private readonly ILogger<RSUContext> _logger;
    
    public DbSet<t_tests> t_tests { get; set; }
        
    public DbSet<v_tests> v_tests { get; set; }

    public TestDb(DbContextOptions<TestDb> options,IOptions<Setting> settings) : base(options)
    {
      _schema = settings.Value.default_schema;
    }
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
      // optionsBuilder.UseLoggerFactory(_logger)  //tie-up DbContext with LoggerFactory object
      //   .EnableSensitiveDataLogging();
    }

    //public DbSet<v_document_no_template> v_document_no_template { get; set; }

    protected override void OnModelCreating(ModelBuilder builder)
    {
      base.OnModelCreating(builder);
      foreach (var property in builder.Model.GetEntityTypes()
        .SelectMany(t => t.GetProperties())
        .Where(p => p.ClrType == typeof(string)))
      {
        if (property.Name.EndsWith("_code") && (property.GetMaxLength() == null))
        {
          property.SetMaxLength(50);
        }
        else if (property.Name.EndsWith("_name_th") && (property.GetMaxLength() == null))
        {
          property.SetMaxLength(200);
        }
        else if (property.Name.EndsWith("_name_en") && (property.GetMaxLength() == null))
        {
          property.SetMaxLength(200);
        }
        else if (property.Name.EndsWith("description") && (property.GetMaxLength() == null))
        {
          property.SetMaxLength(500);
        }
        else if (property.Name.EndsWith("remark") && (property.GetMaxLength() == null))
        {
          property.SetMaxLength(1000);
        }
        else if (property.Name.EndsWith("_url") && (property.GetMaxLength() == null))
        {
          property.SetMaxLength(500);
        }
        else if (property.Name.EndsWith("image_name") && (property.GetMaxLength() == null))
        {
          property.SetMaxLength(500);
        }
        else if (property.Name.EndsWith("_name") && (property.GetMaxLength() == null))
        {
          property.SetMaxLength(200);
        }
        else if (property.Name.EndsWith("_no") && (property.GetMaxLength() == null))
        {
          property.SetMaxLength(50);
        }
        else if (property.Name.EndsWith("_address") && (property.GetMaxLength() == null))
        {
          property.SetMaxLength(1000);
        }
        else if ((property.Name=="created_by" || property.Name=="updated_by" || property.Name=="deleted_by") && (property.GetMaxLength() == null))
        {
          property.SetMaxLength(200);
        }
        else if (property.Name.EndsWith("email") && (property.GetMaxLength() == null))
        {
          property.SetMaxLength(200);
        }

      }
      foreach (var property in builder.Model.GetEntityTypes()
        .SelectMany(t => t.GetProperties())
        .Where(p => p.ClrType == typeof(decimal) || p.ClrType == typeof(decimal?)))
      {
        if (property.Name.EndsWith("amount") || property.Name.Contains("_price") || property.Name.EndsWith("quantity"))
        {
          property.SetPrecision(18);
          property.SetScale(4);
        }
        else if (property.Name.EndsWith("percent"))
        {
          property.SetPrecision(5);
          property.SetScale(2);
        }

      }
      foreach (var property in builder.Model.GetEntityTypes()
        .SelectMany(t => t.GetProperties())
        .Where(p =>p.ClrType == typeof(DateTime?)))
      {
        if (property.Name.EndsWith("_date"))
        {
          property.SetColumnType("date");
        }
      }
      
      builder.Entity<t_tests>().ToTable("tests", "test"); 
      builder.Entity<v_tests>().ToView("v_tests", "test"); 
    }

  }
}
