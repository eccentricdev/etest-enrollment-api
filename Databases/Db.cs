using System;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using SeventyOneDev.Core.Security.Models;
using etest_enrollment_api.Modules.Commons.Models;

namespace etest_enrollment_api.Databases
{
  public partial class Db : DbContext
  {
    private string _schema;
    //private readonly ILogger<RSUContext> _logger;
    public Db(DbContextOptions<Db> options, IOptions<Setting> settings) : base(options)
    {
      _schema = "public2" ;
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
      // optionsBuilder.UseLoggerFactory(_logger)  //tie-up DbContext with LoggerFactory object
      //   .EnableSensitiveDataLogging();
    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
      base.OnModelCreating(builder);
      foreach (var property in builder.Model.GetEntityTypes()
        .SelectMany(t => t.GetProperties())
        .Where(p => p.ClrType == typeof(string)))
      {
        if (property.Name.EndsWith("_code") && (property.GetMaxLength() == null))
        {
          property.SetMaxLength(50);
        }
        else if (property.Name.EndsWith("_name_th") && (property.GetMaxLength() == null))
        {
          property.SetMaxLength(200);
        }
        else if (property.Name.EndsWith("_name_en") && (property.GetMaxLength() == null))
        {
          property.SetMaxLength(200);
        }
        else if (property.Name.EndsWith("description") && (property.GetMaxLength() == null))
        {
          property.SetMaxLength(500);
        }
        else if (property.Name.EndsWith("remark") && (property.GetMaxLength() == null))
        {
          property.SetMaxLength(1000);
        }
        else if (property.Name.EndsWith("_url") && (property.GetMaxLength() == null))
        {
          property.SetMaxLength(500);
        }
        else if (property.Name.EndsWith("image_name") && (property.GetMaxLength() == null))
        {
          property.SetMaxLength(500);
        }
        else if (property.Name.EndsWith("_name") && (property.GetMaxLength() == null))
        {
          property.SetMaxLength(200);
        }
        else if (property.Name.EndsWith("_no") && (property.GetMaxLength() == null))
        {
          property.SetMaxLength(50);
        }
        else if (property.Name.EndsWith("_address") && (property.GetMaxLength() == null))
        {
          property.SetMaxLength(1000);
        }
        else if ((property.Name == "created_by" || property.Name == "updated_by" || property.Name == "deleted_by") &&
                 (property.GetMaxLength() == null))
        {
          property.SetMaxLength(200);
        }

      }

      foreach (var property in builder.Model.GetEntityTypes()
        .SelectMany(t => t.GetProperties())
        .Where(p => p.ClrType == typeof(decimal) || p.ClrType == typeof(decimal?)))
      {
        if (property.Name.EndsWith("amount") || property.Name.Contains("_price") || property.Name.EndsWith("quantity"))
        {
          property.SetPrecision(18);
          property.SetScale(4);
        }
        else if (property.Name.EndsWith("percent"))
        {
          property.SetPrecision(5);
          property.SetScale(2);
        }

      }

      foreach (var property in builder.Model.GetEntityTypes()
        .SelectMany(t => t.GetProperties())
        .Where(p => p.ClrType == typeof(DateTime?)))
      {
        if (property.Name.EndsWith("_date"))
        {
          property.SetColumnType("date");
        }
      }
      OnCommonModelCreating(builder,_schema);
      OnEnrollmentModelCreating(builder,_schema);
    }
    public Task<int> SaveChangesAsync(ClaimsIdentity claimsIdentity,CancellationToken cancellationToken = default)
    {
      var userId = claimsIdentity.FindFirst("user_id")?.Value;
      var entries = ChangeTracker
        .Entries()
        .Where(e => e.Entity is base_table && (
          e.State == EntityState.Added
          || e.State == EntityState.Modified));

      foreach (var entityEntry in entries)
      {
        ((base_table)entityEntry.Entity).updated_datetime = DateTime.Now;
        ((base_table)entityEntry.Entity).updated_by = userId;
        if (entityEntry.State == EntityState.Added)
        {
          ((base_table)entityEntry.Entity).created_datetime = DateTime.Now;
          ((base_table)entityEntry.Entity).created_by = userId;
        }
      }

      return base.SaveChangesAsync(cancellationToken);
    }
  }
}
