﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace etest_enrollment_api.Migrations
{
    public partial class UpdateTestingRegister : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "payment_id",
                schema: "public",
                table: "user_testing_register");

            migrationBuilder.AddColumn<Guid>(
                name: "branch_uid",
                schema: "public",
                table: "user_testing_subject_selected",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "testing_field_uid",
                schema: "public",
                table: "user_testing_subject_selected",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "testing_room_name_th",
                schema: "public",
                table: "user_testing_subject_selected",
                type: "character varying(200)",
                maxLength: 200,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "payment_code",
                schema: "public",
                table: "user_testing_register",
                type: "character varying(50)",
                maxLength: 50,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "branch_uid",
                schema: "public",
                table: "user_testing_subject_selected");

            migrationBuilder.DropColumn(
                name: "testing_field_uid",
                schema: "public",
                table: "user_testing_subject_selected");

            migrationBuilder.DropColumn(
                name: "testing_room_name_th",
                schema: "public",
                table: "user_testing_subject_selected");

            migrationBuilder.DropColumn(
                name: "payment_code",
                schema: "public",
                table: "user_testing_register");

            migrationBuilder.AddColumn<int>(
                name: "payment_id",
                schema: "public",
                table: "user_testing_register",
                type: "integer",
                nullable: true);
        }
    }
}
