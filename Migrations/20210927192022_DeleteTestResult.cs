﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace etest_enrollment_api.Migrations
{
    public partial class DeleteTestResult : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "testing_result",
                schema: "public");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "testing_result",
                schema: "public",
                columns: table => new
                {
                    testing_result_uid = table.Column<Guid>(type: "uuid", nullable: false),
                    academic_semester_uid = table.Column<Guid>(type: "uuid", nullable: true),
                    academic_year_uid = table.Column<Guid>(type: "uuid", nullable: true),
                    created_by = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    credit = table.Column<string>(type: "text", nullable: true),
                    result = table.Column<string>(type: "text", nullable: true),
                    status_id = table.Column<int>(type: "integer", nullable: true),
                    subject_code = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true),
                    subject_name_th = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    updated_by = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_testing_result", x => x.testing_result_uid);
                });
        }
    }
}
