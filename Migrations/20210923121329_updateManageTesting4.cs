﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace etest_enrollment_api.Migrations
{
    public partial class updateManageTesting4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "seat_status",
                schema: "public",
                table: "user_testing_register");

            migrationBuilder.DropColumn(
                name: "seat_status",
                schema: "public",
                table: "manage_testing");

            migrationBuilder.AddColumn<short>(
                name: "seat_status_id",
                schema: "public",
                table: "user_testing_register",
                type: "smallint",
                nullable: false,
                defaultValue: (short)0);

            migrationBuilder.AddColumn<short>(
                name: "seat_status_id",
                schema: "public",
                table: "manage_testing",
                type: "smallint",
                nullable: false,
                defaultValue: (short)0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "seat_status_id",
                schema: "public",
                table: "user_testing_register");

            migrationBuilder.DropColumn(
                name: "seat_status_id",
                schema: "public",
                table: "manage_testing");

            migrationBuilder.AddColumn<string>(
                name: "seat_status",
                schema: "public",
                table: "user_testing_register",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "seat_status",
                schema: "public",
                table: "manage_testing",
                type: "text",
                nullable: true);
        }
    }
}
