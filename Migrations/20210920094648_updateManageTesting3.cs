﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace etest_enrollment_api.Migrations
{
    public partial class updateManageTesting3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "branch_uid",
                schema: "public",
                table: "manage_testing",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "testing_center_uid",
                schema: "public",
                table: "manage_testing",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "testing_field_uid",
                schema: "public",
                table: "manage_testing",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "testing_information_uid",
                schema: "public",
                table: "manage_testing",
                type: "uuid",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "branch_uid",
                schema: "public",
                table: "manage_testing");

            migrationBuilder.DropColumn(
                name: "testing_center_uid",
                schema: "public",
                table: "manage_testing");

            migrationBuilder.DropColumn(
                name: "testing_field_uid",
                schema: "public",
                table: "manage_testing");

            migrationBuilder.DropColumn(
                name: "testing_information_uid",
                schema: "public",
                table: "manage_testing");
        }
    }
}
