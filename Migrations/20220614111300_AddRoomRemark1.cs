﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace etest_enrollment_api.Migrations
{
    public partial class AddRoomRemark1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "room_remark",
                schema: "public2",
                table: "user_testing_register");

            migrationBuilder.AddColumn<string>(
                name: "room_remark",
                schema: "public2",
                table: "user_testing_subject_selected",
                type: "character varying(1000)",
                maxLength: 1000,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "room_remark",
                schema: "public2",
                table: "user_testing_subject_selected");

            migrationBuilder.AddColumn<string>(
                name: "room_remark",
                schema: "public2",
                table: "user_testing_register",
                type: "character varying(1000)",
                maxLength: 1000,
                nullable: true);
        }
    }
}
