﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace etest_enrollment_api.Migrations
{
    public partial class UpdateUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "photo1_name",
                schema: "public2",
                table: "user",
                type: "character varying(200)",
                maxLength: 200,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "photo1_url",
                schema: "public2",
                table: "user",
                type: "character varying(500)",
                maxLength: 500,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "photo2_name",
                schema: "public2",
                table: "user",
                type: "character varying(200)",
                maxLength: 200,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "photo2_url",
                schema: "public2",
                table: "user",
                type: "character varying(500)",
                maxLength: 500,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "photo1_name",
                schema: "public2",
                table: "user");

            migrationBuilder.DropColumn(
                name: "photo1_url",
                schema: "public2",
                table: "user");

            migrationBuilder.DropColumn(
                name: "photo2_name",
                schema: "public2",
                table: "user");

            migrationBuilder.DropColumn(
                name: "photo2_url",
                schema: "public2",
                table: "user");
        }
    }
}
