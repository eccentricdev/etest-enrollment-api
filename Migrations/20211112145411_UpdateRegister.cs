﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace etest_enrollment_api.Migrations
{
    public partial class UpdateRegister : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ref1",
                schema: "public",
                table: "user_testing_register",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ref2",
                schema: "public",
                table: "user_testing_register",
                type: "text",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ref1",
                schema: "public",
                table: "user_testing_register");

            migrationBuilder.DropColumn(
                name: "ref2",
                schema: "public",
                table: "user_testing_register");
        }
    }
}
