﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace etest_enrollment_api.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "public");

            migrationBuilder.CreateTable(
                name: "candidate_type",
                schema: "public",
                columns: table => new
                {
                    candidate_type_uid = table.Column<Guid>(type: "uuid", nullable: false),
                    status_id = table.Column<int>(type: "integer", nullable: true),
                    created_by = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    updated_by = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    candidate_type_code = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true),
                    candidate_type_name_th = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    candidate_type_name_en = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_candidate_type", x => x.candidate_type_uid);
                });

            migrationBuilder.CreateTable(
                name: "manage_testing",
                schema: "public",
                columns: table => new
                {
                    manage_testing_uid = table.Column<Guid>(type: "uuid", nullable: false),
                    status_id = table.Column<int>(type: "integer", nullable: true),
                    created_by = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    updated_by = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    user_testing_register_uid = table.Column<Guid>(type: "uuid", nullable: true),
                    subject_uid = table.Column<Guid>(type: "uuid", nullable: true),
                    testing_datetime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    period_id = table.Column<short>(type: "smallint", nullable: true),
                    new_testing_datetime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    new_period_id = table.Column<short>(type: "smallint", nullable: true),
                    reason = table.Column<string>(type: "text", nullable: true),
                    manage_testing_file = table.Column<string>(type: "text", nullable: true),
                    is_cancel = table.Column<bool>(type: "boolean", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_manage_testing", x => x.manage_testing_uid);
                });

            migrationBuilder.CreateTable(
                name: "status",
                schema: "public",
                columns: table => new
                {
                    status_id = table.Column<short>(type: "smallint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    status_code = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true),
                    status_name_th = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    status_name_en = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    created_by = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    updated_by = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_status", x => x.status_id);
                });

            migrationBuilder.CreateTable(
                name: "testing_result",
                schema: "public",
                columns: table => new
                {
                    testing_result_uid = table.Column<Guid>(type: "uuid", nullable: false),
                    status_id = table.Column<int>(type: "integer", nullable: true),
                    created_by = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    updated_by = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    academic_year_uid = table.Column<Guid>(type: "uuid", nullable: true),
                    academic_semester_uid = table.Column<Guid>(type: "uuid", nullable: true),
                    subject_code = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true),
                    subject_name_th = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    credit = table.Column<string>(type: "text", nullable: true),
                    result = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_testing_result", x => x.testing_result_uid);
                });

            migrationBuilder.CreateTable(
                name: "user",
                schema: "public",
                columns: table => new
                {
                    user_uid = table.Column<Guid>(type: "uuid", nullable: false),
                    status_id = table.Column<int>(type: "integer", nullable: true),
                    created_by = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    updated_by = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    candidate_type_uid = table.Column<Guid>(type: "uuid", nullable: true),
                    examinee_card = table.Column<string>(type: "text", nullable: true),
                    student_code = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true),
                    full_name = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    faculty_uid = table.Column<Guid>(type: "uuid", nullable: true),
                    department_uid = table.Column<Guid>(type: "uuid", nullable: true),
                    mobile_phone = table.Column<string>(type: "text", nullable: true),
                    email = table.Column<string>(type: "text", nullable: true),
                    password = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_user", x => x.user_uid);
                });

            migrationBuilder.CreateTable(
                name: "user_testing_register",
                schema: "public",
                columns: table => new
                {
                    user_testing_register_uid = table.Column<Guid>(type: "uuid", nullable: false),
                    status_id = table.Column<int>(type: "integer", nullable: true),
                    created_by = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    updated_by = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    user_uid = table.Column<Guid>(type: "uuid", nullable: true),
                    examinee_card = table.Column<string>(type: "text", nullable: true),
                    examinee_type_id = table.Column<short>(type: "smallint", nullable: true),
                    examinee_mobile = table.Column<string>(type: "text", nullable: true),
                    testing_information_uid = table.Column<Guid>(type: "uuid", nullable: true),
                    payment_id = table.Column<int>(type: "integer", nullable: true),
                    testing_fee = table.Column<string>(type: "text", nullable: true),
                    seat_status = table.Column<string>(type: "text", nullable: true),
                    is_cancel = table.Column<bool>(type: "boolean", nullable: true),
                    is_pay_later = table.Column<bool>(type: "boolean", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_user_testing_register", x => x.user_testing_register_uid);
                });

            migrationBuilder.CreateTable(
                name: "user_testing_subject_selected",
                schema: "public",
                columns: table => new
                {
                    user_testing_subject_selected_uid = table.Column<Guid>(type: "uuid", nullable: false),
                    status_id = table.Column<int>(type: "integer", nullable: true),
                    created_by = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    created_datetime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    updated_by = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    updated_datetime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    subject_uid = table.Column<Guid>(type: "uuid", nullable: true),
                    testing_datetime = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    period_id = table.Column<short>(type: "smallint", nullable: true),
                    testing_center_uid = table.Column<Guid>(type: "uuid", nullable: true),
                    testing_seat = table.Column<string>(type: "text", nullable: true),
                    testing_room_name = table.Column<string>(type: "character varying(200)", maxLength: 200, nullable: true),
                    user_testing_register_uid = table.Column<Guid>(type: "uuid", nullable: true),
                    is_cancel = table.Column<bool>(type: "boolean", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_user_testing_subject_selected", x => x.user_testing_subject_selected_uid);
                    table.ForeignKey(
                        name: "FK_user_testing_subject_selected_user_testing_register_user_te~",
                        column: x => x.user_testing_register_uid,
                        principalSchema: "public",
                        principalTable: "user_testing_register",
                        principalColumn: "user_testing_register_uid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_user_testing_subject_selected_user_testing_register_uid",
                schema: "public",
                table: "user_testing_subject_selected",
                column: "user_testing_register_uid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "candidate_type",
                schema: "public");

            migrationBuilder.DropTable(
                name: "manage_testing",
                schema: "public");

            migrationBuilder.DropTable(
                name: "status",
                schema: "public");

            migrationBuilder.DropTable(
                name: "testing_result",
                schema: "public");

            migrationBuilder.DropTable(
                name: "user",
                schema: "public");

            migrationBuilder.DropTable(
                name: "user_testing_subject_selected",
                schema: "public");

            migrationBuilder.DropTable(
                name: "user_testing_register",
                schema: "public");
        }
    }
}
