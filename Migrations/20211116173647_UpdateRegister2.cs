﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace etest_enrollment_api.Migrations
{
    public partial class UpdateRegister2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "testing_room_name_th",
                schema: "public",
                table: "user_testing_subject_selected");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "testing_room_name_th",
                schema: "public",
                table: "user_testing_subject_selected",
                type: "character varying(200)",
                maxLength: 200,
                nullable: true);
        }
    }
}
