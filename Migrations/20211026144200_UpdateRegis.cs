﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace etest_enrollment_api.Migrations
{
    public partial class UpdateRegis : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "payment_datetime",
                schema: "public",
                table: "user_testing_register",
                type: "timestamp without time zone",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "photo1_name",
                schema: "public",
                table: "user_testing_register",
                type: "character varying(200)",
                maxLength: 200,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "photo1_url",
                schema: "public",
                table: "user_testing_register",
                type: "character varying(500)",
                maxLength: 500,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "photo2_name",
                schema: "public",
                table: "user_testing_register",
                type: "character varying(200)",
                maxLength: 200,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "photo2_url",
                schema: "public",
                table: "user_testing_register",
                type: "character varying(500)",
                maxLength: 500,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "payment_datetime",
                schema: "public",
                table: "user_testing_register");

            migrationBuilder.DropColumn(
                name: "photo1_name",
                schema: "public",
                table: "user_testing_register");

            migrationBuilder.DropColumn(
                name: "photo1_url",
                schema: "public",
                table: "user_testing_register");

            migrationBuilder.DropColumn(
                name: "photo2_name",
                schema: "public",
                table: "user_testing_register");

            migrationBuilder.DropColumn(
                name: "photo2_url",
                schema: "public",
                table: "user_testing_register");
        }
    }
}
