﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace etest_enrollment_api.Migrations
{
    public partial class UpdateTestingInformation3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            // migrationBuilder.EnsureSchema(
            //     name: "public2");
            //
            // migrationBuilder.RenameTable(
            //     name: "user_testing_subject_selected",
            //     schema: "public",
            //     newName: "user_testing_subject_selected",
            //     newSchema: "public2");
            //
            // migrationBuilder.RenameTable(
            //     name: "user_testing_register",
            //     schema: "public",
            //     newName: "user_testing_register",
            //     newSchema: "public2");
            //
            // migrationBuilder.RenameTable(
            //     name: "user",
            //     schema: "public",
            //     newName: "user",
            //     newSchema: "public2");
            //
            // migrationBuilder.RenameTable(
            //     name: "status",
            //     schema: "public",
            //     newName: "status",
            //     newSchema: "public2");
            //
            // migrationBuilder.RenameTable(
            //     name: "manage_testing",
            //     schema: "public",
            //     newName: "manage_testing",
            //     newSchema: "public2");
            //
            // migrationBuilder.RenameTable(
            //     name: "candidate_type",
            //     schema: "public",
            //     newName: "candidate_type",
            //     newSchema: "public2");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "public");

            migrationBuilder.RenameTable(
                name: "user_testing_subject_selected",
                schema: "public2",
                newName: "user_testing_subject_selected",
                newSchema: "public");

            migrationBuilder.RenameTable(
                name: "user_testing_register",
                schema: "public2",
                newName: "user_testing_register",
                newSchema: "public");

            migrationBuilder.RenameTable(
                name: "user",
                schema: "public2",
                newName: "user",
                newSchema: "public");

            migrationBuilder.RenameTable(
                name: "status",
                schema: "public2",
                newName: "status",
                newSchema: "public");

            migrationBuilder.RenameTable(
                name: "manage_testing",
                schema: "public2",
                newName: "manage_testing",
                newSchema: "public");

            migrationBuilder.RenameTable(
                name: "candidate_type",
                schema: "public2",
                newName: "candidate_type",
                newSchema: "public");
        }
    }
}
