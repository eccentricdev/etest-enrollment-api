﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace etest_enrollment_api.Migrations.SeventyOneDevCore
{
    public partial class UpdateSevenOnedev : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_app_user_role",
                schema: "public",
                table: "app_user_role");

            migrationBuilder.DropPrimaryKey(
                name: "PK_app_security",
                schema: "public",
                table: "app_security");

            migrationBuilder.AlterColumn<int>(
                name: "app_role_id",
                schema: "public",
                table: "app_user_role",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<int>(
                name: "app_user_id",
                schema: "public",
                table: "app_user_role",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AddColumn<int>(
                name: "app_user_role_id",
                schema: "public",
                table: "app_user_role",
                type: "integer",
                nullable: false,
                defaultValue: 0)
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

            migrationBuilder.AlterColumn<int>(
                name: "app_object_id",
                schema: "public",
                table: "app_security",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<int>(
                name: "app_permission_id",
                schema: "public",
                table: "app_security",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AlterColumn<int>(
                name: "app_role_id",
                schema: "public",
                table: "app_security",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AddColumn<int>(
                name: "app_security_id",
                schema: "public",
                table: "app_security",
                type: "integer",
                nullable: false,
                defaultValue: 0)
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

            migrationBuilder.AddPrimaryKey(
                name: "PK_app_user_role",
                schema: "public",
                table: "app_user_role",
                column: "app_user_role_id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_app_security",
                schema: "public",
                table: "app_security",
                column: "app_security_id");

            migrationBuilder.CreateIndex(
                name: "IX_app_user_role_app_user_id",
                schema: "public",
                table: "app_user_role",
                column: "app_user_id");

            migrationBuilder.CreateIndex(
                name: "IX_app_security_app_role_id",
                schema: "public",
                table: "app_security",
                column: "app_role_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_app_user_role",
                schema: "public",
                table: "app_user_role");

            migrationBuilder.DropIndex(
                name: "IX_app_user_role_app_user_id",
                schema: "public",
                table: "app_user_role");

            migrationBuilder.DropPrimaryKey(
                name: "PK_app_security",
                schema: "public",
                table: "app_security");

            migrationBuilder.DropIndex(
                name: "IX_app_security_app_role_id",
                schema: "public",
                table: "app_security");

            migrationBuilder.DropColumn(
                name: "app_user_role_id",
                schema: "public",
                table: "app_user_role");

            migrationBuilder.DropColumn(
                name: "app_security_id",
                schema: "public",
                table: "app_security");

            migrationBuilder.AlterColumn<int>(
                name: "app_user_id",
                schema: "public",
                table: "app_user_role",
                type: "integer",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "app_role_id",
                schema: "public",
                table: "app_user_role",
                type: "integer",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "app_role_id",
                schema: "public",
                table: "app_security",
                type: "integer",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "app_permission_id",
                schema: "public",
                table: "app_security",
                type: "integer",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "app_object_id",
                schema: "public",
                table: "app_security",
                type: "integer",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_app_user_role",
                schema: "public",
                table: "app_user_role",
                columns: new[] { "app_user_id", "app_role_id" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_app_security",
                schema: "public",
                table: "app_security",
                columns: new[] { "app_role_id", "app_permission_id", "app_object_id" });
        }
    }
}
