﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace etest_enrollment_api.Migrations
{
    public partial class UpdateManageTesting : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "user_testing_subject_selected_uid",
                schema: "public",
                table: "manage_testing",
                type: "uuid",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "user_testing_subject_selected_uid",
                schema: "public",
                table: "manage_testing");
        }
    }
}
