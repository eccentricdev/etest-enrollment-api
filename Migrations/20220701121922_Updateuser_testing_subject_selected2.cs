﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace etest_enrollment_api.Migrations
{
    public partial class Updateuser_testing_subject_selected2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "testing_subject_selected_approved",
                schema: "public2",
                table: "user_testing_subject_selected",
                newName: "testing_register_approved");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "testing_register_approved",
                schema: "public2",
                table: "user_testing_subject_selected",
                newName: "testing_subject_selected_approved");
        }
    }
}
