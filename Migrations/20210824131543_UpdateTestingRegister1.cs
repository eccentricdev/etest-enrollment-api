﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace etest_enrollment_api.Migrations
{
    public partial class UpdateTestingRegister1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "examinee_type_id",
                schema: "public",
                table: "user_testing_register");

            migrationBuilder.AddColumn<Guid>(
                name: "candidate_type_uid",
                schema: "public",
                table: "user_testing_register",
                type: "uuid",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "candidate_type_uid",
                schema: "public",
                table: "user_testing_register");

            migrationBuilder.AddColumn<short>(
                name: "examinee_type_id",
                schema: "public",
                table: "user_testing_register",
                type: "smallint",
                nullable: true);
        }
    }
}
