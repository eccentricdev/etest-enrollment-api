﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace etest_enrollment_api.Migrations
{
    public partial class AddUpdateTestingRegister : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "email",
                schema: "public",
                table: "user_testing_register",
                type: "text",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "email",
                schema: "public",
                table: "user_testing_register");
        }
    }
}
