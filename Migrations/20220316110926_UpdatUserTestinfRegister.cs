﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace etest_enrollment_api.Migrations
{
    public partial class UpdatUserTestinfRegister : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "document_name",
                schema: "public",
                table: "user_testing_register",
                type: "character varying(200)",
                maxLength: 200,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "document_status",
                schema: "public",
                table: "user_testing_register",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "document_url",
                schema: "public",
                table: "user_testing_register",
                type: "character varying(500)",
                maxLength: 500,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "document_name",
                schema: "public",
                table: "user_testing_register");

            migrationBuilder.DropColumn(
                name: "document_status",
                schema: "public",
                table: "user_testing_register");

            migrationBuilder.DropColumn(
                name: "document_url",
                schema: "public",
                table: "user_testing_register");
        }
    }
}
