using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using etest_enrollment_api.Databases;
using etest_enrollment_api.Modules.Reports.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Swashbuckle.AspNetCore.Annotations;

namespace etest_enrollment_api.Modules.Reports.Controllers
{
    [SwaggerTag("สำหรับบัตรประจำตัวผู้เข้าสอบ")]
    public class StudentTestingCardController :Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;
        readonly Db _context;
        readonly AdminDb _adminContext;
        
        public StudentTestingCardController( IHttpClientFactory httpClientFactory, Db context,AdminDb adminContext)
        {
            _httpClientFactory = httpClientFactory;
            _context = context;
            _adminContext = adminContext;
        }

        [HttpGet,
         Route(
             "api/reports/enrollment/print/student_testing_card/{user_testing_register_uid}/{type}")]
        [ApiExplorerSettings(GroupName = "reports")]
        public async Task<ActionResult> GetStudentTestingCard(
            [FromRoute, Required] Guid? user_testing_register_uid,
            [FromRoute, Required] string type
        )
        {
            CultureInfo culture = new CultureInfo("th-TH");
            var regis = await _context.t_user_testing_register.AsNoTracking()
                .Include(i => i.user_testing_subject_selecteds)
                .FirstOrDefaultAsync( w => w.user_testing_register_uid == user_testing_register_uid && w.payment_datetime != null);

            if (regis == null)
            {
                var adminRegis = _adminContext.v_testing_register.AsNoTracking()
                    .Include(i => i.testing_subject_selecteds)
                    .FirstOrDefault(w =>
                        w.testing_register_uid == user_testing_register_uid && w.payment_datetime != null);

                if (adminRegis != null)
                {
                    var user = _context.t_user.AsNoTracking().FirstOrDefault(w => w.user_uid == adminRegis.user_uid);
                    var information = _adminContext.t_testing_information.AsNoTracking()
                        .FirstOrDefault(w => w.testing_information_uid == adminRegis.testing_information_uid);
                    var year = _adminContext.t_academic_year.AsNoTracking()
                        .FirstOrDefault(w => w.academic_year_code == information.academic_year_code);
                    var semester = _adminContext.t_academic_semester.AsNoTracking()
                        .FirstOrDefault(w => w.academic_semester_code == information.academic_semester_code);
                    var canType = _context.t_candidate_type.AsNoTracking()
                        .FirstOrDefault(w => w.candidate_type_uid == user.candidate_type_uid);
                    var facuty = _adminContext.t_faculty.AsNoTracking()
                        .FirstOrDefault(w => w.faculty_uid == user.faculty_uid);

                    var result = new student_testing_card()
                    {
                        exam_round_name = information?.testing_information_name,
                        exam_round_code = information?.testing_information_code,
                        academic_year_name = year?.academic_year_name_th,
                        semester_name = semester?.academic_semester_name_th,
                        student_code = user?.student_code,
                        id_card = adminRegis.examinee_card,
                        student_name = user?.full_name,
                        apply_type = canType?.candidate_type_name_th,
                        faculty = facuty?.faculty_name_th,
                        major = null,
                        agency = null,
                        count_of_exam = adminRegis.testing_subject_selecteds.Count()
                    };

                    result.data = new List<data_student_testing_card>();

                    foreach (var tss in adminRegis.testing_subject_selecteds)
                    {
                        var subject = await _adminContext.t_subject.AsNoTracking()
                            .FirstOrDefaultAsync(w => w.subject_uid == tss.subject_uid);
                        var section = await _adminContext.t_testing_period.AsNoTracking()
                            .FirstOrDefaultAsync(w =>
                                w.testing_information_uid == adminRegis.testing_information_uid &&
                                w.period_id == tss.period_id);
                        var branch = await _adminContext.t_branch.AsNoTracking()
                            .FirstOrDefaultAsync(w => w.branch_uid == tss.branch_uid);
                        var field = await _adminContext.t_testing_field.AsNoTracking().FirstOrDefaultAsync(w =>
                            w.testing_field_uid == tss.testing_field_uid && w.branch_uid == tss.branch_uid);
                        var center = await _adminContext.t_testing_center.AsNoTracking()
                            .FirstOrDefaultAsync(w => w.testing_center_uid == tss.testing_center_uid);
                        result.data.Add(new data_student_testing_card()
                        {
                            subject_id = subject?.subject_code,
                            subject_name = subject?.subject_name_th,
                            exam_date = tss.testing_datetime.Value.ToString("dd MMMM yyyy", culture),
                            section = section != null
                                ? section.period_id + " (" + section.start_time.Value.ToString(@"hh\:mm") + ")"
                                : null,
                            exam_center = branch?.branch_name_th,
                            exam_stadium = field?.testing_field_name_th,
                            exam_room = center?.testing_room_name_th,
                            exam_seat = tss.testing_seat
                        });
                    }

                    var httpClient = _httpClientFactory.CreateClient("report");
                    var content = new ByteArrayContent(JsonSerializer.SerializeToUtf8Bytes(result));
                    content.Headers.ContentType =
                        new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                    var res = await httpClient.PostAsync(
                        "report-api/reports/student_exam_card/" + (type == "view" ? "pdf" : type), content);
                    if (res.StatusCode == HttpStatusCode.OK)
                    {
                        var stream = await res.Content.ReadAsStreamAsync();
                        switch (type)
                        {
                            case "view":
                                return File(stream, "application/pdf");
                            case "pdf":
                                return File(stream, "application/pdf", Guid.NewGuid().ToString() + ".pdf");
                            case "xls":
                                return File(stream, "application/vnd.ms-excel",
                                    Guid.NewGuid().ToString() + ".xlsx");
                            case "mht":
                                return File(stream, "multipart/related", Guid.NewGuid().ToString() + ".mht");
                            case "csv":
                                return File(stream, "text/csv", Guid.NewGuid().ToString() + ".csv");

                        }

                        return NoContent();
                    }
                    else
                    {
                        return NotFound();
                    }
                }
                else
                {
                    return NotFound();

                }
            }

            if (regis != null)
            {
                var user = _context.t_user.AsNoTracking().FirstOrDefault(w => w.user_uid == regis.user_uid);
                var information = _adminContext.t_testing_information.AsNoTracking()
                    .FirstOrDefault(w => w.testing_information_uid == regis.testing_information_uid);
                var year = _adminContext.t_academic_year.AsNoTracking()
                    .FirstOrDefault(w => w.academic_year_code == information.academic_year_code);
                var semester = _adminContext.t_academic_semester.AsNoTracking()
                    .FirstOrDefault(w => w.academic_semester_code == information.academic_semester_code);
                var canType =  _context.t_candidate_type.AsNoTracking()
                    .FirstOrDefault(w => w.candidate_type_uid == user.candidate_type_uid);
                var facuty =  _adminContext.t_faculty.AsNoTracking()
                    .FirstOrDefault(w => w.faculty_uid == user.faculty_uid);

                var result = new student_testing_card()
                { 
                    exam_round_name = information?.testing_information_name, 
                    exam_round_code = information?.testing_information_code,
                    academic_year_name = year?.academic_year_name_th, 
                    semester_name = semester?.academic_semester_name_th, 
                    student_code = user.student_code, 
                    id_card = regis.examinee_card, 
                    student_name = user.full_name, 
                    apply_type = canType?.candidate_type_name_th, 
                    faculty = facuty?.faculty_name_th, 
                    major = null, 
                    agency = null, 
                    count_of_exam  = regis.user_testing_subject_selecteds.Count()
                };

                result.data = new List<data_student_testing_card>();

                foreach (var tss in regis.user_testing_subject_selecteds)
                {
                    var subject = await _adminContext.t_subject.AsNoTracking()
                        .FirstOrDefaultAsync(w => w.subject_uid == tss.subject_uid);
                    var section = await _adminContext.t_testing_period.AsNoTracking()
                        .FirstOrDefaultAsync(w =>
                            w.testing_information_uid == regis.testing_information_uid && w.period_id == tss.period_id);
                    var branch = await _adminContext.t_branch.AsNoTracking().FirstOrDefaultAsync(w => w.branch_uid == tss.branch_uid);
                    var field = await _adminContext.t_testing_field.AsNoTracking().FirstOrDefaultAsync(w =>
                        w.testing_field_uid == tss.testing_field_uid && w.branch_uid == tss.branch_uid);
                    var center = await _adminContext.t_testing_center.AsNoTracking()
                        .FirstOrDefaultAsync(w => w.testing_center_uid == tss.testing_center_uid);
                    result.data.Add(new data_student_testing_card()
                    { 
                        subject_id = subject?.subject_code,
                        subject_name = subject?.subject_name_th,
                        exam_date =  tss.testing_datetime.Value.ToString("dd MMMM yyyy",culture),
                        section = section != null ?section.period_id + " ("+section.start_time.Value.ToString(@"hh\:mm") + ")" : null,
                        exam_center = branch?.branch_name_th,
                        exam_stadium = field?.testing_field_name_th,
                        exam_room = center?.testing_room_name_th,
                        exam_seat = tss.testing_seat
                    });
                }
                
                var httpClient = _httpClientFactory.CreateClient("report");
                var content = new ByteArrayContent(JsonSerializer.SerializeToUtf8Bytes(result));
                content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                var res = await httpClient.PostAsync(
                    "report-api/reports/student_exam_card/" + (type == "view" ? "pdf" : type), content);
                if (res.StatusCode == HttpStatusCode.OK)
                {
                    var stream = await res.Content.ReadAsStreamAsync();
                    switch (type)
                    {
                        case "view":
                            return File(stream, "application/pdf");
                        case "pdf":
                            return File(stream, "application/pdf", Guid.NewGuid().ToString() + ".pdf");
                        case "xls":
                            return File(stream, "application/vnd.ms-excel", Guid.NewGuid().ToString() + ".xlsx");
                        case "mht":
                            return File(stream, "multipart/related", Guid.NewGuid().ToString() + ".mht");
                        case "csv":
                            return File(stream, "text/csv", Guid.NewGuid().ToString() + ".csv");
           
                    }
           
                    return NoContent();
                }
                else
                {
                    return NotFound();
                }
            }
            else
            {
                return NotFound();
            }
        }
    }
}