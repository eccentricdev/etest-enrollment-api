using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using etest_enrollment_api.Databases;
using etest_enrollment_api.Modules.Reports.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Swashbuckle.AspNetCore.Annotations;

namespace etest_enrollment_api.Modules.Reports.Controllers
{
    [SwaggerTag("สำหรับใบแจ้งหนี้")]
    public class PaymentController :Controller
    {
        private readonly IHttpClientFactory _httpClientFactory;
        readonly Db _context;
        readonly AdminDb _adminContext;
        
        public PaymentController( IHttpClientFactory httpClientFactory, Db context,AdminDb adminContext)
        {
            _httpClientFactory = httpClientFactory;
            _context = context;
            _adminContext = adminContext;
        }

        [HttpGet,
         Route(
             "api/reports/enrollment/print/payment/{user_testing_register_uid}/{type}")]
        [ApiExplorerSettings(GroupName = "reports")]
        public async Task<ActionResult> GetPayment(
            [FromRoute, Required] Guid? user_testing_register_uid,
            [FromRoute, Required] string type
        )
        {
            CultureInfo culture = new CultureInfo("th-TH");
            DateTime utcDate = DateTime.UtcNow;
            var nowDt = utcDate.AddHours(7);
            var regis = await _context.t_user_testing_register.AsNoTracking()
                .Include(i => i.user_testing_subject_selecteds)
                .FirstOrDefaultAsync(w => w.is_cancel == false && w.user_testing_register_uid == user_testing_register_uid);

            var user = await _context.t_user.AsNoTracking().FirstOrDefaultAsync(w => w.user_uid == regis.user_uid);
            var canType = await _context.t_candidate_type.AsNoTracking().FirstOrDefaultAsync(w => w.candidate_type_uid == user.candidate_type_uid);
            var infomation = await _adminContext.t_testing_information.AsNoTracking()
                .FirstOrDefaultAsync( w => w.testing_information_uid == regis.testing_information_uid);

            var year = await _adminContext.t_academic_year.AsNoTracking().FirstOrDefaultAsync(w => w.academic_year_code == infomation.academic_year_code);
            var semester =await _adminContext.t_academic_semester.AsNoTracking().FirstOrDefaultAsync(w => w.academic_semester_code == infomation.academic_semester_code);
            var facuty = await _adminContext.t_faculty.AsNoTracking()
                .FirstOrDefaultAsync(w => w.faculty_uid == user.faculty_uid);
           

            var result = new payment()
            {
                // comcode = "099400019348308",
                barcode = "|" + "099400019348308" + "\r" + regis.ref1 + "\r" + regis.ref2 + "\r" + regis.testing_fee + "00",
                total_amount_text = ThaiBahtText(regis.testing_fee),
                // bill_uid = null,
                // document_control_no = null,
                bill_no = regis.ref1,
                bill_date = nowDt,
                customer_type_id = null, 
                customer_uid = user.user_uid, 
                customer_code = user.student_code, 
                customer_name = user.full_name, 
                // customer_address = null, 
                academic_semester_uid = semester != null ? semester.academic_semester_uid : null, 
                academic_year_name = year != null ? year.academic_year_name_th : null, 
                semester_name = semester != null ? semester.academic_semester_name_th : null, 
                // curriculum_uid = null, 
                // curriculum_name = null, 
                // year_id = null, 
                faculty_name = facuty?.faculty_name_th, 
                // education_type_name = null, 
                // agency_uid = null, 
                // agency_name = null, 
                // tax_no = null, 
                // branch_no = null, 
                // is_head_office = null, 
                // email = null, 
                // phone = null, 
                // payment_from_date = null, 
                // payment_due_date = null, 
                // remark = null, 
                // before_vat_amount = null, 
                // vat_percent = null, 
                // vat_amount = null, 
                total_amount = int.Parse(regis.testing_fee), 
                // bill_status_id = null, 
                // bill_status_remark = null, 
                // reference_code = null, 
                // no_of_items = null, 
                ref1 = regis.ref1, 
                ref2 = regis.ref2
                // bill_url = null
            };
            result.bill_items = new List<bill_item>();

            foreach (var tss in regis.user_testing_subject_selecteds)
            {
                var subject = await _adminContext.t_subject.AsNoTracking().FirstOrDefaultAsync(w => w.subject_uid == tss.subject_uid);
                var infoSj = await _adminContext.t_testing_subject.AsNoTracking().FirstOrDefaultAsync(w =>
                    w.testing_information_uid == regis.testing_information_uid && w.subject_uid == tss.subject_uid);
                result.bill_items.Add(new bill_item()
                {
                    bill_item_uid = null, 
                    bill_uid = null, 
                    // row_order = null, 
                    item_uid = tss.user_testing_subject_selected_uid, 
                    item_code = subject.subject_code, 
                    // unit_name = null, 
                    item_name_th = "ค่าธรรมเนียมในการลงทะเบียนสอบ e-Testing (" + subject.subject_code + ")",
                    // item_name_en = null, 
                    unit_price = infoSj.amount, 
                    // discount_amount = null, 
                    // discount_type_id = null, 
                    // quantity = null, 
                    // before_vat_amount = null, 
                    item_amount = infoSj.amount , 
                    // vat_amount = null, 
                    amount = infoSj.amount, 
                });
            }

            
            //
            var httpClient = _httpClientFactory.CreateClient("report");
            var content = new ByteArrayContent(JsonSerializer.SerializeToUtf8Bytes(result));
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
            var res = await httpClient.PostAsync(
                "report-api/reports/payment/" + (type == "view" ? "pdf" : type), content);
            if (res.StatusCode == HttpStatusCode.OK)
            {
                var stream = await res.Content.ReadAsStreamAsync();
                switch (type)
                {
                    case "view":
                        return File(stream, "application/pdf");
                    case "pdf":
                        return File(stream, "application/pdf", Guid.NewGuid().ToString() + ".pdf");
                    case "xls":
                        return File(stream, "application/vnd.ms-excel", Guid.NewGuid().ToString() + ".xlsx");
                    case "mht":
                        return File(stream, "multipart/related", Guid.NewGuid().ToString() + ".mht");
                    case "csv":
                        return File(stream, "text/csv", Guid.NewGuid().ToString() + ".csv");
           
                }
           
                return NoContent();
            }
            else
            {
                return NotFound();
            }
        }
        
        public string ThaiBahtText(string strNumber, bool IsTrillion = false) 
        { 
            string BahtText = ""; 
            string strTrillion = ""; 
            string[] strThaiNumber = { "ศูนย์", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า", "สิบ" }; 
            string[] strThaiPos = { "", "สิบ", "ร้อย", "พัน", "หมื่น", "แสน", "ล้าน" };
            decimal decNumber = 0; 
            decimal.TryParse(strNumber, out decNumber);
            
            if (decNumber == 0) 
            { 
                return "ศูนย์บาทถ้วน"; 
            }
            
            strNumber = decNumber.ToString("0.00"); 
            string strInteger = strNumber.Split('.')[0]; 
            string strSatang = strNumber.Split('.')[1];
            
            if (strInteger.Length > 13) 
                throw new Exception("รองรับตัวเลขได้เพียง ล้านล้าน เท่านั้น!");
            
            bool _IsTrillion = strInteger.Length > 7 ; 
            if (_IsTrillion) 
            { 
                strTrillion = strInteger.Substring(0, strInteger.Length - 6); 
                BahtText = ThaiBahtText(strTrillion, _IsTrillion); 
                strInteger = strInteger.Substring(strTrillion.Length); 
            }
            
            int strLength = strInteger.Length; 
            
            for (int i = 0; i < strInteger.Length; i++) 
            { 
                string number = strInteger.Substring(i, 1); 
                if (number != "0") 
                { 
                    if (i == strLength - 1 && number == "1" && strLength != 1) 
                    { 
                        BahtText += "เอ็ด"; 
                    }
                    else if (i == strLength - 2 && number == "2" && strLength != 1) 
                    { 
                        BahtText += "ยี่"; 
                    }
                    else if (i != strLength - 2 || number != "1") 
                    { 
                        BahtText += strThaiNumber[int.Parse(number)]; 
                    }
                    
                    BahtText += strThaiPos[(strLength - i) - 1]; 
                } 
            }
            
            if (IsTrillion) 
            { 
                return BahtText + "ล้าน"; 
            }
            
            if(strInteger != "0") 
            { 
                BahtText += "บาท"; 
            }
            
            if (strSatang == "00") 
            { 
                BahtText += "ถ้วน"; 
            }
            else 
            { 
                strLength = strSatang.Length; 
                for (int i = 0; i < strSatang.Length; i++) 
                { 
                    string number = strSatang.Substring(i, 1); 
                    if (number != "0") 
                    { 
                        if (i == strLength - 1 && number == "1" && strSatang[0].ToString() != "0") 
                        { 
                            BahtText += "เอ็ด"; 
                        }
                        else if (i == strLength - 2 && number == "2" && strSatang[0].ToString() != "0") 
                        { 
                            BahtText += "ยี่"; 
                        }
               
                        else if (i != strLength - 2 || number != "1") 
                        { 
                            BahtText += strThaiNumber[int.Parse(number)]; 
                        }
                        
                        BahtText += strThaiPos[(strLength - i) - 1]; 
                    } 
                }
                
                BahtText += "สตางค์"; 
            }
            
            return BahtText; 
        }
    }
}