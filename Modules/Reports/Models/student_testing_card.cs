using System.Collections.Generic;

namespace etest_enrollment_api.Modules.Reports.Models
{
    public class student_testing_card
    {
        public string exam_round_name {get;set;}
        public string exam_round_code {get;set;}
        public string academic_year_name {get;set;}
        public string semester_name {get;set;}
        public string student_code {get;set;}
        public string id_card {get;set;}
        public string student_name {get;set;}
        public string apply_type {get;set;}
        public string faculty {get;set;}
        public string major {get;set;}
        public string agency {get;set;}
        public int? count_of_exam {get;set;}
        public List<data_student_testing_card> data { get; set; }
    }
    
    public class data_student_testing_card
    {
        public string subject_id {get;set;}
        public string subject_name {get;set;}
        public string exam_date {get;set;}
        public string section {get;set;}
        public string exam_center {get;set;}
        public string exam_stadium {get;set;}
        public string exam_room {get;set;}
        public string exam_seat {get;set;}
    }
}