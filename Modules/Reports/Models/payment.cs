using System;
using System.Collections.Generic;

namespace etest_enrollment_api.Modules.Reports.Models
{
    public class payment
    {
        public string comcode {get;set;}
        public string barcode {get;set;}
        public string total_amount_text {get;set;}
        
        public List<bill_item> bill_items {get;set;}
        
        public Guid? bill_uid {get;set;}
        public string document_control_no {get;set;}
        public string bill_no {get;set;}
        public DateTime? bill_date {get;set;}
        public int? customer_type_id {get;set;}
        public Guid? customer_uid {get;set;}
        public string customer_code {get;set;}
        public string customer_name {get;set;}
        public string customer_address {get;set;}
        public Guid? academic_semester_uid {get;set;}
        public string academic_year_name {get;set;}
        public string semester_name {get;set;}
        public Guid? curriculum_uid {get;set;}
        public string curriculum_name {get;set;}
        public string year_id {get;set;}
        public string faculty_name {get;set;}
        public string education_type_name {get;set;}
        public Guid? agency_uid {get;set;}
        public Guid? agency_name {get;set;}
        public Guid? tax_no {get;set;}
        public Guid? branch_no {get;set;}
        public Boolean? is_head_office {get;set;}
        public string email {get;set;}
        public string phone {get;set;}
        public DateTime? payment_from_date {get;set;}
        public DateTime? payment_due_date {get;set;}
        public string remark {get;set;}
        public int? before_vat_amount {get;set;}
        public int? vat_percent {get;set;}
        public int? vat_amount {get;set;}
        public int? total_amount {get;set;}
        public int? bill_status_id {get;set;}
        public string bill_status_remark {get;set;}
        public string reference_code {get;set;}
        public int? no_of_items {get;set;}
        public string ref1 {get;set;}
        public string ref2 {get;set;}
        public string bill_url {get;set;}
    }
    
    public class bill_item
    {
        public Guid? bill_item_uid {get;set;}
        public Guid? bill_uid {get;set;}
        public int? row_order {get;set;}
        public Guid? item_uid {get;set;}
        public string item_code {get;set;}
        public string unit_name {get;set;}
        public string item_name_th {get;set;}
        public string item_name_en {get;set;}
        public int? unit_price {get;set;}
        public int? discount_amount {get;set;}
        public int? discount_type_id {get;set;}
        public int? quantity {get;set;}
        public int? before_vat_amount {get;set;}
        public int? item_amount {get;set;}
        public int? vat_amount {get;set;}
        public int? amount {get;set;}
    }
}