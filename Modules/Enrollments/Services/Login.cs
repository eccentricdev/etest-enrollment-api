using System;
using System.Linq;
using System.Threading.Tasks;
using etest_enrollment_api.Databases;
using etest_enrollment_api.Databases.Models;
using etest_enrollment_api.Modules.Commons.Models;
using etest_enrollment_api.Modules.Enrollments.Databases.Models;
using etest_enrollment_api.Modules.Enrollments.Models;
using Microsoft.EntityFrameworkCore;

namespace etest_enrollment_api.Modules.Enrollments.Services
{
    public class LoginService
    {
        readonly Db _context;
        readonly AdminDb _adminContext;

        public LoginService(Db context, AdminDb adminDb)
        {
            _context = context;
            _adminContext = adminDb;
        }
        
        public async Task<user_respone> UserLogin(login_request loginRequest)
        {
            var userRs = new v_user();
            var rs = new user_respone();
            if (loginRequest.student_code != null)
            {
                // if (loginRequest.student_code.Trim().Length == 10)
                // { 
                    userRs = _context.v_user.FirstOrDefault(w => 
                    w.candidate_type_uid == loginRequest.candidate_type_uid && 
                    w.student_code == loginRequest.student_code && 
                    w.password == loginRequest.password);
                // }
            }
            else if (loginRequest.examinee_card != null)
            {
                userRs = _context.v_user.FirstOrDefault(w => 
                    w.candidate_type_uid == loginRequest.candidate_type_uid && 
                    w.examinee_card == loginRequest.examinee_card && 
                    w.password == loginRequest.password);
            }

            if (userRs?.user_uid != null)
            {
                var faculty = _adminContext.t_faculty.FirstOrDefault(w => w.faculty_uid == userRs.faculty_uid);
                var depaetment = _adminContext.t_department.FirstOrDefault(w => w.department_uid == userRs.department_uid);
                var type = _context.t_candidate_type.FirstOrDefault(w =>
                    w.candidate_type_uid == userRs.candidate_type_uid);
                rs.user_uid = userRs.user_uid;
                rs.candidate_type_uid = userRs.candidate_type_uid;
                rs.candidate_type_name_th = type != null ? type.candidate_type_name_th : null;
                rs.examinee_card = userRs?.examinee_card ?? null;
                rs.student_code = userRs?.student_code ?? null;
                rs.full_name = userRs.full_name;
                rs.faculty_uid = userRs.faculty_uid;
                rs.department_uid = userRs.department_uid;
                rs.mobile_phone = userRs.mobile_phone;
                rs.email = userRs.email;
                rs.password = userRs.password;
                rs.photo1_name = userRs.photo1_name;
                rs.photo1_url = userRs.photo1_url;
                rs.photo2_name =userRs.photo2_name; 
                rs.photo2_url = userRs.photo2_url;
                rs.is_register = userRs.is_register;
                rs.user_approve_pdpa = userRs.user_approve_pdpa;
                if (faculty != null && faculty.faculty_name_th != null)
                {
                    rs.faculty_name_th = faculty.faculty_name_th;
                }
                if (depaetment != null && depaetment.department_name_th != null)
                {
                    rs.department_name_th = depaetment.department_name_th;
                }

                return rs;
            }
            else
            {
                return null;
            }
        }

    }
}