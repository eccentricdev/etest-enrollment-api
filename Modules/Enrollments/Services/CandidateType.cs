using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;
using etest_enrollment_api.Databases;
using etest_enrollment_api.Databases.Models;
using Db = etest_enrollment_api.Databases.Db;
using System;
using etest_enrollment_api.Modules.Enrollments.Databases.Models;

namespace etest_enrollment_api.Modules.Enrollments.Services
{

  public class CandidateTypeService
  {
    readonly Db _context;

    public CandidateTypeService(Db context)
    {
      _context = context;
    }

    public async Task<List<v_candidate_type>> ListCandidateType(v_candidate_type candidate_type)
    {

      IQueryable < v_candidate_type > candidate_types = _context.v_candidate_type.AsNoTracking();


      if (candidate_type != null)
      {
        candidate_types = candidate_type.search != null ? candidate_types.Search(candidate_type.search) : candidate_types.Search(candidate_type);
      }
      candidate_types = candidate_types.OrderBy(c => c.candidate_type_uid);
      return await candidate_types.ToListAsync();
    }

    public async Task<v_candidate_type> GetCandidateType(Guid id)
    {
      return await _context.v_candidate_type.AsNoTracking().FirstOrDefaultAsync(a => a.candidate_type_uid == id);
    }

    public async Task<t_candidate_type> NewCandidateType(t_candidate_type candidate_type, ClaimsIdentity claimsIdentity)
    {
      candidate_type.candidate_type_uid = Guid.NewGuid();
      await _context.t_candidate_type.AddAsync(candidate_type);
      await _context.SaveChangesAsync();
      return candidate_type;
    }

    public async Task<int> UpdateCandidateType(t_candidate_type candidate_type, ClaimsIdentity claimsIdentity)
    {
      _context.t_candidate_type.Update(candidate_type);
      return await _context.SaveChangesAsync();
    }

    public async Task<int> DeleteCandidateType(Guid id, ClaimsIdentity claimsIdentity)
    {
      var candidateType = await _context.t_candidate_type.FirstOrDefaultAsync(c => c.candidate_type_uid == id);
      _context.Remove(candidateType);
      return await _context.SaveChangesAsync();
    }
  }

}
