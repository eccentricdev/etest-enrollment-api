using System;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using etest_enrollment_api.Databases;
using etest_enrollment_api.Modules.Commons.Models;
using etest_enrollment_api.Modules.Enrollments.Models;
using Microsoft.EntityFrameworkCore;

namespace etest_enrollment_api.Modules.Enrollments.Services
{
  public class ResetPasswordService
  {
    readonly Db _context;
    private readonly Settings _setting;
    public ResetPasswordService(Db context,Settings setting)
    {
      this._context = context;
      this._setting = setting;
    }

    public async Task SendMail(string password , string email)
    {
      // var stream = await res.Content.ReadAsStreamAsync();
      try{
      using (var mailMessage = new MailMessage())
      {
        using (var client = new SmtpClient(_setting.smtp_server, _setting.smtp_port))
        {
          client.UseDefaultCredentials = false;
          client.Credentials = new NetworkCredential(_setting.smtp_user_name, _setting.smtp_password);
          client.DeliveryMethod = SmtpDeliveryMethod.Network;
          client.EnableSsl = _setting.smtp_use_ssl;
          mailMessage.From = new MailAddress(_setting.smtp_user_name);
          mailMessage.To.Add(email);
          mailMessage.Subject = "Password for your Account"; 
          mailMessage.Body = "Your password is:" + password; 
          mailMessage.IsBodyHtml = true;
          // ContentType ct = new ContentType(System.Net.Mime.MediaTypeNames.Application.Pdf);
          // Attachment attach = new Attachment(stream, ct);
          // attach.ContentDisposition.FileName = Guid.NewGuid().ToString() + ".pdf";
          // mailMessage.Attachments.Add(attach);
          client.Send(mailMessage);
        }
      }}
      catch (Exception ex) {
         Console.WriteLine("Exception in sendEmail:" + ex.Message);
      }
    }

    public async Task<int> ResetPassword (string student_code, ClaimsIdentity claimsIdentity)
    {
       int length = 8;
       var result = _context.t_user.AsNoTracking()
         .FirstOrDefault(w => w.student_code == student_code);
       if(result != null)
       {
         string newPassword = GeneratePassword(length);
         if(newPassword != null)
         {
           result.password = newPassword;
           _context.t_user.Update(result);
           await _context.SaveChangesAsync();
           SendMail(result.password,result.email);
          return 1;     
         }else
         {
           return 0;
         }
       }
       else 
       {
         return 0;
       }  
    }

    private static string GeneratePassword(int length)
    {
      const string chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
      StringBuilder sb = new StringBuilder();
      Random rnd = new Random();
      for (int i = 0; i < length; i++)
      {
        int index = rnd.Next(chars.Length);
        sb.Append(chars[index]);
      }
      return sb.ToString();
    }

  }
}
