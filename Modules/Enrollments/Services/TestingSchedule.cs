using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using etest_enrollment_api.Databases;
using etest_enrollment_api.Modules.Enrollments.Models;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;

namespace etest_enrollment_api.Modules.Enrollments.Services
{
    public class TestingScheduleService
    {
        readonly Db _context;
        private readonly AdminDb _adminContext;

        public TestingScheduleService(Db context, AdminDb adminContext)
        {
            _context = context;
            _adminContext = adminContext;
        }

        public async Task<List<testing_schedule>> ListTestingResult(string student_code)
        {
            CultureInfo culture = new CultureInfo("th-TH");
            var result = new List<testing_schedule>();
            var user = _context.t_user.AsNoTracking().FirstOrDefault(w => w.student_code == student_code);
            var regiss = _context.t_user_testing_register.AsNoTracking()
                .Where(w => w.user_uid == user.user_uid && w.is_cancel == false && w.seat_status_id == 5).ToList();
            
            foreach (var regis in regiss)
            {
                var subjectSelects = _context.t_user_testing_subject_selected.AsNoTracking()
                    .Where(w => w.user_testing_register_uid == regis.user_testing_register_uid);
                
                foreach (var rs in subjectSelects)
                {
                    var subject = _adminContext.t_subject.AsNoTracking().FirstOrDefault(w => w.subject_uid == rs.subject_uid);
                    var branch = _adminContext.t_branch.AsNoTracking().FirstOrDefault(w => w.branch_uid == rs.branch_uid);
                    var field = _adminContext.t_testing_field.AsNoTracking()
                        .FirstOrDefault(w => w.testing_field_uid == rs.testing_field_uid);
                    var room = _adminContext.t_testing_center.AsNoTracking()
                        .FirstOrDefault(w => w.testing_center_uid == rs.testing_center_uid);
                    var roomPeriod = _adminContext.t_testing_period.AsNoTracking()
                        .FirstOrDefault(w =>
                            w.testing_information_uid == regis.testing_information_uid && w.period_id == rs.period_id);
                    var time = roomPeriod != null ? roomPeriod.start_time.Value.ToString(@"hh\:mm") : " ";
                    result.Add(new testing_schedule()
                    { 
                        user_testing_subject_selected_uid =rs.user_testing_subject_selected_uid , 
                        subject_uid = rs.subject_uid, 
                        subject_code = subject != null ? subject.subject_code : null, 
                        testing_datetime = rs.testing_datetime.Value.ToString("dd MMMM yyyy",culture)
                                           + " "+ time +" น.",
                        branch_uid = rs.branch_uid, 
                        branch_name_th = branch != null ? branch.branch_name_th : null, 
                        testing_field_uid = rs.testing_field_uid, 
                        testing_field_name_th = field != null ? field.testing_field_name_th : null, 
                        testing_center_uid = rs.testing_center_uid, 
                        testing_room_name_th = room != null ? room.testing_room_name_th : null, 
                        testing_seat = rs.testing_seat
                    });
                }
            }
            return result;
        }
    }
}