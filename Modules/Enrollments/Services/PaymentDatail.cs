using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;
using etest_enrollment_api.Databases;
using etest_enrollment_api.Databases.Models;
using etest_enrollment_api.Modules.Commons.Models;
using Db = etest_enrollment_api.Databases.Db;
using System;
using System.Globalization;
using etest_enrollment_api.Modules.Enrollments.Databases.Models;
using etest_enrollment_api.Modules.Enrollments.Models;
using SeventyOneDev.Core.Document;
using SeventyOneDev.Core.Document.Services;

namespace etest_enrollment_api.Modules.Enrollments.Services
{

  public class PaymentDetailService
  {
    readonly Db _context;
    readonly AdminDb _adminContext;
    private readonly DocumentNoService _documentNoService;

    public PaymentDetailService(Db context, AdminDb adminDb, DocumentNoService documentNoService)
    {
      _context = context;
      _adminContext = adminDb;
      _documentNoService = documentNoService;
    }

    public List<payment_detail> ListPayment(Guid id)
    {
        var registerList =  _context.t_user_testing_register.Where(a => a.user_uid == id && a.is_cancel == false).ToList();
        List<payment_detail> rs = new List<payment_detail>();
        foreach(var register in registerList)
        {
          var info = _adminContext.t_testing_information.AsNoTracking()
            .FirstOrDefault(w => w.testing_information_uid == register.testing_information_uid);
          rs.Add(new payment_detail()
          {
            user_uid = register.user_uid,
            examinee_card = register.examinee_card,
            user_testing_register_uid = register.user_testing_register_uid,
            payment_code = register.payment_code ?? register.ref2,
            payment_name = "ค่าธรรมเนียมในการลงทะเบียนสอบ e-Testing",
            testing_fee = register.testing_fee,
            is_pay_later = register.is_pay_later,
            testing_information_uid = register.testing_information_uid,
            testing_information_name = info?.testing_information_name,
            testing_information_code = info?.testing_information_code,
            academic_semester_code = info?.academic_semester_code,
            academic_year_code = info?.academic_year_code
          });
        } 
        return rs;
    }

    public  List<testing_detail> ListPaymentDetail (Guid id)
    {
      var result = new List<testing_detail>();
      var register = _context.v_user_testing_register
        .Include(w => w.user_testing_subject_selecteds.OrderBy(w => w.testing_datetime).ThenBy(w => w.period_id))
        .FirstOrDefault(a => a.user_testing_register_uid == id && a.is_cancel == false);

      foreach(var detail in register.user_testing_subject_selecteds){
        var subAmount = _adminContext.v_testing_subject.FirstOrDefault(a =>
          a.subject_uid == detail.subject_uid && a.testing_information_uid == detail.testing_information_uid);
        var rs = new testing_detail()
        {
          testing_information_name = detail.testing_information_name,
          testing_information_code = detail.testing_information_code,
          subject_code = subAmount?.subject_code,
          subject_name_th =  subAmount?.subject_name_th,
          testing_datetime = detail.testing_datetime?.Date,
          period_id = detail.period_id,
          start_time = detail.start_time,
          fee = detail.amount?.ToString(),
          total_subject = "1",
          total_fee = subAmount?.amount.ToString(),
          ref2 = register?.ref2
        };
        result.Add(rs);
      }
      
      return result;
    }

    public async Task<t_user_testing_register> ListPaymentCallBack(payment_callback request)
    {
      var payDatetime = DateTime.Now;
      ThaiBuddhistCalendar cal = new ThaiBuddhistCalendar();
      int year;
      if (payDatetime.Month >= 10 && payDatetime.Day >= 1)
      {
        year = cal.GetYear(payDatetime.AddYears(1));
      }
      else
      {
        year = cal.GetYear(payDatetime);
      }
      var documentNoData = new document_no_data()
      {
        year_code = year.ToString()
      };

      if (request.Invoice != null)
      {
        var register = await _context.t_user_testing_register.FirstOrDefaultAsync(u =>
          u.ref2 == request.Invoice);
        register.is_pay_later = false;
        register.payment_datetime = payDatetime;
        register.payment_method = "credit_card";
        register.payment_code = await _documentNoService.GetDocumentNo(null, "PAYMENT", documentNoData);
        register.seat_status_id = SeatStatus.Status.Get("CONFIRM").id;
        _context.Update(register);
        await _context.SaveChangesAsync();

        return register;
      }
      else
      {
        return null;
      }
    }

  }


}
