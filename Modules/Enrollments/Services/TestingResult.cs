using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;
using etest_enrollment_api.Databases;
using etest_enrollment_api.Modules.Enrollments.Databases.AdminModels;
using etest_enrollment_api.Modules.Enrollments.Databases.TestModels;
using etest_enrollment_api.Modules.Enrollments.Models;

namespace etest_enrollment_api.Modules.Enrollments.Services
{

  public class TestingResultService
  {
    readonly Db _context;
    private readonly AdminDb _adminContext;
    readonly TestDb _testContext;

    public TestingResultService(Db context, TestDb testContext, AdminDb adminContext)
    {
      _context = context;
      _testContext = testContext;
      _adminContext = adminContext;
    }

    public async Task<List<testing_result_respone>> ListTestingResult(testing_result_request request)
    {
      var testingResultRespones = new List<testing_result_respone>();
      var tests = _testContext.v_tests.AsNoTracking().ToList();

      if (request.user_uid != null)
      {
        var user = await _context.v_user.AsNoTracking().FirstOrDefaultAsync(w => w.user_uid == request.user_uid);
        if (user != null)
        {
          tests = tests.Where(w => w.student_code == user.student_code).ToList();
        }
      }
      if (request.academic_semester_code != null)
      {
        tests = tests.Where(w => w.academic_semester_code == request.academic_semester_code).ToList();
      }
      if (request.academic_year_code != null)
      {
        tests = tests.Where(w => w.academic_year_code == request.academic_year_code).ToList();
      }

      foreach (var test in tests)
      {
        var rs = new testing_result_respone();
        // var sj = await _adminContext.v_subject.AsNoTracking().FirstOrDefaultAsync(w => w.subject_uid == test.subject_uid);
        // var info = await _adminContext.v_testing_information.AsNoTracking()
        //   .FirstOrDefaultAsync(w => w.testing_information_uid == test.testing_information_uid);

        // if (info != null)
        // {
        var year = await _adminContext.t_academic_year.FirstOrDefaultAsync(
          w => w.academic_year_code == test.academic_year_code);
        var semester = await
          _adminContext.t_academic_semester.FirstOrDefaultAsync(w =>
            w.academic_semester_code == test.academic_semester_code);

        rs.examinee_card = test.citizen_id ?? null;
        rs.testing_information_uid = test.testing_information_uid;
        rs.subject_uid = test.subject_uid;
        rs.academic_semester_code = test.academic_semester_code;
        rs.academic_year_code = test.academic_year_code;
        rs.grade = test.grade ?? null;
        if (year != null && semester != null)
        {
          rs.academic_semester_name_th = semester.academic_semester_name_th;
          rs.academic_year_name_th = year.academic_year_name_th;
        }

        rs.subject_code = test.subject_information_code;
        rs.subject_name_th = test.subject_information_name_th;

        // }

        testingResultRespones.Add(rs);
      }

      return testingResultRespones;
    }
  }
}

