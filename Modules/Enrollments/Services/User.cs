using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;
using etest_enrollment_api.Databases;
using etest_enrollment_api.Databases.Models;
using Db = etest_enrollment_api.Databases.Db;
using System;
using etest_enrollment_api.Modules.Commons.Models;
using etest_enrollment_api.Modules.Enrollments.Databases.AdminModels;
using etest_enrollment_api.Modules.Enrollments.Databases.Models;
using etest_enrollment_api.Modules.Enrollments.Models;

namespace etest_enrollment_api.Modules.Enrollments.Services
{

  public class UserService
  {
    readonly Db _context;
    readonly AdminDb _adminContext;

    public UserService(Db context, AdminDb adminDb)
    {
      _context = context;
      _adminContext = adminDb;
    }

    public async Task<user_respone> GetUser(Guid id)
    {
      var rs = new user_respone();
      var user = await _context.v_user.AsNoTracking().FirstOrDefaultAsync(a => a.user_uid == id);
      if (user != null)
      {
        var faculty = _adminContext.t_faculty.FirstOrDefault(w => w.faculty_uid == user.faculty_uid);
        var depaetment = _adminContext.t_department.FirstOrDefault(w => w.department_uid == user.department_uid);
        var type = _context.t_candidate_type.FirstOrDefault(w => w.candidate_type_uid == user.candidate_type_uid);
        rs.user_uid = user.user_uid;
        rs.candidate_type_uid = user.candidate_type_uid;
        rs.candidate_type_name_th = type != null ? type.candidate_type_name_th : null;
        rs.examinee_card = user.examinee_card;
        rs.student_code = user.student_code;
        rs.full_name = user.full_name;
        rs.faculty_uid = user.faculty_uid;
        rs.department_uid = user.department_uid;
        rs.mobile_phone = user.mobile_phone;
        rs.email = user.email;
        rs.password = user.password;
        rs.photo1_name = user.photo1_name;
        rs.photo1_url = user.photo1_url;
        rs.photo2_name =user.photo2_name;
        rs.photo2_url = user.photo2_url;
        rs.is_register = user.is_register;
        rs.user_approve_pdpa = user.user_approve_pdpa;
        if (faculty != null && faculty.faculty_name_th != null)
        {
          rs.faculty_name_th = faculty.faculty_name_th;
        }
        if (depaetment != null && depaetment.department_name_th != null)
        {
          rs.department_name_th = depaetment.department_name_th;
        }
      }
      return rs;
    }
    public async Task<user_respone> GetUserByCard(string id)
    {
      var rs = new user_respone();
      var user = await _context.v_user.AsNoTracking().FirstOrDefaultAsync(a => a.examinee_card == id);
      if (user != null)
      {
        var faculty = _adminContext.t_faculty.FirstOrDefault(w => w.faculty_uid == user.faculty_uid);
        var depaetment = _adminContext.t_department.FirstOrDefault(w => w.department_uid == user.department_uid);
        var type = _context.t_candidate_type.FirstOrDefault(w => w.candidate_type_uid == user.candidate_type_uid);
        rs.user_uid = user.user_uid;
        rs.candidate_type_uid = user.candidate_type_uid;
        rs.candidate_type_name_th = type != null ? type.candidate_type_name_th : null;
        rs.examinee_card = user.examinee_card;
        rs.student_code = user.student_code;
        rs.full_name = user.full_name;
        rs.faculty_uid = user.faculty_uid;
        rs.department_uid = user.department_uid;
        rs.mobile_phone = user.mobile_phone;
        rs.email = user.email;
        rs.password = user.password;
        rs.photo1_name = user.photo1_name;
        rs.photo1_url = user.photo1_url;
        rs.photo2_name =user.photo2_name; 
        rs.photo2_url = user.photo2_url;
        rs.is_register = user.is_register;
        rs.user_approve_pdpa = user.user_approve_pdpa;
        if (faculty != null && faculty.faculty_name_th != null)
        {
          rs.faculty_name_th = faculty.faculty_name_th;
        }
        if (depaetment != null && depaetment.department_name_th != null)
        {
          rs.department_name_th = depaetment.department_name_th;
        }
      }
      return rs;
    }

     public async Task<List<v_user>> ListUser(v_user user)
    {

      IQueryable < v_user > users = _context.v_user.AsNoTracking();


      if (user != null)
      {
        users = user.search != null ? users.Search(user.search) : users.Search(user);
      }
      users = users.OrderBy(c => c.user_uid);
      return await users.ToListAsync();
    }

     public async Task<t_user> NewUser(t_user user, ClaimsIdentity claimsIdentity)
     {
       var findUser = await _context.v_user.AsNoTracking()
         .FirstOrDefaultAsync(w => w.student_code == user.student_code);

       if (findUser != null) throw new Exception("มีรหัสผู้สอบนี้อยู่ในระบบแล้ว");

       user.user_uid = Guid.NewGuid();
       user.student_code = user.student_code?.Replace(" ", String.Empty);
       user.examinee_card = string.Empty;
       await _context.t_user.AddAsync(user);
       await _context.SaveChangesAsync();
       return user;
     }

     public async Task<int> UpdateUser(t_user user, ClaimsIdentity claimsIdentity)
     {
       var result = _context.t_user.AsNoTracking().FirstOrDefault(w => w.student_code == user.student_code && w.user_uid != user.user_uid);
       if (result == null)
       {
         _context.t_user.Update(user);
         return await _context.SaveChangesAsync();
       }
       else
       {
         return 0;
       }
     }

     public async Task<int> DeleteUser(Guid id, ClaimsIdentity claimsIdentity)
    {
      var user  = await _context.t_user.FirstOrDefaultAsync(c => c.user_uid == id);
      _context.Remove(user );
      return await _context.SaveChangesAsync();

    }
    
    public async Task<student_check> ChkStudentLogin(string student_code,string id_card)
    {
      var rs = new student_check();
      rs.is_student = false;
      t_student student = null;

      if (student_code != null && id_card != null)
      {
         student = await _adminContext.t_student.AsNoTracking()
          .FirstOrDefaultAsync(w => w.student_code == student_code);

         if (student == null)
         {
           student = await _adminContext.t_student.AsNoTracking()
             .FirstOrDefaultAsync(w => w.id_card == id_card);
         }
      }
      else if (student_code != null)
      {
        student = await _adminContext.t_student.AsNoTracking()
          .FirstOrDefaultAsync(w => w.student_code == student_code);
      }
      else if (id_card != null)
      {
        student = await _adminContext.t_student.AsNoTracking()
          .FirstOrDefaultAsync(w => w.id_card == id_card);
      }
      if (student == null)
      {
        var ru_student = await _adminContext.t_ru_import_student.AsNoTracking()
          .FirstOrDefaultAsync(w => w.student_code == student_code);
        if (ru_student != null)
        {
          rs.is_student = true;
        }
      }
      if (student != null)
      {
        rs.is_student = true;
      }
      
      return rs;
    }
  }


}
