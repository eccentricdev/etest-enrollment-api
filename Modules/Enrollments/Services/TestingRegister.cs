using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;
using etest_enrollment_api.Databases;
using System;
using System.Globalization;
using etest_enrollment_api.Modules.Enrollments.Databases.AdminModels;
using etest_enrollment_api.Modules.Enrollments.Databases.Models;
using etest_enrollment_api.Modules.Enrollments.Models;
using SeventyOneDev.Core.Document;
using SeventyOneDev.Core.Document.Services;
using Db = etest_enrollment_api.Databases.Db;


namespace etest_enrollment_api.Modules.Enrollments.Services
{
  public class TestingRegisterService
  {
    readonly Db _context;
    readonly AdminDb _adminContext;
    private readonly DocumentNoService _documentNoService;
    public TestingRegisterService(Db context, AdminDb adminDb,DocumentNoService documentNoService)
    {
        _context = context;
        _adminContext = adminDb;
        _documentNoService = documentNoService;
    }
    public async Task<List<v_user_testing_register>> ListTestingRegister(v_user_testing_register user_testing_register)
    {
      IQueryable<v_user_testing_register> user_testing_registers = _context.v_user_testing_register.AsNoTracking();
      if (user_testing_register != null)
      {
        user_testing_registers = user_testing_register.search != null ? user_testing_registers.Search(user_testing_register.search) : user_testing_registers.Search(user_testing_register);
      }
      user_testing_registers = user_testing_registers.OrderBy(c => c.user_testing_register_uid);
      return await user_testing_registers.ToListAsync();
    }
    public async Task<v_user_testing_register> GetTestingRegister(Guid id)
    {
      return await _context.v_user_testing_register
       .Include(t => t.user_testing_subject_selecteds)
      .FirstOrDefaultAsync(a => a.user_testing_register_uid == id);
    }
    public async Task<t_user_testing_register> NewTestingRegister(t_user_testing_register user_testing_register, ClaimsIdentity claimsIdentity)
    {
      var chkSubject = CheckInForAndSubject(user_testing_register);
      if (chkSubject == null)
      {
        var chkDate = CheckDate(user_testing_register);
        if (chkDate == null)
        {
          user_testing_register.user_testing_register_uid = Guid.NewGuid();
          List<t_user_testing_subject_selected> subjectSeatList = new List<t_user_testing_subject_selected>();
          var subSelect = new List<t_user_testing_subject_selected>();
          subjectSeatList = SeatManage(user_testing_register);
          if (subjectSeatList.Any(i => i == null))
          {
            return null;
          }
          else
          {
            DateTime utcDate = DateTime.UtcNow;
            var nowDt = utcDate.AddHours(7);
            // foreach(var rs in subjectSeatList){
            foreach (var rs in user_testing_register.user_testing_subject_selecteds)
            {
              var roomName =
                _adminContext.t_testing_center.FirstOrDefault(r => r.testing_center_uid == rs.testing_center_uid);
              rs.user_testing_subject_selected_uid = Guid.NewGuid();
              rs.user_testing_register_uid = user_testing_register.user_testing_register_uid;
              rs.is_cancel = false;
              rs.testing_room_name = roomName.testing_room_name_th;
              subSelect.Add(rs);
            }

            var amount = new List<int?>();
            foreach (var subject in user_testing_register.user_testing_subject_selecteds)
            {
              var testingSubject = _adminContext.v_testing_subject.FirstOrDefault(r =>
                r.subject_uid == subject.subject_uid &&
                r.testing_information_uid == user_testing_register.testing_information_uid);
              if (testingSubject != null)
              {
                amount.Add(testingSubject.amount);
              }
            }

            var user = _context.t_user.AsNoTracking().FirstOrDefault(w => w.user_uid == user_testing_register.user_uid);

            ThaiBuddhistCalendar cal = new ThaiBuddhistCalendar();
            int y = cal.GetYear(nowDt);
            // if (nowDt.Month >= 8 && nowDt.Day >= 1)
            // {
            //   y = cal.GetYear(nowDt.AddYears(1));
            // }
            // else
            // {
            //   y = cal.GetYear(nowDt);
            // }
            //
            // var payment = _adminContext.v_testing_register
            //   .Where(t => t.payment_code != null && (t.payment_code.Substring(0, 4)) == y.ToString()).ToList();
            
            var paymentEn = _context.v_user_testing_register
              .Where(t => t.ref2 != null && (t.ref2.Substring(0, 4)) == y.ToString()).ToList();

            int sequenceNumber = paymentEn.Count() + 1;
            string id = string.Format(y + "{0}", sequenceNumber.ToString().PadLeft(6, '0'));
            CultureInfo culture = new CultureInfo("th-TH");
            string ref1 = "";
            int r;
            if (user != null)
            {
              for (int i = 0; i < 999; i++)
              {
                r = (new Random()).Next(100, 1000);
                ref1 = user.student_code.PadLeft(10, '0') + nowDt.ToString("yy", culture) + r.ToString("000");
                ;
                var rss = _context.t_user_testing_register.AsNoTracking().FirstOrDefault(w => w.ref1 == ref1);
                if (rss == null)
                {
                  ref1 = ref1;
                  break;
                }
              }
            }
            
            t_student student = null;
            if (user?.student_code != null)
            {
              student = await _adminContext.t_student.AsNoTracking().FirstOrDefaultAsync(w =>
                w.testing_information_uid == user_testing_register.testing_information_uid &&
                w.student_code == user.student_code);
            }

            string total = amount.Sum().ToString();
            user_testing_register.examinee_card = String.Empty;
            user_testing_register.payment_code = null;
            user_testing_register.testing_fee = total;
            user_testing_register.is_cancel = false;
            user_testing_register.is_pay_later = true;
            user_testing_register.payment_datetime = null;
            user_testing_register.payment_method = null;
            user_testing_register.user_testing_subject_selecteds = subSelect;
            user_testing_register.created_datetime = nowDt;
            user_testing_register.ref1 = ref1;
            user_testing_register.ref2 = id;
            user_testing_register.photo1_url = student?.photo_url ?? null;
            await _context.t_user_testing_register.AddAsync(user_testing_register);
            await _context.SaveChangesAsync();
            return user_testing_register;
          }
        }
        else
        {
          throw new Exception(chkDate);
        }
      }
      else
      {
        throw new Exception(chkSubject);
      }
    }
    public List<t_user_testing_subject_selected> SeatManage(t_user_testing_register user_testing_register)
    {
      List<t_user_testing_subject_selected> subjectSelecteds = new List<t_user_testing_subject_selected>();
      List<seat_detail> seatDetails = new List<seat_detail>();
      List<curr_seat> currSeat = new List<curr_seat>();

      foreach (var testingSubjectSelecteds in user_testing_register.user_testing_subject_selecteds)
      {
        var currentSeat = _context.v_user_testing_subject_selected
          .Where(t => t.is_cancel == false 
                      && t.testing_center_uid == testingSubjectSelecteds.testing_center_uid 
                      && t.period_id == testingSubjectSelecteds.period_id 
                      &&t.testing_datetime.Value.Date == testingSubjectSelecteds.testing_datetime.Value.Date)
          .Select(t => new { t.testing_seat, t.period_id }).ToList();
        
        var currentSeatAd = _adminContext.v_testing_subject_selected
          .Where(t => t.is_cancel == false 
                      && t.testing_center_uid == testingSubjectSelecteds.testing_center_uid 
                      && t.period_id == testingSubjectSelecteds.period_id 
                      &&t.testing_datetime.Value.Date == testingSubjectSelecteds.testing_datetime.Value.Date)
          .Select(t => new { t.testing_seat, t.period_id }).ToList();
      
        foreach (var rs in currentSeat)
        {
          var seat = new curr_seat();
          // seat.testing_datetime = rs.testing_datetime;
          seat.testing_seat = rs.testing_seat;
          seat.period_id = rs.period_id;
          currSeat.Add(seat);
        }

        foreach (var rs in currentSeatAd)
        {
          var seat = new curr_seat();
          // seat.testing_datetime = rs.testing_datetime;
          seat.testing_seat = rs.testing_seat;
          seat.period_id = rs.period_id;
          currSeat.Add(seat);
        }
        
        var testingRoom = _adminContext.v_testing_room.FirstOrDefault(r =>
          r.testing_information_uid == user_testing_register.testing_information_uid
          && r.testing_center_uid == testingSubjectSelecteds.testing_center_uid);
        
        var testingSeats = _adminContext.v_testing_seat.Where(a => a.testing_room_uid == testingRoom.testing_room_uid).ToList();
        foreach (var testingSeat in testingSeats)
        {
          if (testingSeat.is_specify_subject == true)
          {
            var seatSubs = _adminContext.v_testing_seat_subject.Where(a =>
              a.testing_seat_uid == testingSeat.testing_seat_uid &&
              a.subject_uid == testingSubjectSelecteds.subject_uid).ToList();
            if (seatSubs.Count() != 0)
            {
              foreach (var seatSub in seatSubs)
              {
                var rs = new seat_detail();
                rs.subject_uid = seatSub.subject_uid;
                rs.row_name = testingSeat.row_name;
                rs.no_of_seats = testingSeat.no_of_seats;
                rs.start_seat_no = seatSub.start_seat_no ?? 0;
                rs.end_seat_no = seatSub.end_seat_no ?? 0;
                rs.is_specify_subject = true;
                seatDetails.Add(rs);
              }
            }
          }
          else
          {
            var rs = new seat_detail();
            rs.subject_uid = null;
            rs.row_name = testingSeat.row_name;
            rs.no_of_seats = testingSeat.no_of_seats;
            rs.start_seat_no = 0;
            rs.end_seat_no = 0;
            rs.is_specify_subject = false;
            seatDetails.Add(rs);
          }
        }
        testingSubjectSelecteds.testing_seat = SelectSeat(testingRoom.is_random_seat, seatDetails, testingSubjectSelecteds.period_id, currSeat
                                                , testingSubjectSelecteds.testing_datetime);
        if (testingSubjectSelecteds.testing_seat != null)
        {
          testingSubjectSelecteds.testing_room_name = testingRoom.testing_room_name_th;
          subjectSelecteds.Add(testingSubjectSelecteds);
        }
        else
        {
          subjectSelecteds.Add(null);
        }
      }
      return subjectSelecteds;
    }
    public string SelectSeat(Boolean? isRandom, List<seat_detail> seatDetails, int? period_id
   , List<curr_seat> ListCurrSeat, DateTime? testingDate)
    {
      var seatNo = new List<string>();
      string seat;
      DateTime testDate = (Convert.ToDateTime(testingDate.ToString()));
      var currseat = new List<string>();
      foreach (var currSeat in ListCurrSeat)
      {
        currseat.Add(currSeat.testing_seat);
      }
      
      foreach (var datail in seatDetails)
      {
        if (datail.is_specify_subject != true)
        {
          for (int column = 1; column <= datail.no_of_seats; column++)
          {
            seatNo.Add(datail.row_name + column);
          }
        }
        else
        {
          if (datail.start_seat_no != 0 && datail.end_seat_no != 0)
          {
            for (int column = (int)datail.start_seat_no; column <= datail.end_seat_no; column++)
            {
              seatNo.Add(datail.row_name + column);
            }
          }
        }
      }
      if (isRandom != true)
      {
        seat = seatNo.Except(currseat).FirstOrDefault();
        currseat.Add(seat);
      }
      else
      {
        var leftSeats = seatNo.Except(currseat).ToList();
        Random ran = new Random();
        var r = ran.Next(leftSeats.Count);
        seat = leftSeats.ElementAt(r);
        currseat.Add(seat);
      }
      return seat;
    }
    
    public string CheckInForAndSubject(t_user_testing_register user_testing_register)
    {
      string msg = null;
      var regis = _context.v_user_testing_register.AsNoTracking()
        .Where(w => w.user_uid == user_testing_register.user_uid &&
                    w.testing_information_uid == user_testing_register.testing_information_uid &&
                    w.is_cancel == false)
        .Include(i => i.user_testing_subject_selecteds)
        .ToList();
      
      var adRegis = _adminContext.v_testing_register.AsNoTracking()
        .Where(w => w.user_uid == user_testing_register.user_uid &&
                    w.testing_information_uid == user_testing_register.testing_information_uid &&
                    w.is_cancel == false)
        .Include(i => i.testing_subject_selecteds)
        .ToList();
      var info = _adminContext.t_testing_information.FirstOrDefault(w =>
        w.testing_information_uid == user_testing_register.testing_information_uid);
      if (regis.Count != 0 || adRegis.Count != 0)
      {
        var sjList = new List<v_user_testing_subject_selected>();
        foreach (var rg in regis)
        {
          foreach (var sj in rg.user_testing_subject_selecteds)
          {
            sjList.Add(sj);
          }
        }
        foreach (var rg in adRegis)
        {
          foreach (var sj in rg.testing_subject_selecteds)
          {
            var subject = new v_user_testing_subject_selected()
            {
              subject_uid = sj.subject_uid
            };
            sjList.Add(subject);
          }
        }

        foreach (var sjSelect in user_testing_register.user_testing_subject_selecteds)
        {
          var subject = sjList.Where(w => w.subject_uid == sjSelect.subject_uid).ToList();
          if (info.number_of_times_testing == null || info.number_of_times_testing == 0)
          {
            info.number_of_times_testing = 1;
          }
          
          if (info.number_of_times_testing != null)
          {
            if (subject.Count() >= info.number_of_times_testing)
            {
              var sj = _adminContext.t_subject.FirstOrDefault(w => w.subject_uid == sjSelect.subject_uid);
              msg = "ไม่สามารถลงทะเบียนวิชา " + sj.subject_code + " ได้ รอบสอบนี้สามารถลงทะเบียนได้วิชาละ " +
                    info.number_of_times_testing + "ครั้ง";
            }
            break;
          }
        }
      }
      return msg;
    }
    
    public string CheckDate(t_user_testing_register user_testing_register)
    {
      string msg = null;
      var regis = _context.v_user_testing_register.AsNoTracking()
        .Where(w => w.user_uid == user_testing_register.user_uid &&
                    w.testing_information_uid == user_testing_register.testing_information_uid &&
                    w.is_cancel == false)
        .Include(i => i.user_testing_subject_selecteds)
        .ToList();
      var adRegis = _adminContext.v_testing_register.AsNoTracking()
        .Where(w => w.user_uid == user_testing_register.user_uid &&
                    w.testing_information_uid == user_testing_register.testing_information_uid &&
                    w.is_cancel == false)
        .Include(i => i.testing_subject_selecteds)
        .ToList();
      if (regis.Count != 0 || adRegis.Count != 0)
      {
        var sjList = new List<v_user_testing_subject_selected>();
        foreach (var rg in regis)
        {
          foreach (var sj in rg.user_testing_subject_selecteds)
          {
            sjList.Add(sj);
          }
        }
        
        foreach (var rg in adRegis)
        {
          foreach (var sj in rg.testing_subject_selecteds)
          {
            var subject = new v_user_testing_subject_selected()
            {
              subject_uid = sj.subject_uid,
              testing_datetime = sj.testing_datetime,
              period_id = sj.period_id
            };
            sjList.Add(subject);
          }
        }

        foreach (var sjSelect in user_testing_register.user_testing_subject_selecteds)
        {
          var rs = sjList
            .Where(w => w.testing_datetime == sjSelect.testing_datetime && w.period_id == sjSelect.period_id).ToList();
          if (rs.Count != 0)
          {
            msg = "วันสอบและคาบสอบชนกับที่สมัครไว้แล้ว";
          }
          break;
        }
      }
      return msg;
    }

    public async Task<int> PayLater(Guid id, ClaimsIdentity claimsIdentity)
    {
      var testRegis = _context.t_user_testing_register.FirstOrDefault(c => c.user_testing_register_uid == id);
      testRegis.is_pay_later = true;
      testRegis.payment_datetime = null;
      testRegis.payment_method = null;
      testRegis.seat_status_id = SeatStatus.Status.Get("WAITCONFIRM").id;
      _context.t_user_testing_register.Update(testRegis);
      return await _context.SaveChangesAsync();
    }
    public async Task<int> CancelRegis(Guid id, ClaimsIdentity claimsIdentity)
    {
      var userRegis = _context.t_user_testing_register.FirstOrDefault(c => c.user_testing_register_uid == id);
      if (userRegis != null)
      {
        userRegis.is_cancel = true;
        _context.t_user_testing_register.Update(userRegis);
        await _context.SaveChangesAsync();
        var testing_subject_selected = _context.t_user_testing_subject_selected.Where(c => c.user_testing_register_uid == userRegis.user_testing_register_uid).ToList();
        foreach(var rs in testing_subject_selected){
          rs.is_cancel= true;
          _context.Update(rs);
        }
        return await _context.SaveChangesAsync();
      }
      else
      {
        var regis = _adminContext.t_testing_register.FirstOrDefault(w => w.testing_register_uid == id);
        regis.is_cancel = true;
        _adminContext.t_testing_register.Update(regis);
        await _adminContext.SaveChangesAsync();
        
        var testing_subject_selected = _adminContext.t_testing_subject_selected.Where(c => c.testing_register_uid == regis.testing_register_uid).ToList();
        foreach(var rs in testing_subject_selected){
          rs.is_cancel= true;
          _adminContext.Update(rs);
        }
        return await _adminContext.SaveChangesAsync();
      }
    }

    public async Task<int> Paid(Guid id, ClaimsIdentity claimsIdentity)
    {
      var payDatetime = DateTime.Now;
      ThaiBuddhistCalendar cal = new ThaiBuddhistCalendar();
      int year;
      if (payDatetime.Month >= 10 && payDatetime.Day >= 1)
      {
        year = cal.GetYear(payDatetime.AddYears(1));
      }
      else
      {
        year = cal.GetYear(payDatetime);
      }
      var documentNoData = new document_no_data()
      {
        year_code = year.ToString()
      };
      var testRegis = _context.t_user_testing_register
        .FirstOrDefault(c => c.user_testing_register_uid == id);
      
      testRegis.is_pay_later = false;
      testRegis.payment_datetime = payDatetime;
      testRegis.payment_method = "online";
      testRegis.payment_code = await _documentNoService.GetDocumentNo(null, "PAYMENT", documentNoData);
      testRegis.seat_status_id = SeatStatus.Status.Get("CONFIRM").id;

      _context.t_user_testing_register.Update(testRegis);
      return await _context.SaveChangesAsync();
    }

  }
}
