using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;
using etest_enrollment_api.Databases;
using etest_enrollment_api.Databases.Models;
using Db = etest_enrollment_api.Databases.Db;
using etest_enrollment_api.Modules.Commons.Models;
using System;
using etest_enrollment_api.Modules.Enrollments.Databases.Models;
using etest_enrollment_api.Modules.Enrollments.Models;

namespace etest_enrollment_api.Modules.Enrollments.Services
{

  public class TestingManageService
  {
    readonly Db _context;
    readonly AdminDb _adminContext;
    readonly TestDb _testContext;

    public TestingManageService(Db context, AdminDb adminDb,TestDb testContext)
    {
      _context = context;
    _adminContext = adminDb;
    _testContext = testContext;
    }

    public async Task<List<testing_register_manage>> ListRegister(string id)
    {
        var registerList =  _context.v_user_testing_register.Where(a => a.examinee_card == id && a.is_cancel == false).ToList();
        List<testing_register_manage> rs = new List<testing_register_manage>();
        foreach(var register in registerList){
            var testInfo = await _adminContext.v_testing_information.FirstOrDefaultAsync(a => a.testing_information_uid == register.testing_information_uid);
            if (testInfo != null)
            {
                var testRegister = new testing_register_manage();
                testRegister.user_uid = register.user_uid;
                testRegister.examinee_card = register.examinee_card;
                testRegister.user_testing_register_uid = register.user_testing_register_uid;
                testRegister.academic_year_code = testInfo.academic_year_code;
                testRegister.academic_semester_code = testInfo.academic_semester_code;
                testRegister.testing_information_uid = register.testing_information_uid;
                testRegister.testing_information_name = testInfo.testing_information_name;
                testRegister.testing_information_code = testInfo.testing_information_code;
                testRegister.is_pay_later = register.is_pay_later;
                testRegister.seat_status_id = register.seat_status_id;
                testRegister.seat_status_name = SeatStatus.StatusId.Get(register.seat_status_id).description;
                
                rs.Add(testRegister);
            }
        } 
        return rs;
    }
    
    public async Task<List<testing_register_manage>> ListRegisterByUser(Guid id)
    {
        var registerList =  _context.v_user_testing_register.Where(a => a.user_uid == id && a.is_cancel == false).ToList();
        var adminRegisterList =  _adminContext.v_testing_register.Where(a => a.user_uid == id && a.is_cancel == false).ToList();
        List<testing_register_manage> rs = new List<testing_register_manage>();
        foreach (var register in registerList)
        {
            var testInfo = await _adminContext.v_testing_information.FirstOrDefaultAsync(a =>
                a.testing_information_uid == register.testing_information_uid);
            var testRegister = new testing_register_manage()
            {
                user_uid = register.user_uid,
                examinee_card = register.examinee_card,
                user_testing_register_uid = register.user_testing_register_uid,
                academic_year_code = testInfo?.academic_year_code,
                academic_semester_code = testInfo?.academic_semester_code,
                testing_information_uid = register?.testing_information_uid,
                testing_information_name = testInfo?.testing_information_name,
                testing_information_code = testInfo?.testing_information_code,
                is_pay_later = register.is_pay_later,
                seat_status_id = register.seat_status_id,
                seat_status_name = SeatStatus.StatusId.Get(register.seat_status_id).description,
                is_admin = false,
                remark = register.remark
            };
            rs.Add(testRegister);
        }
        foreach (var register in adminRegisterList)
        {
            var testInfo = await _adminContext.v_testing_information.FirstOrDefaultAsync(a =>
                a.testing_information_uid == register.testing_information_uid);
            var testRegister = new testing_register_manage()
            {
                user_uid = register.user_uid,
                examinee_card = register.examinee_card,
                user_testing_register_uid = register.testing_register_uid,
                academic_year_code = testInfo?.academic_year_code,
                academic_semester_code = testInfo?.academic_semester_code,
                testing_information_uid = register?.testing_information_uid,
                testing_information_name = testInfo?.testing_information_name,
                testing_information_code = testInfo?.testing_information_code,
                is_pay_later = false,
                seat_status_id = 5,
                seat_status_name = SeatStatus.StatusId.Get(5).description,
                is_admin = true
            };
            rs.Add(testRegister);
        }
        return rs;
    }

    public async Task<user_testing_register_respone> PreviewRegister (Guid id)
    {
        var result = new user_testing_register_respone();
        
        var testRegis = await _context.v_user_testing_register.AsNoTracking()
            .Include(t => t.user_testing_subject_selecteds)
            .FirstOrDefaultAsync(w => w.user_testing_register_uid == id);
        
        var adminTestRegis = await _adminContext.v_testing_register.AsNoTracking()
            .Include(t => t.testing_subject_selecteds)
            .FirstOrDefaultAsync(w => w.testing_register_uid == id);

        if (testRegis != null)
        {
            var candidate =
                await _context.t_candidate_type.AsNoTracking()
                    .FirstOrDefaultAsync(w => w.candidate_type_uid == testRegis.candidate_type_uid);
            
            result.user_testing_register_uid = testRegis.user_testing_register_uid;
            result.user_uid = testRegis.user_uid;
            result.full_name = testRegis.full_name;
            result.examinee_card = testRegis.examinee_card;
            result.candidate_type_uid = testRegis.candidate_type_uid;
            result.candidate_type_name_th = candidate?.candidate_type_name_th;
            result.examinee_mobile = testRegis.examinee_mobile;
            result.testing_information_uid = testRegis.testing_information_uid;
            result.testing_information_name = testRegis.testing_information_name;
            result.testing_information_code = testRegis.testing_information_code;
            result.payment_code = testRegis.payment_code ?? testRegis.ref2;
            result.testing_fee = testRegis.testing_fee;
            result.seat_status_id = testRegis.seat_status_id;
            result.seat_status_name = SeatStatus.StatusId.Get(testRegis.seat_status_id).description;
            result.is_cancel = testRegis.is_cancel;
            result.is_pay_later = testRegis.is_pay_later;
            result.payment_datetime = testRegis.payment_datetime;
            result.photo1_url = testRegis.photo1_url;
            result.photo1_name = testRegis.photo1_name;
            result.photo2_url = testRegis.photo2_url;
            result.photo2_name = testRegis.photo2_name;
            result.user_testing_subject_selecteds =
                new List<user_testing_subject_selected_respone>();

            foreach (var userTestingSubjectSelected in testRegis.user_testing_subject_selecteds)
            {
                var sj = await _adminContext.t_subject.AsNoTracking()
                    .FirstOrDefaultAsync(w => w.subject_uid == userTestingSubjectSelected.subject_uid);
                var pwd = await _testContext.v_tests.AsNoTracking().FirstOrDefaultAsync(w =>
                    w.testing_subject_selected_uid == userTestingSubjectSelected.user_testing_subject_selected_uid);
                
                result.user_testing_subject_selecteds.Add(
                    new user_testing_subject_selected_respone()
                    {
                        user_testing_subject_selected_uid =
                            userTestingSubjectSelected.user_testing_subject_selected_uid,
                        subject_uid = userTestingSubjectSelected.subject_uid,
                        subject_code = sj?.subject_code,
                        subject_name_th = sj?.subject_name_th,
                        testing_datetime = userTestingSubjectSelected.testing_datetime,
                        period_id = userTestingSubjectSelected.period_id,
                        start_time = userTestingSubjectSelected.start_time,
                        branch_name_th = userTestingSubjectSelected.branch_name_th,
                        testing_field_name_th = userTestingSubjectSelected.testing_field_name_th,
                        branch_uid = userTestingSubjectSelected.branch_uid,
                        testing_field_uid = userTestingSubjectSelected.testing_field_uid,
                        testing_center_uid = userTestingSubjectSelected.testing_center_uid,
                        testing_room_name_th = userTestingSubjectSelected.testing_room_name,
                        testing_seat = userTestingSubjectSelected.testing_seat,
                        user_testing_register_uid = userTestingSubjectSelected.user_testing_register_uid,
                        is_cancel = userTestingSubjectSelected.is_cancel,
                        remark = sj?.remark,
                        pwd = pwd?.pwd,
                        testing_register_approved = userTestingSubjectSelected.testing_register_approved,
                    });
            }
        }
        if (adminTestRegis != null)
        {
            var candidate =
                await _context.t_candidate_type.AsNoTracking()
                    .FirstOrDefaultAsync(w => w.candidate_type_uid == adminTestRegis.candidate_type_uid);
            var infor = await _adminContext.t_testing_information.AsNoTracking()
                .FirstOrDefaultAsync(w => w.testing_information_uid == adminTestRegis.testing_information_uid);

            result.user_testing_register_uid = adminTestRegis.testing_register_uid;
            result.user_uid = adminTestRegis.user_uid;
            result.full_name = adminTestRegis.examinee_full_name;
            result.examinee_card = adminTestRegis.examinee_card;
            result.candidate_type_uid = adminTestRegis.candidate_type_uid;
            result.candidate_type_name_th = candidate?.candidate_type_name_th;
            result.examinee_mobile = adminTestRegis.examinee_mobile;
            result.testing_information_uid = adminTestRegis.testing_information_uid;
            result.testing_information_name = infor?.testing_information_name;
            result.testing_information_code = infor?.testing_information_code;
            result.payment_code = adminTestRegis.payment_code;
            result.testing_fee = adminTestRegis.testing_fee;
            result.seat_status_id = 5;
            result.seat_status_name = SeatStatus.StatusId.Get(5).description;
            result.is_cancel = adminTestRegis.is_cancel;
            result.is_pay_later = false;
            result.payment_datetime = adminTestRegis.payment_datetime;
            result.photo1_url = adminTestRegis.photo1_url;
            result.photo1_name = adminTestRegis.photo1_name;
            result.photo2_url = adminTestRegis.photo2_url;
            result.photo2_name = adminTestRegis.photo2_name;

            result.user_testing_subject_selecteds =
                new List<user_testing_subject_selected_respone>();

            foreach (var userTestingSubjectSelected in adminTestRegis.testing_subject_selecteds)
            {
                var sj = await _adminContext.t_subject.AsNoTracking()
                    .FirstOrDefaultAsync(w => w.subject_uid == userTestingSubjectSelected.subject_uid);
                
                var pwd = await _testContext.v_tests.AsNoTracking().FirstOrDefaultAsync(w =>
                    w.testing_subject_selected_uid == userTestingSubjectSelected.testing_subject_selected_uid);

                result.user_testing_subject_selecteds.Add(
                    new user_testing_subject_selected_respone()
                    {
                        user_testing_subject_selected_uid =
                            userTestingSubjectSelected.testing_subject_selected_uid,
                        subject_uid = userTestingSubjectSelected.subject_uid,
                        subject_code = sj?.subject_code,
                        subject_name_th = sj?.subject_name_th,
                        testing_datetime = userTestingSubjectSelected.testing_datetime,
                        period_id = userTestingSubjectSelected.period_id,
                        start_time = userTestingSubjectSelected.start_time,
                        branch_name_th = userTestingSubjectSelected.branch_name_th,
                        testing_field_name_th = userTestingSubjectSelected.testing_field_name_th,
                        branch_uid = userTestingSubjectSelected.branch_uid,
                        testing_field_uid = userTestingSubjectSelected.testing_field_uid,
                        testing_center_uid = userTestingSubjectSelected.testing_center_uid,
                        testing_room_name_th = userTestingSubjectSelected.testing_room_name_th,
                        testing_seat = userTestingSubjectSelected.testing_seat,
                        user_testing_register_uid = userTestingSubjectSelected.testing_register_uid,
                        is_cancel = userTestingSubjectSelected.is_cancel,
                        remark = sj?.remark,
                        pwd = pwd?.pwd,
                        testing_register_approved = userTestingSubjectSelected.testing_register_approved,
                    });
            }
        }

        return result;

    }

    public  async Task<List<testing_subject_manage>> PreviewSubject(Guid id){
        var listSubject = _context.v_user_testing_subject_selected.
            Where(a => a.user_testing_register_uid == id && a.is_cancel == false).ToList();
        if (listSubject.Count != 0)
        {
            List<testing_subject_manage> rs = new List<testing_subject_manage>();
            if (listSubject.Count != 0)
            {
                foreach (var subject in listSubject)
                {
                    var sj = _adminContext.t_subject.FirstOrDefault(w => w.subject_uid == subject.subject_uid);
                    var testSubject = new testing_subject_manage();
                    testSubject.user_testing_register_uid = subject.user_testing_register_uid;
                    testSubject.user_testing_subject_selected_uid = subject.user_testing_subject_selected_uid;
                    testSubject.testing_information_uid = subject.testing_information_uid;
                    testSubject.testing_information_name = subject.testing_information_name;
                    testSubject.testing_information_code = subject.testing_information_code;
                    testSubject.branch_uid = subject.branch_uid;
                    testSubject.testing_field_uid = subject.testing_field_uid;
                    testSubject.testing_center_uid = subject.testing_center_uid;
                    testSubject.subject_uid = subject.subject_uid;
                    testSubject.subject_code = sj?.subject_code;
                    testSubject.subject_name_th = sj?.subject_name_th;
                    testSubject.testing_datetime = subject.testing_datetime;
                    testSubject.period_id = subject.period_id;
                    testSubject.start_time = subject.start_time;

                    rs.Add(testSubject);
                }
            }

            return rs;
        }
        else
        {
            return null;
        }
    }

    public async Task<t_manage_testing> ChangeTestingDate(t_manage_testing manage_testing, ClaimsIdentity claimsIdentity)
    {
        var chkManageTesting = _context.v_manage_testing.AsNoTracking().FirstOrDefault(w =>
            w.user_testing_subject_selected_uid == manage_testing.user_testing_subject_selected_uid &&
            w.seat_status_id == 4);
        if(chkManageTesting != null)
            throw new Exception("คำขอของคุณอยู่ระหว่างการอนุมัติ ไม่สามารถสร้างคำขอใหม่ได้");
        
        var regis = _context.t_user_testing_register.FirstOrDefault(w =>
            w.user_testing_register_uid == manage_testing.user_testing_register_uid);

        manage_testing.manage_testing_uid = Guid.NewGuid();
        manage_testing.testing_information_uid = regis?.testing_information_uid; 
        manage_testing.seat_status_id =  SeatStatus.Status.Get("WAITCONFIRMDATE").id;
        await _context.t_manage_testing.AddAsync(manage_testing);
        await _context.SaveChangesAsync();
        
        if (regis != null)
        {
            manage_testing.seat_status_id =  SeatStatus.Status.Get("WAITCONFIRMDATE").id;
            _context.t_user_testing_register.Update(regis);
            await _context.SaveChangesAsync();
        }
        return manage_testing;
    }
    
    public async Task<List<v_manage_testing>> GetChangeTestingDate (Guid id){

        return _context.v_manage_testing.Where(a => a.user_uid == id).ToList();

    }
    
    public async Task<int> UpdateTestingRegisterApproved(Guid id)
    {
        var utss = await _context.t_user_testing_subject_selected.AsNoTracking()
            .FirstOrDefaultAsync(w => w.user_testing_subject_selected_uid == id);
        var tss = await _adminContext.t_testing_subject_selected.AsNoTracking()
            .FirstOrDefaultAsync(w => w.testing_subject_selected_uid == id);
        if (utss != null)
        {
            utss.testing_register_approved = true;
            _context.t_user_testing_subject_selected.Update(utss);
            await _context.SaveChangesAsync();
        }
        if (tss != null)
        {
            tss.testing_register_approved = true;
            _adminContext.t_testing_subject_selected.Update(tss);
            await _adminContext.SaveChangesAsync();
        }
        return 1;
    }
  }


}
