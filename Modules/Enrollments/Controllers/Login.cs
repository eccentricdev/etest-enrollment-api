using System;
using System.Security.Claims;
using System.Threading.Tasks;
using etest_enrollment_api.Databases.Models;
using etest_enrollment_api.Modules.Commons.Models;
using etest_enrollment_api.Modules.Commons.Services;
using etest_enrollment_api.Modules.Enrollments.Models;
using etest_enrollment_api.Modules.Enrollments.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace etest_enrollment_api.Modules.Enrollments.Controllers
{
    public class LoginController : Controller
    {
        readonly LoginService _loginService;

        public LoginController(LoginService loginService)
        {
            this._loginService = loginService;
        }
        
        [HttpPost, Route("api/v1/enrollment/user_login")]
        [ApiExplorerSettings(GroupName = "enrollment")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<user_respone>> UserLogin(
            [FromBody] login_request _login_request)
        {
            var getResult = await _loginService.UserLogin(_login_request);
            if (getResult == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(getResult);
            }

        }
    }
}