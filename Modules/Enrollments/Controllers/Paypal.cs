using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Linq;
using etest_enrollment_api.Databases;
using etest_enrollment_api.Modules.Enrollments.Databases.Models;
using PayPalCheckoutSdk.Core;
using PayPalCheckoutSdk.Orders;
using PayPalHttp;
using SeventyOneDev.Core.Document;
using SeventyOneDev.Core.Document.Services;

namespace etest_paypal_integrate.Controllers
{

    public class Paypal: ControllerBase
    {

        public IConfiguration Configuration { get;set; }
        Db _context;
        private readonly DocumentNoService _documentNoService;
        public Paypal(
            IConfiguration configuration,
            Db context,
            DocumentNoService documentNoService
            
        ){
            this.Configuration = configuration;

            this._context = context;
            this._documentNoService = documentNoService;
        }

        [HttpGet, Route("api/get")]
        public async Task<List<t_user_testing_register>> getAll(){
            return  this._context.t_user_testing_register.ToList();
        }

        [HttpGet, Route("api/capture_transcation/{order_id}/{user_testing_register_uid}")]

        public async Task<HttpResponse> CaptureOrder(string order_id, Guid user_testing_register_uid)
        {
            try
            {
                var request = new OrdersCaptureRequest(order_id);
                request.Prefer("return=representation");
                request.RequestBody(new OrderActionRequest());
                var response = await new PayPalHttpClient(
                    new SandboxEnvironment(
                        Configuration["paypal:CLIENTID"],
                        Configuration["paypal:SRECETKEY"]
                    )
                ).Execute(request);
                
                var payDatetime = DateTime.Now;
                ThaiBuddhistCalendar cal = new ThaiBuddhistCalendar();
                int year;
                if (payDatetime.Month >= 10 && payDatetime.Day >= 1)
                {
                    year = cal.GetYear(payDatetime.AddYears(1));
                }
                else
                {
                    year = cal.GetYear(payDatetime);
                }
                var documentNoData = new document_no_data()
                {
                    year_code = year.ToString()
                };
                
                var user = this._context.t_user_testing_register.SingleOrDefault(u =>
                    u.user_testing_register_uid == user_testing_register_uid);
                user.is_pay_later = false;
                user.payment_datetime = payDatetime;
                user.payment_method = "PAYPAL";
                user.payment_code = await _documentNoService.GetDocumentNo(null, "PAYMENT", documentNoData);
                user.seat_status_id = SeatStatus.Status.Get("CONFIRM").id;
                _context.Update(user);
                await _context.SaveChangesAsync();

                return response;
            }
            catch (System.Exception)
            {

                throw;
            }

        }



    }
}