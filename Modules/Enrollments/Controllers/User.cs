using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SeventyOneDev.Utilities;
using etest_enrollment_api.Databases.Models;
using etest_enrollment_api.Modules.Commons.Models;
using etest_enrollment_api.Modules.Commons.Services;
using etest_enrollment_api.Modules.Enrollments.Databases.Models;
using etest_enrollment_api.Modules.Enrollments.Models;
using etest_enrollment_api.Modules.Enrollments.Services;

namespace etest_enrollment_api.Modules.Enrollments.Controllers
{
  public class UserController : Controller
  {
    readonly UserService _userService;

    public UserController(UserService userService)
    {
      this._userService = userService;
    }

    [HttpGet, Route("api/v1/enrollment/users")]
    [ApiExplorerSettings(GroupName = "enrollment")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<List<v_user>>> GetUsers(
      [FromQuery] v_user _user = null)
    {
      if (EFExtension.IsNullOrEmpty(_user))
      {
        var listResult = await _userService.ListUser(null);
        return Ok(listResult);
      }
      else
      {
        var getResult = await _userService.ListUser(_user);
        if (getResult.Count() == 0)
        {
          return Ok(new List<v_user>());
        }
        else
        {
          return getResult.ToList();
        }

      }

    }

    [HttpGet, Route("api/v1/enrollment/users/{user_uid}")]
    [ApiExplorerSettings(GroupName = "enrollment")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<user_respone>> GetUser([FromRoute] Guid user_uid)
    {

      var getResult = await _userService.GetUser(user_uid);
      if (getResult == null)
      {
        return NotFound();
      }
      else
      {
        return Ok(getResult);
      }
    }
    
    [HttpGet, Route("api/v1/enrollment/users_by_card/{examinee_card}")]
    [ApiExplorerSettings(GroupName = "enrollment")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<user_respone>> GetUserByCard([FromRoute] string examinee_card)
    {

      var getResult = await _userService.GetUserByCard(examinee_card);
      if (getResult == null)
      {
        return NotFound();
      }
      else
      {
        return Ok(getResult);
      }
    }

    [HttpPost, Route("api/v1/enrollment/users")]
    [ApiExplorerSettings(GroupName = "enrollment")]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<t_user>> NewUser(
      [FromBody] t_user _user)
    {
      try
      {
        var newResult =
          await _userService.NewUser(_user, User.Identity as ClaimsIdentity);
        return Created("/api/v1/enrollment/users/" + newResult.user_uid.ToString(), newResult);
      }
      // catch(DuplicateException ex)
      // {
      //     response = ApiHelper.ApiError<v_user>(ex.ErrorId,ex.Message);
      // }
      catch (Exception ex)
      {
        Console.WriteLine("Create user failed: " + ex.Message);
        return BadRequest(ex.Message);
      }

    }

    [HttpPut, Route("api/v1/enrollment/users")]
    [ApiExplorerSettings(GroupName = "enrollment")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult> UpdateUser([FromBody] t_user _user)
    {
      try
      {
        var result = await _userService.UpdateUser(_user, User.Identity as ClaimsIdentity);
        if(result == 0)
        {
          return BadRequest();
        }
        else
        {
           return NoContent();
        }
      }
      // catch(DuplicateException ex)
      // {
      //     response = ApiHelper.ApiError<v_user>(ex.ErrorId,ex.Message);
      // }
      catch (Exception ex)
      {
        Console.WriteLine("Update user failed: " + ex.Message);
        return BadRequest();
      }

    }

    [HttpDelete, Route("api/v1/enrollment/users/{user_uid}")]
    [ApiExplorerSettings(GroupName = "enrollment")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult> DeleteUser([FromRoute] Guid user_uid)
    {
      try
      {
        var _deleteCount =
          await _userService.DeleteUser(user_uid, User.Identity as ClaimsIdentity);
        if (_deleteCount == 1)
        {
          return NoContent();
        }
        else
        {
          return NotFound();
        }
      }
      // catch(DuplicateException ex)
      // {
      //     response = ApiHelper.ApiError<v_user>(ex.ErrorId,ex.Message);
      // }
      catch (Exception ex)
      {
        Console.WriteLine("Delete user failed: " + ex.Message);
        return BadRequest();
      }
    }
    
     
    [HttpPost, Route("api/v1/enrollment/chk_student")]
    [ApiExplorerSettings(GroupName = "enrollment")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<user_respone>> ChkStudentLogin(
      [FromQuery] string student_code,[FromQuery] string id_card)
    {
      var getResult = await _userService.ChkStudentLogin(student_code,id_card);
      if (getResult == null)
      {
        return NotFound();
      }
      else
      {
        return Ok(getResult);
      }

    }
  }
}
