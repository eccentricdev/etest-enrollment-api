using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SeventyOneDev.Utilities;
using etest_enrollment_api.Databases.Models;
using etest_enrollment_api.Modules.Commons.Services;
using etest_enrollment_api.Modules.Enrollments.Services;

namespace etest_enrollment_api.Modules.Enrollments.Controllers
{
  public class ResetPasswordController : Controller
  {
    readonly ResetPasswordService _resetPasswordService;

    public ResetPasswordController(ResetPasswordService resetPasswordService)
    {
      this._resetPasswordService = resetPasswordService;
    }

    [HttpPost, Route("api/v1/enrollment/reset_password/{student_code}")]
    [ApiExplorerSettings(GroupName = "enrollment")]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult> ResetPassword([FromRoute] string student_code)
    {
      try
      {
        var result = await _resetPasswordService.ResetPassword(student_code, User.Identity as ClaimsIdentity);
         if(result == 0)
        {
          return BadRequest();
        }
        else
        {
           return NoContent();
        }
      }
      // catch(DuplicateException ex)
      // {
      //     response = ApiHelper.ApiError<v_user>(ex.ErrorId,ex.Message);
      // }
      catch (Exception ex)
      {
        Console.WriteLine(" Reset assword failed: " + ex.Message);
        return BadRequest();
      }

    }
  }
}
