using System.Collections.Generic;
using System.Threading.Tasks;
using etest_enrollment_api.Modules.Enrollments.Models;
using etest_enrollment_api.Modules.Enrollments.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace etest_enrollment_api.Modules.Enrollments.Controllers
{
    [SwaggerTag("สำหรับแสดงตารางสอบ")]
    public class TestingScheduleController : Controller
    {
        readonly TestingScheduleService _testingScheduleService;

        public TestingScheduleController(TestingScheduleService testingScheduleService)
        {
            this._testingScheduleService = testingScheduleService;
        }
        
        [HttpGet, Route("api/v1/enrollment/testing_schedule/{student_code}")]
        [ApiExplorerSettings(GroupName = "enrollment")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<List<testing_schedule>>> GetManageReceipt ([FromRoute] string student_code)
        {
            var getResult = await _testingScheduleService.ListTestingResult(student_code);
            if (getResult == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(getResult);
            }
        }
    }
}