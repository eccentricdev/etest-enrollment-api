using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using etest_enrollment_api.Modules.Commons.Services;
using etest_enrollment_api.Modules.Commons.Models;
using System;
using System.Text.Json;
using etest_enrollment_api.Modules.Enrollments.Models;
using etest_enrollment_api.Modules.Enrollments.Services;

namespace etest_enrollment_api.Modules.Enrollments.Controllers
{
  public class PaymentController : Controller
  {
    readonly PaymentDetailService _paymentDetailService;

    public PaymentController(PaymentDetailService paymentDetailService)
    {
      this._paymentDetailService = paymentDetailService;
    }

    [HttpGet, Route("api/v1/enrollment/payment/{user_uid}")]
    [ApiExplorerSettings(GroupName = "enrollment")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public ActionResult<List<payment_detail>> GetPayments([FromRoute] Guid user_uid)
    {
      var getResult = _paymentDetailService.ListPayment(user_uid);
      if (getResult.Count() == 0)
      {
        return Ok(new List<payment_detail>());
      }
      else
      {
        return getResult.ToList();
      }

    }

    [HttpGet, Route("api/v1/enrollment/payment_details/{user_testing_register_uid}")]
    [ApiExplorerSettings(GroupName = "enrollment")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public ActionResult<List<testing_detail>> PaymentDetails ([FromRoute] Guid user_testing_register_uid)
    {
      var getResult = _paymentDetailService.ListPaymentDetail(user_testing_register_uid);
      if (getResult.Count() == 0)
      { 
        return Ok(new List<testing_detail>());
      }
      else 
      { 
        return getResult.ToList();
      }

    }
    
    
    // [HttpPost, Route("api/v1/enrollment/payment_callback")]
    // [ApiExplorerSettings(GroupName = "enrollment")]
    // [ProducesResponseType(StatusCodes.Status200OK)]
    // [ProducesResponseType(StatusCodes.Status400BadRequest)]
    // [ProducesResponseType(StatusCodes.Status404NotFound)]
    // public ActionResult<payment_callback> PaymentDetails (
    //   [FromForm] payment_callback request)
    // {
    //   try
    //   {
    //     var newResult =  _paymentDetailService.ListPaymentCallBack(request);
    //     if (newResult.Result == null)
    //     {
    //       return NotFound();
    //     }
    //     else
    //     {
    //       return Ok(newResult.Result);
    //
    //     }
    //   }
    //   catch (Exception ex)
    //   {
    //     Console.WriteLine(" payment_callback failed: " + ex.Message);
    //     return BadRequest(ex.Message);
    //   }
    //
    // }

    
    [HttpPost, Route("api/v1/enrollment/payment_callback")]
    [ApiExplorerSettings(GroupName = "enrollment")]
    public ActionResult CreditCardPayment([FromForm] payment_callback request)
    {
      Console.WriteLine("Response:" + JsonSerializer.Serialize(request));
      
      var newResult =  _paymentDetailService.ListPaymentCallBack(request).Result;

      if (newResult != null)
      {
        return Redirect("https://ru.71dev.com/register/menu/payment-later");
        // return Redirect("https://exam.ru.ac.th/reg-stu/menu/payment-later");
      }
      else
      {
        return BadRequest();
      }
    }

  }
}
