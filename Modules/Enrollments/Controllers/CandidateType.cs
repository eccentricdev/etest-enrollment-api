using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SeventyOneDev.Utilities;
using etest_enrollment_api.Databases.Models;
using etest_enrollment_api.Modules.Commons.Services;
using etest_enrollment_api.Modules.Enrollments.Databases.Models;
using etest_enrollment_api.Modules.Enrollments.Services;

namespace etest_enrollment_api.Modules.Enrollments.Controllers
{
  public class CandidateTypeController : Controller
  {
    readonly CandidateTypeService _candidateTypeService;

    public CandidateTypeController(CandidateTypeService candidateTypeService)
    {
      this._candidateTypeService = candidateTypeService;
    }

    [HttpGet, Route("api/v1/enrollment/candidate_types")]
    [ApiExplorerSettings(GroupName = "enrollment")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<List<v_candidate_type>>> GetCandidateTypes(
      [FromQuery] v_candidate_type _candidate_type = null)
    {
      if (EFExtension.IsNullOrEmpty(_candidate_type))
      {
        var listResult = await _candidateTypeService.ListCandidateType(null);
        return Ok(listResult);
      }
      else
      {
        var getResult = await _candidateTypeService.ListCandidateType(_candidate_type);
        if (getResult.Count() == 0)
        {
          return Ok(new List<v_candidate_type>());
        }
        else
        {
          return getResult.ToList();
        }

      }

    }

    [HttpGet, Route("api/v1/enrollment/candidate_types/{candidate_type_uid}")]
    [ApiExplorerSettings(GroupName = "enrollment")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<v_candidate_type>> GetCandidateType([FromRoute] Guid candidate_type_uid)
    {

      var getResult = await _candidateTypeService.GetCandidateType(candidate_type_uid);
      if (getResult == null)
      {
        return NotFound();
      }
      else
      {
        return Ok(getResult);
      }
    }

    [HttpPost, Route("api/v1/enrollment/candidate_types")]
    [ApiExplorerSettings(GroupName = "enrollment")]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<t_candidate_type>> NewCandidateType(
      [FromBody] t_candidate_type _candidate_type)
    {
      try
      {
        var newResult =
          await _candidateTypeService.NewCandidateType(_candidate_type, User.Identity as ClaimsIdentity);
        return Created("/api/v1/enrollment/candidate_types/" + newResult.candidate_type_uid.ToString(), newResult);
      }
      // catch(DuplicateException ex)
      // {
      //     response = ApiHelper.ApiError<v_candidate_type>(ex.ErrorId,ex.Message);
      // }
      catch (Exception ex)
      {
        Console.WriteLine("Create candidate_type failed: " + ex.Message);
        return BadRequest();
      }

    }

    [HttpPut, Route("api/v1/enrollment/candidate_types")]
    [ApiExplorerSettings(GroupName = "enrollment")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult> UpdateCandidateType([FromBody] t_candidate_type _candidate_type)
    {
      try
      {
        var result =
          await _candidateTypeService.UpdateCandidateType(_candidate_type, User.Identity as ClaimsIdentity);
        return NoContent();
      }
      // catch(DuplicateException ex)
      // {
      //     response = ApiHelper.ApiError<v_candidate_type>(ex.ErrorId,ex.Message);
      // }
      catch (Exception ex)
      {
        Console.WriteLine("Update candidate_type failed: " + ex.Message);
        return BadRequest();
      }

    }

    [HttpDelete, Route("api/v1/enrollment/candidate_types/{candidate_type_uid}")]
    [ApiExplorerSettings(GroupName = "enrollment")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult> DeleteCandidateType([FromRoute] Guid candidate_type_uid)
    {
      try
      {
        var _deleteCount =
          await _candidateTypeService.DeleteCandidateType(candidate_type_uid, User.Identity as ClaimsIdentity);
        if (_deleteCount == 1)
        {
          return NoContent();
        }
        else
        {
          return NotFound();
        }

      }
      // catch(DuplicateException ex)
      // {
      //     response = ApiHelper.ApiError<v_candidate_type>(ex.ErrorId,ex.Message);
      // }
      catch (Exception ex)
      {
        Console.WriteLine("Delete candidate_type failed: " + ex.Message);
        return BadRequest();
      }
    }
  }
}
