using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using etest_enrollment_api.Modules.Enrollments.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.Annotations;

namespace etest_enrollment_api.Modules.Enrollments.Controllers
{
  [SwaggerTag("upload")]
  public class UploadController : Controller
  {
   // readonly UploadService _subjectService;
    private readonly application_setting _applicationSetting;

    public UploadController(IOptions<application_setting> applicationSetting)
    {
     // this._subjectService = subjectService;
      _applicationSetting = applicationSetting.Value;
    }


    [HttpPost, Route("api/upload/upload")]
    [ApiExplorerSettings(GroupName = "enrollment")]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<List<upload_document>> DocumentUpload()
    {
      var files = Request.Form.Files;
      if (!Directory.Exists(_applicationSetting.document_path))
      {
        Directory.CreateDirectory(_applicationSetting.document_path);
      }

      var uploads = Path.Combine(_applicationSetting.document_path);
      List<upload_document> fileNames = new List<upload_document>();
      foreach (var file in files)
      {
        if (file.Length > 0)
        {
          String newFileName = Guid.NewGuid().ToString() + file.FileName.Substring(file.FileName.LastIndexOf('.'), 4);
          using (var fileStream = new FileStream(Path.Combine(uploads, newFileName), FileMode.Create))
          {
            await file.CopyToAsync(fileStream);
          }

          fileNames.Add(new upload_document()
          {
            file_name = _applicationSetting.document_url + newFileName, name = file.FileName,
            mime_type = file.ContentType
          });
        }
      }

      return fileNames;
    }
  }
}
