using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SeventyOneDev.Utilities;
using etest_enrollment_api.Databases.Models;
using etest_enrollment_api.Modules.Commons.Services;
using etest_enrollment_api.Modules.Commons.Models;
using System.IO;
using etest_enrollment_api.Modules.Enrollments.Databases.Models;
using etest_enrollment_api.Modules.Enrollments.Models;
using etest_enrollment_api.Modules.Enrollments.Services;

namespace etest_enrollment_api.Modules.Enrollments.Controllers
{
  public class TestingManageController : Controller
  {
    readonly TestingManageService _testingManageService;

    public TestingManageController(TestingManageService testingManageService)
    {
      this._testingManageService = testingManageService;
    }

    // [HttpGet, Route("api/v1/enrollment/testing_manages/{examinee_card}")]
    // [ApiExplorerSettings(GroupName = "enrollment")]
    // [ProducesResponseType(StatusCodes.Status200OK)]
    // [ProducesResponseType(StatusCodes.Status404NotFound)]
    // public async Task<ActionResult<testing_register_manage>> GeListRegister([FromRoute] string examinee_card)
    // {
    //
    //   var getResult = await _testingManageService.ListRegister(examinee_card);
    //   if (getResult == null)
    //   {
    //     return NotFound();
    //   }
    //   else
    //   {
    //     return Ok(getResult.ToList());
    //   }
    // }
    
    [HttpGet, Route("api/v1/enrollment/testing_manages_by_user_uid/{user_uid}")]
    [ApiExplorerSettings(GroupName = "enrollment")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<testing_register_manage>> GeListRegister([FromRoute] Guid user_uid)
    {

      var getResult = await _testingManageService.ListRegisterByUser(user_uid);
      if (getResult == null)
      {
        return NotFound();
      }
      else
      {
        return Ok(getResult.ToList());
      }
    }

    [HttpGet, Route("api/v1/enrollment/testing_subject/{user_testing_register_uid}")]
    [ApiExplorerSettings(GroupName = "enrollment")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<testing_subject_manage>> PerviewSubject([FromRoute] Guid user_testing_register_uid)
    {

      var getResult = await _testingManageService.PreviewSubject(user_testing_register_uid);
      if (getResult == null)
      {
        return NotFound();
      }
      else
      {
        return Ok(getResult.ToList());
      }
    }

    [HttpGet, Route("api/v1/enrollment/preview_testing_register/{user_testing_register_uid}")]
    [ApiExplorerSettings(GroupName = "enrollment")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<v_user_testing_register>> PerviewRegister ([FromRoute] Guid user_testing_register_uid)
    {

      var getResult = await _testingManageService.PreviewRegister(user_testing_register_uid);
      if (getResult == null)
      {
        return NotFound();
      }
      else
      {
        return Ok(getResult);
      }
    }

    [HttpPost, Route("api/v1/enrollment/change_testing_date")]
    [ApiExplorerSettings(GroupName = "enrollment")]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<t_manage_testing>> ChangeTestingDate(
      [FromBody] t_manage_testing _manage_testing)
    {
      try
      {
        var newResult =
          await _testingManageService.ChangeTestingDate(_manage_testing, User.Identity as ClaimsIdentity);
        return Created("/api/v1/enrollment/change_testing_date/" + newResult.manage_testing_uid.ToString(), newResult);
      }
      // catch(DuplicateException ex)
      // {
      //     response = ApiHelper.ApiError<v_testing_result>(ex.ErrorId,ex.Message);
      // }
      catch (Exception ex)
      {
        Console.WriteLine("Create testing_result failed: " + ex.Message);
        return BadRequest(ex.Message);
      }

    }

    [HttpGet, Route("api/v1/enrollment/manage_testing/{user_uid}")]
    [ApiExplorerSettings(GroupName = "enrollment")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<v_manage_testing>> GetChangeTestingDate ([FromRoute] Guid user_uid)
    {

      var getResult = await _testingManageService.GetChangeTestingDate(user_uid);
      if (getResult == null)
      {
        return NotFound();
      }
      else
      {
        return Ok(getResult);
      }
    }
    
    [HttpPut, Route("api/v1/enrollment/testing_register_approved/{user_testing_subject_selected_uid}")]
    [ApiExplorerSettings(GroupName = "enrollment")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult> UpdateTestingRegisterApproved([FromRoute] Guid user_testing_subject_selected_uid)
    {
      try
      {
        var result =
          await _testingManageService.UpdateTestingRegisterApproved(user_testing_subject_selected_uid);
        if(result == 1)
        {
          return NoContent();
        }
        else
        {
          return BadRequest();
        }
      }
      // catch(DuplicateException ex)
      // {
      //     response = ApiHelper.ApiError<v_candidate_type>(ex.ErrorId,ex.Message);
      // }
      catch (Exception ex)
      {
        Console.WriteLine("Update testing_register_approved failed: " + ex.Message);
        return BadRequest();
      }

    }
  }
}
