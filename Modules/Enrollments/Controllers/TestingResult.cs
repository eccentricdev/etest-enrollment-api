using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using etest_enrollment_api.Modules.Enrollments.Models;
using etest_enrollment_api.Modules.Enrollments.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SeventyOneDev.Utilities;
using Swashbuckle.AspNetCore.Annotations;

namespace etest_enrollment_api.Modules.Enrollments.Controllers
{
    [SwaggerTag("สำหรับแสดงผลการสอบ")]
    public class TestingResultController : Controller
    {
        readonly TestingResultService _testingResultService;
        readonly IMapper _mapper;

        public TestingResultController(TestingResultService testingResultService,IMapper mapper)
        {
            this._testingResultService = testingResultService;
            this._mapper = mapper;
        }

        [HttpGet, Route("api/v1/enrollment/testing_results")]
        [ApiExplorerSettings(GroupName = "enrollment")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<List<testing_result_respone>>> GetTestingResults(
            [FromQuery] testing_result_request _testing_result = null)
        {
            // var request = _mapper.Map<testing_result_respone>(_testing_result);
            // if (EFExtension.IsNullOrEmpty(request))
            // {
            //     var listResult = await _testingResultService.ListTestingResult(null);
            //     return Ok(listResult);
            // }
            // else
            // {
                var getResult = await _testingResultService.ListTestingResult(_testing_result);
                if (getResult.Count() == 0)
                {
                    return Ok(new List<testing_result_respone>());
                }
                else
                {
                    return getResult.ToList();
                }

            // }
        }
    }
}