using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SeventyOneDev.Utilities;
using etest_enrollment_api.Databases.Models;
using etest_enrollment_api.Modules.Commons.Services;
using etest_enrollment_api.Modules.Enrollments.Databases.Models;
using etest_enrollment_api.Modules.Enrollments.Services;

namespace etest_enrollment_api.Modules.Enrollments.Controllers
{

  public class TestingRegisterController : Controller
  {
    readonly TestingRegisterService _testingRegisterService;

    public TestingRegisterController(TestingRegisterService TestingRegisterService)
    {
      this._testingRegisterService = TestingRegisterService;
    }

    [HttpGet, Route("api/v1/enrollment/testing_registers")]
    [ApiExplorerSettings(GroupName = "enrollment")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<List<v_user_testing_register>>> GetTestingRegisters(
      [FromQuery] v_user_testing_register _user_testing_register = null)
    {
      if (EFExtension.IsNullOrEmpty(_user_testing_register))
      {
        var listResult = await _testingRegisterService.ListTestingRegister(null);
        return Ok(listResult);
      }
      else
      {
        var getResult = await _testingRegisterService.ListTestingRegister(_user_testing_register);
        if (getResult.Count() == 0)
        {
          return Ok(new List<v_user_testing_register>());
        }
        else
        {
          return getResult.ToList();
        }

      }

    }

    [HttpGet, Route("api/v1/enrollment/testing_registers/{testing_register_uid}")]
    [ApiExplorerSettings(GroupName = "enrollment")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<v_user_testing_register>> GetTestingRegister([FromRoute] Guid testing_register_uid)
    {

      var getResult = await _testingRegisterService.GetTestingRegister(testing_register_uid);
      if (getResult == null)
      {
        return NotFound();
      }
      else
      {
        return Ok(getResult);
      }
    }

    [HttpPost, Route("api/v1/enrollment/testing_registers")]
    [ApiExplorerSettings(GroupName = "enrollment")]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<t_user_testing_register>> NewTestingRegister(
      [FromBody] t_user_testing_register _user_testing_register)
    {
      try
      {
        var newResult =
          await _testingRegisterService.NewTestingRegister(_user_testing_register, User.Identity as ClaimsIdentity);
        if (newResult == null)
        {
          return NotFound();
        }
        else
        {
          return Created("/api/v1/enrollment/testing_registers/" + newResult.user_testing_register_uid.ToString(), newResult);

        }
      }
      // catch(DuplicateException ex)
      // {
      //     response = ApiHelper.ApiError<v_testing_register>(ex.ErrorId,ex.Message);
      // }
      catch (Exception ex)
      {
        Console.WriteLine("Create testing_register failed: " + ex.Message);
        return BadRequest(ex.Message);
      }

    }

    [HttpPut, Route("api/v1/enrollment/pay_later/{testing_register_uid}")]
    [ApiExplorerSettings(GroupName = "enrollment")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult> PayLater([FromRoute] Guid testing_register_uid)
    {
      try
      {
        var result =
          await _testingRegisterService.PayLater(testing_register_uid, User.Identity as ClaimsIdentity);
        return NoContent();
      }
      // catch(DuplicateException ex)
      // {
      //     response = ApiHelper.ApiError<v_testing_register>(ex.ErrorId,ex.Message);
      // }
      catch (Exception ex)
      {
        Console.WriteLine("Update testing_register failed: " + ex.Message);
        return BadRequest();
      }

    }

    [HttpPut, Route("api/v1/enrollment/cancel/{testing_register_uid}")]
    [ApiExplorerSettings(GroupName = "enrollment")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult> CancleRegis([FromRoute] Guid testing_register_uid)
    {
      try
      {
        var result =
          await _testingRegisterService.CancelRegis(testing_register_uid, User.Identity as ClaimsIdentity);
        return NoContent();
      }
      // catch(DuplicateException ex)
      // {
      //     response = ApiHelper.ApiError<v_testing_register>(ex.ErrorId,ex.Message);
      // }
      catch (Exception ex)
      {
        Console.WriteLine("Update testing_register failed: " + ex.Message);
        return BadRequest();
      }

    }
    
    [HttpPut, Route("api/v1/enrollment/paid/{testing_register_uid}")]
    [ApiExplorerSettings(GroupName = "enrollment")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult> Paid([FromRoute] Guid testing_register_uid)
    {
      try
      {
        var result =
          await _testingRegisterService.Paid(testing_register_uid, User.Identity as ClaimsIdentity);
        return NoContent();
      }
      // catch(DuplicateException ex)
      // {
      //     response = ApiHelper.ApiError<v_testing_register>(ex.ErrorId,ex.Message);
      // }
      catch (Exception ex)
      {
        Console.WriteLine("Update testing_register failed: " + ex.Message);
        return BadRequest();
      }

    }
  }
}
