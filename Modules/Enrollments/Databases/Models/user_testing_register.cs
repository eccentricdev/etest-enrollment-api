using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using etest_enrollment_api.Modules.Commons.Models;
using SeventyOneDev.Utilities.Attributes;

namespace etest_enrollment_api.Modules.Enrollments.Databases.Models
{
  public class user_testing_register:base_table
  {
    [Key]
    public Guid? user_testing_register_uid {get;set;}
    public Guid? user_uid {get;set;}
    public string examinee_card {get;set;}
    public Guid? candidate_type_uid {get;set;}
    public string examinee_mobile {get;set;}
    public string email {get;set;}
    public Guid? testing_information_uid {get;set;}
    public string payment_code {get;set;}
    public string testing_fee {get;set;}
    public short? seat_status_id {get;set;}
    public Boolean? is_cancel {get;set;}
    public Boolean? is_pay_later {get;set;}
    public DateTime? payment_datetime {get;set;}
    public string photo1_url { get; set; }
    public string photo1_name { get; set; }
    public string photo2_url { get; set; }
    public string photo2_name { get; set; }
    public string ref1 {get;set;}
    public string ref2 {get;set;}
    public int? bill_status_id {get;set;}
    public string payment_method { get; set; }
    public string document_url { get; set; }
    public string document_name { get; set; }
    public string document_status { get; set; }
    public string remark { get; set; }
  }

  public class t_user_testing_register : user_testing_register
  {
    public List<t_user_testing_subject_selected> user_testing_subject_selecteds { get; set; }
  }

  public class v_user_testing_register : user_testing_register
  {
    [NotMapped]
    public string search { get; set; }
    public string testing_information_code {get;set;}
    public string testing_information_name {get;set;}
    public string student_code {get;set;}
    public string full_name {get;set;}
    public List<v_user_testing_subject_selected> user_testing_subject_selecteds { get; set; }

  }
}
