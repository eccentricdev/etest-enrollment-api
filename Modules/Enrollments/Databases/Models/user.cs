using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using etest_enrollment_api.Modules.Commons.Models;
using SeventyOneDev.Utilities.Attributes;

namespace etest_enrollment_api.Modules.Enrollments.Databases.Models
{
  public class user : base_table
  {
    [Key]
    public Guid? user_uid {get;set;}
    public Guid? candidate_type_uid {get;set;}
    public string examinee_card {get;set;}
    public string student_code {get;set;}
    public string full_name {get;set;}
    public Guid? faculty_uid {get;set;}
    public Guid? department_uid {get;set;}
    // public short? institution_id {get;set;}
    public string mobile_phone {get;set;}
    public string email {get;set;}
    public string password {get;set;}
    
    public string photo1_name {get;set;}
    public string photo1_url {get;set;}
    public string photo2_name {get;set;}
    public string photo2_url {get;set;}
    public Boolean? user_approve_pdpa {get;set;}
  }

  public class t_user : user
  {
    public t_user()
    {
    }

    public t_user(Guid? userUid, Guid? candidateTypeUid, string examineeCard,
      string studentCode, string fullName, string lastName, short? facultyId,
      short? departmentId ,short? institution_id ,string mobilePhone, string email,
      string password)
    {
      this.user_uid = userUid;
      this.candidate_type_uid = candidateTypeUid;
      this.examinee_card = examineeCard;
      this.student_code = studentCode;
      this.full_name = fullName;
      this.mobile_phone =mobilePhone;
      this.email = email;
      this.password = password;
      this.created_by = "SYSTEM";
      this.created_datetime = DateTime.Now;
      this.updated_by = "SYSTEM";
      this.updated_datetime = DateTime.Now;
    }
  }
  public class v_user : user
  {
    public Boolean? is_register {get;set;}
    [NotMapped]
    public string search { get; set; }
  }
}
