using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using etest_enrollment_api.Modules.Commons.Models;
using SeventyOneDev.Utilities.Attributes;

namespace etest_enrollment_api.Modules.Enrollments.Databases.Models

{public class user_testing_subject_selected:base_table
  {
    [Key]
    public Guid? user_testing_subject_selected_uid {get;set;}
    public Guid? subject_uid {get;set;}
    public DateTime? testing_datetime {get;set;}
    public short? period_id { get; set; }
    public Guid? branch_uid {get;set;}
    public Guid? testing_field_uid {get;set;}
    public Guid? testing_center_uid {get;set;}
    public string testing_seat {get;set;}
    public string testing_room_name {get;set;}
   public Guid? user_testing_register_uid {get;set;}
   public Boolean? room_status {get;set;}
    public Boolean? is_cancel {get;set;}
    public Boolean? is_checked {get;set;}
    public string room_remark {get;set;}
    public Boolean? testing_register_approved {get;set;}
  }

  public class t_user_testing_subject_selected : user_testing_subject_selected
  {
    [JsonIgnore]
    public t_user_testing_register user_testing_register { get; set; }
  }
  public class v_user_testing_subject_selected : user_testing_subject_selected
  {
    [NotMapped]
    public string search { get; set; }
    public TimeSpan? start_time {get;set;}
    public string testing_room_name_th {get;set;}
    public Guid? testing_information_uid {get;set;}
    public string testing_room_code {get;set;}
    public string testing_field_code {get;set;}
    public string testing_field_name_th {get;set;}
    public string branch_code {get;set;}
    public string branch_name_th {get;set;}
    public string student_code {get;set;}
    public string examinee_card {get;set;}
    public string full_name {get;set;}
    public string academic_year_code {get;set;}
    public string academic_semester_code {get;set;}
    public string testing_information_code {get;set;}
    public string testing_information_name {get;set;}
    public DateTime? start_datetime {get;set;}
    public DateTime? end_datetime {get;set;}
    public DateTime? registration_start_datetime {get;set;}
    public DateTime? registration_end_datetime {get;set;}
    public DateTime? testing_start_datetime {get;set;}
    public DateTime? testing_end_datetime {get;set;}
    public string email { get; set; }
    public string examinee_mobile { get; set; }
    public Guid? candidate_type_uid { get; set; }
    public string photo1_name {get;set;}
    public string photo1_url {get;set;}
    public string photo2_name {get;set;}
    public string photo2_url {get;set;}
    public DateTime? payment_datetime {get;set;}
    public int? amount {get;set;}
    [JsonIgnore]
    public v_user_testing_register user_testing_register { get; set; }
  }

}
