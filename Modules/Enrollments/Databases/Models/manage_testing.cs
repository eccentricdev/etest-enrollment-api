using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using etest_enrollment_api.Modules.Commons.Models;
using SeventyOneDev.Utilities.Attributes;

namespace etest_enrollment_api.Modules.Enrollments.Databases.Models
{
  public class manage_testing:base_table
  {
    [Key]
    public Guid? manage_testing_uid {get;set;}
    public Guid? user_uid {get;set;}
    public Guid? user_testing_register_uid {get;set;}
    public Guid? user_testing_subject_selected_uid {get;set;}
    public Guid? testing_information_uid {get;set;}
    public Guid? branch_uid {get;set;}
    public Guid? testing_field_uid {get;set;}
    public Guid? testing_center_uid {get;set;}
    public Guid? subject_uid {get;set;}
    public DateTime? testing_datetime {get;set;}
    public short? period_id { get; set; }    
    public DateTime? new_testing_datetime {get;set;}
    public short? new_period_id { get; set; }    
    public string reason { get; set; }
    public string manage_testing_file {get;set;}
    public Boolean? is_cancel {get;set;}
    public short? seat_status_id { get; set; }

  }

  public class t_manage_testing : manage_testing
  {

  }

   public class v_manage_testing : manage_testing
  {
    public string student_code { get; set; }
    public string full_name { get; set; }
    public string branch_name_th { get; set; }
    public string testing_field_name_th { get; set; }
    public string testing_information_name { get; set; }
    public string testing_information_code { get; set; }
    public string testing_room_code { get; set; }
    public string testing_room_name_th { get; set; }
    public string subject_code { get; set; }
    public string subject_name_th { get; set; }
    public TimeSpan? start_time { get; set; }
    public TimeSpan? new_start_time { get; set; }
    public string seat_no { get; set; }

    [NotMapped] public string seat_status_name { get; set; }
    [NotMapped] public string search { get; set; }
  }
}
