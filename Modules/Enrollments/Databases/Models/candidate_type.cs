using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using etest_enrollment_api.Modules.Commons.Models;
using SeventyOneDev.Utilities.Attributes;

namespace etest_enrollment_api.Modules.Enrollments.Databases.Models
{
  public class candidate_type : base_table
  {
    [Key]
    public Guid? candidate_type_uid {get;set;}
    public string candidate_type_code {get;set;}
    [Search]
    public string candidate_type_name_th {get;set;}
    [Search]
    public string candidate_type_name_en {get;set;}
  }

  public class t_candidate_type : candidate_type
  {
    public t_candidate_type()
    {
    }

    public t_candidate_type(Guid? candidateTypeUid, string candidateTypeCode, string candidateTypeNameEn,
      string candidateTypeNameTh)
    {
      this.candidate_type_uid = candidateTypeUid;
      this.candidate_type_code = candidateTypeCode;
      this.candidate_type_name_en = candidateTypeNameEn;
      this.candidate_type_name_th = candidateTypeNameTh;
      this.created_by = "SYSTEM";
      this.created_datetime = DateTime.Now;
      this.updated_by = "SYSTEM";
      this.updated_datetime = DateTime.Now;
    }
  }
  public class v_candidate_type : candidate_type
  {
    [NotMapped]
    public string search { get; set; }
  }
}
