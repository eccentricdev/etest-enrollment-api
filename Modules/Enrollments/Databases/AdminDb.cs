using Microsoft.EntityFrameworkCore;
using etest_enrollment_api.Modules.Enrollments.Databases.AdminModels;

namespace etest_enrollment_api.Databases
{
  public partial class AdminDb : DbContext
  {
    public DbSet<t_testing_information> t_testing_information { get; set; }
    public DbSet<v_testing_information> v_testing_information { get; set; }
    public DbSet<t_testing_calendar> t_testing_calendar { get; set; }
    public DbSet<v_testing_calendar> v_testing_calendar { get; set; }
    public DbSet<t_testing_subject> t_testing_subject { get; set; }
    public DbSet<v_testing_subject> v_testing_subject { get; set; }
    public DbSet<t_testing_room> t_testing_room { get; set; }
    public DbSet<v_testing_room> v_testing_room { get; set; }
    public DbSet<t_testing_period> t_testing_period { get; set; }
    public DbSet<v_testing_period> v_testing_period { get; set; }
    public DbSet<t_testing_seat> t_testing_seat { get; set; }
    public DbSet<v_testing_seat> v_testing_seat { get; set; }
    public DbSet<t_testing_seat_subject> t_testing_seat_subject { get; set; }
    public DbSet<v_testing_seat_subject> v_testing_seat_subject { get; set; }
    public DbSet<t_testing_register> t_testing_register { get; set; }
    public DbSet<v_testing_register> v_testing_register { get; set; }
    public DbSet<t_testing_subject_selected> t_testing_subject_selected { get; set; }
    public DbSet<v_testing_subject_selected> v_testing_subject_selected { get; set; }
    public DbSet<t_testing_center> t_testing_center { get; set; }
    public DbSet<v_testing_center> v_testing_center { get; set; }
    public DbSet<t_subject> t_subject { get; set; }
    public DbSet<v_subject> v_subject { get; set; }
    public DbSet<t_testing_center_period> t_testing_center_period { get; set; }
    public DbSet<v_testing_center_period> v_testing_center_period { get; set; }
    public DbSet<t_testing_center_row> t_testing_center_row { get; set; }
    public DbSet<v_testing_center_row> v_testing_center_row { get; set; }
    public DbSet<t_department> t_department { get; set; }
    public DbSet<v_department> v_department { get; set; }
    public DbSet<t_faculty> t_faculty { get; set; }
    public DbSet<v_faculty> v_faculty { get; set; }
    
    public DbSet<t_branch> t_branch { get; set; }
    public DbSet<v_branch> v_branch { get; set; }
    
    public DbSet<t_testing_field> t_testing_field { get; set; }
    public DbSet<v_testing_field> v_testing_field { get; set; }
    
    public DbSet<t_academic_semester> t_academic_semester { get; set; }
    public DbSet<v_academic_semester> v_academic_semester { get; set; }
    public DbSet<t_academic_year> t_academic_year { get; set; }
    public DbSet<v_academic_year> v_academic_year { get; set; }
    
    public DbSet<t_student> t_student { get; set; }
    public DbSet<v_student> v_student { get; set; }
    
    public DbSet<t_ru_import_student> t_ru_import_student { get; set; }

 
    private void OnAdminModelCreating(ModelBuilder builder,string _schema)
    {
      builder.Entity<t_testing_information>().ToTable("testing_information", _schema);
      builder.Entity<v_testing_information>().ToView("v_testing_information", _schema);

      builder.Entity<t_testing_calendar>().ToTable("testing_calendar", _schema);
      builder.Entity<t_testing_calendar>().HasOne(p => p.testing_information).WithMany(p => p.testing_calendars)
        .HasForeignKey(p => p.testing_information_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_testing_calendar>().ToView("v_testing_calendar", _schema);
      builder.Entity<v_testing_calendar>().HasOne(p => p.testing_information).WithMany(p => p.testing_calendars)
        .HasForeignKey(p => p.testing_information_uid).OnDelete(DeleteBehavior.Cascade);
      
      builder.Entity<t_testing_subject>().ToTable("testing_subject", _schema);
      builder.Entity<t_testing_subject>().HasOne(p => p.testing_information).WithMany(p => p.testing_subjects)
        .HasForeignKey(p => p.testing_information_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_testing_subject>().ToView("v_testing_subject", _schema);
      builder.Entity<v_testing_subject>().HasOne(p => p.testing_information).WithMany(p => p.testing_subjects)
        .HasForeignKey(p => p.testing_information_uid).OnDelete(DeleteBehavior.Cascade);
      
      builder.Entity<t_testing_room>().ToTable("testing_room", _schema);
      builder.Entity<t_testing_room>().HasOne(p => p.testing_information).WithMany(p => p.testing_rooms)
        .HasForeignKey(p => p.testing_information_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_testing_room>().ToView("v_testing_room", _schema);
      builder.Entity<v_testing_room>().HasOne(p => p.testing_information).WithMany(p => p.testing_rooms)
        .HasForeignKey(p => p.testing_information_uid).OnDelete(DeleteBehavior.Cascade);
      
      builder.Entity<t_testing_seat>().ToTable("testing_seat", _schema);
      builder.Entity<t_testing_seat>().HasOne(p => p.testing_room).WithMany(p => p.testing_seats)
        .HasForeignKey(p => p.testing_room_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_testing_seat>().ToView("v_testing_seat", _schema);
      builder.Entity<v_testing_seat>().HasOne(p => p.testing_room).WithMany(p => p.testing_seats)
        .HasForeignKey(p => p.testing_room_uid).OnDelete(DeleteBehavior.Cascade);

      builder.Entity<t_testing_seat_subject>().ToTable("testing_seat_subject", _schema);
      builder.Entity<t_testing_seat_subject>().HasOne(p => p.testing_seat).WithMany(p => p.testing_seat_subjects)
        .HasForeignKey(p => p.testing_seat_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_testing_seat_subject>().ToView("v_testing_seat_subject", _schema);
      builder.Entity<v_testing_seat_subject>().HasOne(p => p.testing_seat).WithMany(p => p.testing_seat_subjects)
        .HasForeignKey(p => p.testing_seat_uid).OnDelete(DeleteBehavior.Cascade);
      
      builder.Entity<t_testing_register>().ToTable("testing_register", _schema);
      builder.Entity<v_testing_register>().ToView("v_testing_register", _schema);

      builder.Entity<t_testing_subject_selected>().ToTable("testing_subject_selected", _schema);
      builder.Entity<t_testing_subject_selected>().HasOne(p => p.testing_register).WithMany(p => p.testing_subject_selecteds)
        .HasForeignKey(p => p.testing_register_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_testing_subject_selected>().ToView("v_testing_subject_selected", _schema);
      builder.Entity<v_testing_subject_selected>().HasOne(p => p.testing_register).WithMany(p => p.testing_subject_selecteds)
        .HasForeignKey(p => p.testing_register_uid).OnDelete(DeleteBehavior.Cascade);
      
      builder.Entity<t_testing_center>().ToTable("testing_center", _schema);
      builder.Entity<v_testing_center>().ToView("v_testing_center", _schema);

      builder.Entity<t_subject>().ToTable("subject", _schema);
      builder.Entity<v_subject>().ToView("v_subject", _schema);
      
      builder.Entity<t_testing_center_period>().ToTable("testing_center_period", _schema);
      // builder.Entity<t_testing_center_period>().HasOne(p => p.testing_center).WithMany(p => p.testing_center_periods)
      //   .HasForeignKey(p => p.testing_center_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_testing_center_period>().ToView("v_testing_center_period", _schema);
      // builder.Entity<v_testing_center_period>().HasOne(p => p.testing_center).WithMany(p => p.testing_center_periods)
      //   .HasForeignKey(p => p.testing_center_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<t_testing_center_row>().ToTable("testing_center_row", _schema);
      builder.Entity<t_testing_center_row>().HasOne(p => p.testing_center).WithMany(p => p.testing_center_rows)
        .HasForeignKey(p => p.testing_center_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_testing_center_row>().ToView("v_testing_center_row", _schema);
      builder.Entity<v_testing_center_row>().HasOne(p => p.testing_center).WithMany(p => p.testing_center_rows)
        .HasForeignKey(p => p.testing_center_uid).OnDelete(DeleteBehavior.Cascade);
      
      builder.Entity<t_department>().ToTable("department", _schema);
      builder.Entity<v_department>().ToView("v_department", _schema);
      
      builder.Entity<t_faculty>().ToTable("faculty",_schema);
      builder.Entity<v_faculty>().ToView("v_faculty", _schema);
      
      builder.Entity<t_branch>().ToTable("branch", _schema);
      builder.Entity<v_branch>().ToView("v_branch", _schema);
      
      builder.Entity<t_testing_field>().ToTable("testing_field", _schema);
      builder.Entity<v_testing_field>().ToView("v_testing_field", _schema);
      
      builder.Entity<t_academic_semester>().ToTable("academic_semester", _schema);
      builder.Entity<v_academic_semester>().ToView("v_academic_semester", _schema);
      builder.Entity<t_academic_year>().ToTable("academic_year", _schema);
      builder.Entity<v_academic_year>().ToView("v_academic_year", _schema);
      
      builder.Entity<t_testing_period>().ToTable("testing_period", _schema);
      builder.Entity<t_testing_period>().HasOne(p => p.testing_information).WithMany(p => p.testing_periods)
        .HasForeignKey(p => p.testing_information_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_testing_period>().ToView("v_testing_period", _schema);
      builder.Entity<v_testing_period>().HasOne(p => p.testing_information).WithMany(p => p.testing_periods)
        .HasForeignKey(p => p.testing_information_uid).OnDelete(DeleteBehavior.Cascade);
      
      builder.Entity<t_student>().ToTable("student", _schema);
      builder.Entity<v_student>().ToView("v_student", _schema);
      
      builder.Entity<t_ru_import_student>().ToTable("ru_import_student", _schema);

    }
  }
}
