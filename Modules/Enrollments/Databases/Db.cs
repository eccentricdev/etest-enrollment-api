using Microsoft.EntityFrameworkCore;
using etest_enrollment_api.Databases.Models;
using etest_enrollment_api.Modules.Enrollments.Databases.Models;

namespace etest_enrollment_api.Databases
{
  public partial class Db : DbContext
  {
    public DbSet<t_candidate_type> t_candidate_type { get; set; }
    public DbSet<v_candidate_type> v_candidate_type { get; set; }
    public DbSet<t_user> t_user { get; set; }
    public DbSet<v_user> v_user { get; set; }
     public DbSet<t_user_testing_register> t_user_testing_register { get; set; }
    public DbSet<v_user_testing_register> v_user_testing_register { get; set; }
    public DbSet<t_user_testing_subject_selected> t_user_testing_subject_selected { get; set; }
    public DbSet<v_user_testing_subject_selected> v_user_testing_subject_selected { get; set; }
    public DbSet<t_manage_testing> t_manage_testing { get; set; }
    public DbSet<v_manage_testing> v_manage_testing { get; set; }

    private void OnEnrollmentModelCreating(ModelBuilder builder,string _schema)
    {
      builder.Entity<t_candidate_type>().ToTable("candidate_type", _schema);
      builder.Entity<v_candidate_type>().ToView("v_candidate_type", _schema);

      builder.Entity<t_user>().ToTable("user", _schema);
      builder.Entity<v_user>().ToView("v_user", _schema);

      builder.Entity<t_user_testing_register>().ToTable("user_testing_register", _schema);
      builder.Entity<v_user_testing_register>().ToView("v_user_testing_register", _schema);

      builder.Entity<t_user_testing_subject_selected>().ToTable("user_testing_subject_selected", _schema);
      builder.Entity<t_user_testing_subject_selected>().HasOne(p => p.user_testing_register).WithMany(p => p.user_testing_subject_selecteds)
        .HasForeignKey(p => p.user_testing_register_uid).OnDelete(DeleteBehavior.Cascade);
      builder.Entity<v_user_testing_subject_selected>().ToView("v_user_testing_subject_selected", _schema);
      builder.Entity<v_user_testing_subject_selected>().HasOne(p => p.user_testing_register).WithMany(p => p.user_testing_subject_selecteds)
        .HasForeignKey(p => p.user_testing_register_uid).OnDelete(DeleteBehavior.Cascade);
      
      builder.Entity<t_manage_testing>().ToTable("manage_testing", _schema);
      builder.Entity<v_manage_testing>().ToView("v_manage_testing", _schema);
    }
  }
}
