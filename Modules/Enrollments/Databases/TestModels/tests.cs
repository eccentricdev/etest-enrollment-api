using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace etest_enrollment_api.Modules.Enrollments.Databases.TestModels
{
  public class tests
  {
    [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid? tests_uid { get; set; }

        // public decimal? candidate_score { get; set; }
        public decimal? total_score { get; set; }

        public decimal? score { get; set; }
        public decimal? score_percent { get; set; }
        public decimal? written_score { get; set; }
        public int? total_test_time { get; set; }
        public int? test_status_id { get; set; }
        public TimeSpan? test_start_time { get; set; }
        public TimeSpan? test_end_time { get; set; }
        public DateTime? test_start_date { get; set; }
        public DateTime? test_end_date { get; set; }


        public Guid? testing_subject_selected_uid { get; set; }
        //public Guid? candidate_uid { get; set; }

        public string grade { get; set; }


        public string candidate_img { get; set; }

        public string remark { get; set; }

        public int? time_use { get; set; }

        public TimeSpan? end_time { get; set; }

        public string pwd { get; set; }

        public int? total_question { get; set; }
        public string candidate_img_1 { get; set; }
        public string candidate_img_2 { get; set; }
        public string document_file_url { get; set; }
        public string document_file_name { get; set; }
        public DateTime? create_date { get; set; }
        public TimeSpan? create_time { get; set; }
        public string grading_code { get; set; }
        public string random_question_code { get; set; }
        public bool? document_approved { get; set; }
        public string create_by { get; set; }
        public Guid? testing_information_uid { get; set; }
        public Guid? subject_uid { get; set; }
    }

    public class t_tests : tests
    {
    }

    public class v_tests : tests
    {
        public string student_code { get; set; }
        public Guid? candidate_type_uid { get; set; }
        public short? period_id { get; set; }
        public DateTime? test_date { get; set; }


        public Guid? testing_field_uid { get; set; }
        public Guid? branch_uid { get; set; }
        public string seat_no { get; set; }
        public Guid? testing_center_uid { get; set; }
        public TimeSpan? start_time { get; set; }

        public string citizen_id { get; set; }

        /// <summary>
        /// //
        /// </summary>
        public string branch_name_th { get; set; }

        public string location_name_th { get; set; }
        public string location_name_en { get; set; }
        public string location_room_name_th { get; set; }
        public string location_room_name_en { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public DateTime? from_date { get; set; }
        public DateTime? end_date { get; set; }
        public string test_status_code { get; set; }
        public string test_status_name { get; set; }
        public string candidate_name { get; set; }
        public string subject_information_name_th { get; set; }
        public string subject_information_name_en { get; set; }
        public string subject_information_code { get; set; }

        public string testing_information_name { get; set; }
        public string testing_information_code { get; set; }
        public string academic_semester_code { get; set; }
        public string academic_year_code { get; set; }
        public string display_time_use { get; set; }
        public string time_to_end { get; set; }
        public string real_end_time { get; set; }
        public string exten_time { get; set; }

        [NotMapped] public int? total_correct { get; set; }
        [NotMapped] public int? total_not_correct { get; set; }
        [NotMapped] public int? total_flag { get; set; }
        [NotMapped] public string search { get; set; }

        [NotMapped] public string time_minute_use { get; set; }

        [NotMapped] public int? display_time { get; set; }

    }
}