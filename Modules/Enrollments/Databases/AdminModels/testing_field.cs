using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using etest_enrollment_api.Modules.Commons.Models;

namespace etest_enrollment_api.Modules.Enrollments.Databases.AdminModels
{
    public class testing_field :base_table
    {
        [Key]
        public Guid? testing_field_uid {get;set;}
        public Guid? branch_uid {get;set;}
        public string testing_field_name_th {get;set;}
        public string testing_field_name_en { get; set;}
    }
    
    public class t_testing_field : testing_field
    {
    }
    public class v_testing_field : testing_field
    {
        [NotMapped]
        public string search { get; set; }
    }
}