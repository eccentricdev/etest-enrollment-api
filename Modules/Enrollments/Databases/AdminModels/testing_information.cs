using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using etest_enrollment_api.Modules.Commons.Models;

namespace etest_enrollment_api.Modules.Enrollments.Databases.AdminModels
{
  public class testing_information:base_table
  {
    [Key]
    public Guid? testing_information_uid {get;set;}
    public string testing_information_code {get;set;}
    public string testing_information_name {get;set;}
    public string academic_semester_code {get;set;}
    public string academic_year_code {get;set;}
    [MaxLength(50)]
    public string testing_information_status {get;set;}
    [MaxLength(200)]
    public string type_of_examinee {get;set;}
    public Boolean? is_student_code {get;set;}
    public string first_student_code {get;set;}
    public string last_student_code {get;set;}
    public DateTime? registration_start_datetime { get; set; }
    public DateTime? registration_end_datetime { get; set; }
    public DateTime? testing_start_datetime { get; set; }
    public DateTime? testing_end_datetime { get; set; }
    public int? number_of_times_testing { get; set; }
    public int? times_the_score_is_calculated { get; set; }
    public Boolean? is_online_register { get; set; }
    public Boolean? is_main { get; set; }
    public Guid? main_testing_information_uid {get;set;}
    public int? registration_type {get;set;}
    public Boolean? is_upload_document {get;set;}
    public Boolean? is_register_verify {get;set;}
    public Boolean? is_register {get;set;}
    public Boolean? is_counter {get;set;}
    
  }

  public class t_testing_information : testing_information
  {
    public List<t_testing_calendar> testing_calendars { get; set; }
    public List<t_testing_subject> testing_subjects { get; set; }
    public List<t_testing_room> testing_rooms { get; set; }
    public List<t_testing_period> testing_periods { get; set; }
  }

  public class v_testing_information : testing_information
  {
    [NotMapped]
    public string search { get; set; }
    public List<v_testing_calendar> testing_calendars { get; set; }
    public List<v_testing_subject> testing_subjects { get; set; }
    public List<v_testing_room> testing_rooms { get; set; }
    public List<v_testing_period> testing_periods { get; set; }
  }
}
