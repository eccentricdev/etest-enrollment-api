using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace etest_enrollment_api.Modules.Enrollments.Databases.AdminModels
{
    public class student
    {
        [Key]
        public Guid? student_uid {get;set;}
        public string academic_semester {get;set;}
        public string academic_year {get;set;}
        public string student_code {get;set;}
        public string student_name_th {get;set;}
        public string student_name_en {get;set;}
        public string testing_information_code {get;set;}
        public string register_subject_code {get;set;}
        public DateTime? testing_date {get;set;}
        public int? section {get;set;}
        public string branch_code {get;set;}
        public string testing_field_code {get;set;}
        public string testing_room_code {get;set;}
        public List<string> subject_registers { get; set; }
        [MaxLength(10000)]
        public string photo_url { get; set; }
        public Guid? testing_information_uid {get;set;}
        public int? registration_type {get;set;}
        public Boolean? is_generate_seat {get;set;}
        public Guid? testing_subject_selected_uid {get;set;}
        public Boolean? is_register {get;set;}
        public string id_card {get;set;}
        [NotMapped]
        public string no {get;set;}
    }
    
    public class t_student : student
    {
    }
    public class v_student : student
    {
        public Guid? branch_uid {get;set;}
        public string branch_name_th {get;set;}
        public Guid? testing_field_uid {get;set;}
        public string testing_field_name_th {get;set;}
        public Guid? testing_center_uid {get;set;}
        public string testing_room_name_th {get;set;}
        
        public Guid? subject_uid {get;set;}
        public string subject_name_th {get;set;}

        public string testing_seat {get;set;}
        public string testing_information_name {get;set;}
        [NotMapped]
        public string search { get; set; }
    }
}