using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using etest_enrollment_api.Modules.Commons.Models;

namespace etest_enrollment_api.Modules.Enrollments.Databases.AdminModels
{
  public class department:base_table
  {
    [Key]
    public Guid? department_uid {get;set;}
    public Guid? faculty_uid {get;set;}
    public string department_code {get;set;}
    public string department_name_th {get;set;}
    public string department_name_en {get;set;}
  }

  public class t_department : department
  {
  }
  public class v_department : department
  {
    public string faculty_name_th {get;set;}

    [NotMapped]
    public string search { get; set; }
  }
}
