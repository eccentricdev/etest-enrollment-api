using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using etest_enrollment_api.Modules.Commons.Models;

namespace etest_enrollment_api.Modules.Enrollments.Databases.AdminModels
{
  public class academic_year:base_table
  {
    [Key]
    public Guid? academic_year_uid {get;set;}
    public string academic_year_code {get;set;}
    public string academic_year_name_th {get;set;}
    public string academic_year_name_en {get;set;}
  }

  public class t_academic_year : academic_year
  {
  }
  public class v_academic_year : academic_year
  {
    [NotMapped]
    public string search { get; set; }
  }
}
