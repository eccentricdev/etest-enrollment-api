using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using etest_enrollment_api.Modules.Commons.Models;

namespace etest_enrollment_api.Modules.Enrollments.Databases.AdminModels
{
    public class testing_room : base_table
    {
        [Key]
        public Guid? testing_room_uid { get; set; }
        public Guid? branch_uid { get; set; }
        public Guid? testing_field_uid { get; set; }
        public Guid? testing_center_uid {get;set;}
        public string testing_auth_name { get; set; }
        public Boolean? is_random_seat { get; set; }
        public Guid? testing_information_uid {get;set;}
    }

    public class t_testing_room : testing_room
    {
        [JsonIgnore]
        public t_testing_information testing_information { get; set; }
        public List<t_testing_seat> testing_seats { get; set; }
    }
    public class v_testing_room : testing_room
    {
        public string branch_name_th {get;set;}
        public string testing_field_name_th {get;set;}        
        public string testing_room_name_th { get; set; }
        [NotMapped]
        public string search { get; set; }
        [JsonIgnore]
        public v_testing_information testing_information { get; set; }
        public List<v_testing_seat> testing_seats { get; set; }
    }

}
