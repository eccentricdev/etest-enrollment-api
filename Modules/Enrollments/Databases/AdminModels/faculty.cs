using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using etest_enrollment_api.Modules.Commons.Models;

namespace etest_enrollment_api.Modules.Enrollments.Databases.AdminModels
{
  public class faculty:base_table
  {
    [Key]
    public Guid? faculty_uid {get;set;}
    public string faculty_code {get;set;}
    public string faculty_name_th {get;set;}
    public string faculty_name_en {get;set;}
  }

  public class t_faculty : faculty
  {
  }
  public class v_faculty : faculty
  {
    [NotMapped]
    public string search { get; set; }
  }
}
