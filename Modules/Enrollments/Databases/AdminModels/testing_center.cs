using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using etest_enrollment_api.Modules.Commons.Models;

namespace etest_enrollment_api.Modules.Enrollments.Databases.AdminModels
{
  public class testing_center:base_table
  {
      [Key]
    public Guid? testing_center_uid {get;set;}
    public Guid? branch_uid {get;set;}
    public Guid? testing_field_uid {get;set;}
    public string testing_room_name_th {get;set;}
    public string testing_room_name_en {get;set;}
    public Guid? academic_year_uid { get; set; }
    public Guid? academic_semester_uid {get;set;}

  }

  public class t_testing_center : testing_center
  {
      // public List<t_testing_center_period> testing_center_periods { get; set; }
      public List<t_testing_center_row> testing_center_rows { get; set; }
  }
  public class v_testing_center : testing_center
  {
      public string branch_name_th {get;set;}
      public string testing_field_name_th {get;set;}
      public string academic_year_name_th {get;set;}
      public string academic_semester_name_th {get;set;}
      [NotMapped]
      public string search { get; set; }
      // public List<v_testing_center_period> testing_center_periods { get; set; }
      public List<v_testing_center_row> testing_center_rows { get; set; }
  }
}
