using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using etest_enrollment_api.Modules.Commons.Models;

namespace etest_enrollment_api.Modules.Enrollments.Databases.AdminModels
{
  public class testing_center_period:base_table
  {
    [Key]
    public Guid? testing_center_period_uid {get;set;}
    public Guid? testing_center_uid {get;set;}
    public short? period_id { get; set; }
    public TimeSpan? start_time { get; set; }
  }

  public class t_testing_center_period : testing_center_period
  {
    // [JsonIgnore]
    // public t_testing_center testing_center { get; set; }
  }
  public class v_testing_center_period : testing_center_period
  {
    [NotMapped]
    public string search { get; set; }
    // [JsonIgnore]
    // public v_testing_center testing_center { get; set; }
  }
}
