using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using etest_enrollment_api.Modules.Commons.Models;

namespace etest_enrollment_api.Modules.Enrollments.Databases.AdminModels
{
    public class testing_register:base_table
    {
        [Key]
        public Guid? testing_register_uid {get;set;}
        public string examinee_card {get;set;}
        public string student_code {get;set;}
        public string examinee_full_name {get;set;}
        public Guid? faculty_uid {get;set;}
        public Guid? department_uid {get;set;}
        public Guid? candidate_type_uid {get;set;}
        public string examinee_mobile {get;set;}
        public string email {get;set;}
        public string testing_fee {get;set;}
        public Guid? testing_information_uid {get;set;}
        public string payment_code {get;set;}
        public Boolean? is_cancel {get;set;}
        public DateTime? payment_datetime {get;set;}
        public string photo1_url { get; set; }
        public string photo1_name { get; set; }
        public string photo2_url { get; set; }
        public string photo2_name { get; set; }
        public string payment_method { get; set; }
        public string document_url { get; set; }
        public string document_name { get; set; }
        public string document_status { get; set; }
        public Boolean? is_from_upload {get;set;}
        public string counter_no {get;set;}
        public string counter_pin {get;set;}
        public int? counter_period {get;set;}

    }

    public class t_testing_register : testing_register
    {
        public List<t_testing_subject_selected> testing_subject_selecteds { get; set; }
    }

    public class v_testing_register : testing_register
    {
        [NotMapped]
        public string search { get; set; }
        public Guid? user_uid {get;set;}
        public int? registration_type {get;set;}
        public string faculty_code { get; set; }
        public string faculty_name_th { get; set; }
        public string faculty_name_en { get; set; }
        public string department_code { get; set; }
        public string department_name_th { get; set; }
        public string department_name_en { get; set; }
        public string candidate_type_code { get; set; }
        public string candidate_type_name_th { get; set; }
        public string candidate_type_name_en { get; set; }
        public List<v_testing_subject_selected> testing_subject_selecteds { get; set; }

    }
}
