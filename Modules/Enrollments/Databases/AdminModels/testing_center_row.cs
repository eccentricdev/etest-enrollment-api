using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using etest_enrollment_api.Modules.Commons.Models;

namespace etest_enrollment_api.Modules.Enrollments.Databases.AdminModels
{
  public class testing_center_row:base_table
  {
    [Key]
    public Guid? testing_center_row_uid {get;set;}
    public Guid? testing_center_uid {get;set;}
    public string row_name { get; set; }
    public short? no_of_seats { get; set; }
  }

  public class t_testing_center_row : testing_center_row
  {
    [JsonIgnore]
    public t_testing_center testing_center { get; set; }
  }
  public class v_testing_center_row : testing_center_row
  {
    [NotMapped]
    public string search { get; set; }
    [JsonIgnore]
    public v_testing_center testing_center { get; set; }
  }
}
