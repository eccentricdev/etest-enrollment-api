using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using etest_enrollment_api.Databases.Models;
using etest_enrollment_api.Modules.Commons.Models;

namespace etest_enrollment_api.Modules.Enrollments.Databases.AdminModels
{
  public class testing_calendar:base_table
  {
    [Key]
    public Guid? testing_calendar_uid {get;set;}
    public DateTime? start_datetime { get; set; }
    public DateTime? end_datetime { get; set; }
    public Guid? calendar_type_uid {get;set;}
    public Guid? testing_information_uid {get;set;}
  }

  public class t_testing_calendar : testing_calendar
  {
     [JsonIgnore]
      public t_testing_information testing_information { get; set; }
  }
  public class v_testing_calendar : testing_calendar
  {
    public string calendar_type_name_th {get;set;}
    [NotMapped]
    public string search { get; set; }
    [JsonIgnore]
    public v_testing_information testing_information { get; set; }
  }

}
