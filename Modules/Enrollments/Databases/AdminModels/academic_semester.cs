using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using etest_enrollment_api.Modules.Commons.Models;

namespace etest_enrollment_api.Modules.Enrollments.Databases.AdminModels
{
  public class academic_semester:base_table
  {
    [Key]
    public Guid? academic_semester_uid {get;set;}
    public string academic_semester_code {get;set;}
    public string academic_semester_name_th {get;set;}
    public string academic_semester_name_en {get;set;}
  }

  public class t_academic_semester : academic_semester
  {
  }
  public class v_academic_semester : academic_semester
  {
    [NotMapped]
    public string search { get; set; }
  }
}
