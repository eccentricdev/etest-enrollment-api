using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using etest_enrollment_api.Modules.Commons.Models;

namespace etest_enrollment_api.Modules.Enrollments.Databases.AdminModels
{
  public class testing_seat_subject:base_table
  {
    [Key]
    public Guid? testing_seat_subject_uid {get;set;}
    public Guid? subject_uid {get;set;}
    public short? start_seat_no {get;set;}
    public short? end_seat_no {get;set;}
    public Guid? testing_seat_uid {get;set;}
    
  }

  public class t_testing_seat_subject : testing_seat_subject
  {
    [JsonIgnore]
       public t_testing_seat testing_seat { get; set; }
  }
  public class v_testing_seat_subject : testing_seat_subject
  {
    public string subject_code {get;set;}
    public string subject_name_th {get;set;}
    [NotMapped]
    public string search { get; set; }
    [JsonIgnore]
    public v_testing_seat testing_seat { get; set; }
    
  }

}
