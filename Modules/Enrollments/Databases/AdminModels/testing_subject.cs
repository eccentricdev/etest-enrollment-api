using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using etest_enrollment_api.Modules.Commons.Models;

namespace etest_enrollment_api.Modules.Enrollments.Databases.AdminModels
{
  public class testing_subject:base_table
  {
    [Key]
    public Guid? testing_subject_uid {get;set;}
    public Guid? subject_uid {get;set;}
    public int? amount {get;set;}
    public int? random_status {get;set;}
    public int? grading_status {get;set;}
    public Guid? testing_information_uid {get;set;}
  }

  public class t_testing_subject : testing_subject
  {
    [JsonIgnore]
    public t_testing_information testing_information { get; set; }
  }
  public class v_testing_subject : testing_subject
  {
    public string subject_code {get;set;}
    public string subject_name_th {get;set;}
    //random
    public int? random_type {get;set;}
    public int? random_question_type {get;set;}
    public string random_question_code {get;set;}
    public int? total_set {get;set;}
    public int? total_question {get;set;}
    
    //grade
    public int? grading_characteristic {get;set;}
    public int? grading_type {get;set;}
    public int? announcement_type {get;set;}
    public DateTime? announcement_date {get;set;}
    public string grading_code {get;set;}
    
    public int? add_date_status {get;set;}
    
    [NotMapped]
    public string search { get; set; }
    [JsonIgnore]
    public v_testing_information testing_information { get; set; }
  }

}
