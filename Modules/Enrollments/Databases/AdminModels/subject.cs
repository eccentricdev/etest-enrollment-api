using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using etest_enrollment_api.Modules.Commons.Models;

namespace etest_enrollment_api.Modules.Enrollments.Databases.AdminModels
{
  public class subject:base_table
  {
    [Key]
    public Guid? subject_uid {get;set;}
    public string subject_code {get;set;}
    public string subject_name_th {get;set;}
    public string subject_name_en {get;set;}
    public Guid? education_type_uid {get;set;}
    public Guid? subject_type_uid {get;set;}
    public Guid? responsible_faculty_uid { get; set; }
    public string major_name_th { get; set; }
    public string major_name_en { get; set; }
    public string remark { get; set; }
    public string file_url { get; set; }
    public string file_name { get; set; }
  }

  public class t_subject : subject
  {
  }
  public class v_subject : subject
  {
    [NotMapped]
    public string search { get; set; }
  }
}
