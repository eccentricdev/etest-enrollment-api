using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using etest_enrollment_api.Modules.Commons.Models;

namespace etest_enrollment_api.Modules.Enrollments.Databases.AdminModels
{
  public class branch:base_table
  {
    [Key]
    public Guid? branch_uid {get;set;}
    public string branch_code {get;set;}
    public string branch_name_th {get;set;}
    public string branch_name_en {get;set;}
    public Guid? province_uid { get; set; }
    public Guid? region_uid {get;set;}
  }

  public class t_branch : branch
  {
  }
  public class v_branch : branch
  {
    [NotMapped]
    public string search { get; set; }
  }
}
