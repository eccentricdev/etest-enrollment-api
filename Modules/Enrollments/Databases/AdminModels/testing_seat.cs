using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using etest_enrollment_api.Modules.Commons.Models;

namespace etest_enrollment_api.Modules.Enrollments.Databases.AdminModels
{
  public class testing_seat:base_table
  {
    [Key]
    public Guid? testing_seat_uid {get;set;}
    public Guid? testing_center_row_uid {get;set;}
    public Boolean? is_specify_subject { get; set; }
    public Guid? testing_room_uid { get; set; }
    
  }

  public class t_testing_seat : testing_seat
  {
      [JsonIgnore]
      public t_testing_room testing_room { get; set; }
      public List<t_testing_seat_subject> testing_seat_subjects { get; set; }
  }
  public class v_testing_seat : testing_seat
  {
    public string row_name { get; set; }
    public short? no_of_seats { get; set; }
    [NotMapped]
    public string search { get; set; }
    [JsonIgnore]
    public v_testing_room testing_room { get; set; }
    public List<v_testing_seat_subject> testing_seat_subjects { get; set; }
  }

}
