using System.ComponentModel.DataAnnotations;

namespace etest_enrollment_api.Modules.Enrollments.Databases.AdminModels
{
  public class t_ru_import_student
  {
    [MaxLength(10),Key]
    public string student_code { get; set; }
    [MaxLength(2)]
    public string prefix { get; set; }
    [MaxLength(40)]
    public string student_name_th { get; set; }
    [MaxLength(30)]
    public string student_name_en { get; set; }

    public t_ru_import_student()
    {
    }

    public t_ru_import_student(string studentCode, string prefix, string studentNameTh, string studentNameEn)
    {
      student_code = studentCode;
      this.prefix = prefix;
      student_name_th = studentNameTh;
      student_name_en = studentNameEn;
    }
  }

}
