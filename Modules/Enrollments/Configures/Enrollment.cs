using Microsoft.Extensions.DependencyInjection;
using etest_enrollment_api.Modules.Commons.Services;
using etest_enrollment_api.Modules.Enrollments.Services;

namespace etest_enrollment_api.Modules.Enrollments.Configures
{
  public static class EnrollmentCollectionExtension
  {
    public static IServiceCollection AddEnrollmentServices(this IServiceCollection services)
    {
      services.AddScoped<StatusService>();
      services.AddScoped<CandidateTypeService>();
      services.AddScoped<UserService>();
      services.AddScoped<TestingRegisterService>();
      services.AddScoped<PaymentDetailService>();
      services.AddScoped<TestingManageService>();
      services.AddScoped<ResetPasswordService>();
      services.AddScoped<TestingResultService>();
      services.AddScoped<LoginService>();
      services.AddScoped<TestingScheduleService>();

      return services;
    }
  }
}
