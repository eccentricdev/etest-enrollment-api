using AutoMapper;
using etest_enrollment_api.Modules.Enrollments.Models;

namespace etest_enrollment_api.Modules.Enrollments
{
    public class TestingResultMap : Profile
    {

        public TestingResultMap()
        {
            CreateMap<testing_result_request, testing_result_respone>();


        }
    }
}