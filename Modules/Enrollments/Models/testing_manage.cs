using System;
using System.Collections.Generic;

namespace etest_enrollment_api.Modules.Enrollments.Models
{
  public class testing_register_manage
  {
     public Guid? user_uid {get;set;}
    public string examinee_card {get;set;}
    public Guid? user_testing_register_uid {get;set;}
    public string academic_semester_code {get;set;}
    public string academic_year_code {get;set;}
    public Guid? testing_information_uid {get;set;}
    public string testing_information_name {get;set;}
    public string testing_information_code {get;set;}
    public Boolean? is_pay_later {get;set;}
    public short? seat_status_id {get;set;}
    public string seat_status_name {get;set;}
    public Boolean is_admin {get;set;}
    public string remark { get; set; }
    // public List<testing_detail> testing_details { get; set; }
  }

  public class testing_subject_manage
  {
    public Guid? user_testing_register_uid {get;set;}
    public Guid? user_testing_subject_selected_uid {get;set;}
    public Guid? testing_information_uid {get;set;}
    public string testing_information_name {get;set;}
    public string testing_information_code {get;set;}
    public Guid? branch_uid {get;set;}
    public Guid? testing_field_uid {get;set;}
    public Guid? testing_center_uid {get;set;}
    public Guid? subject_uid {get;set;}
    public string subject_code {get;set;}
    public string subject_name_th {get;set;}
    public DateTime? testing_datetime {get;set;}
    public short? period_id { get; set; }
    public TimeSpan? start_time { get; set; }
    
  }

}
