namespace etest_enrollment_api.Modules.Enrollments.Models
{
  public class application_setting
  {
      public string temporary_path {get;set;}
      public string document_path {get;set;}
      public string document_url {get;set;}

      public string elastic_search {get;set;}
      public string report_url {get;set;}
  }
}
