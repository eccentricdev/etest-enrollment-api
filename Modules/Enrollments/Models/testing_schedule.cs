using System;

namespace etest_enrollment_api.Modules.Enrollments.Models
{
    public class testing_schedule
    {
        public Guid? user_testing_subject_selected_uid {get;set;}
        public Guid? subject_uid {get;set;}
        public string subject_code {get;set;}
        public string testing_datetime {get;set;}
        public Guid? branch_uid {get;set;}
        public string branch_name_th {get;set;}
        public Guid? testing_field_uid {get;set;}
        public string testing_field_name_th {get;set;}
        public Guid? testing_center_uid {get;set;}
        public string testing_room_name_th {get;set;}
        public string testing_seat {get;set;}
    }
}