namespace etest_enrollment_api.Modules.Enrollments.Models
{
    public class payment_callback
    {
        public int? status {get;set;}
        public string message {get;set;}
        public string Invoice {get;set;}
        public string redirect_url {get;set;}
        public string ref_code {get;set;}
        public string trans_id {get;set;}
    }
}