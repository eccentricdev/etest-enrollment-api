using System;
using System.ComponentModel.DataAnnotations;

namespace etest_enrollment_api.Modules.Enrollments.Models
{
    public class user_respone
    {
        public Guid? user_uid {get;set;}
        public Guid? candidate_type_uid {get;set;}
        public string candidate_type_name_th {get;set;}
        public string examinee_card {get;set;}
        public string student_code {get;set;}
        public string full_name {get;set;}
        public Guid? faculty_uid {get;set;}
        public Guid? department_uid {get;set;}
        public string faculty_name_th {get;set;}
        public string department_name_th {get;set;}
        public string mobile_phone {get;set;}
        public string email {get;set;}
        public string password {get;set;}
        public string photo1_name {get;set;}
        public string photo1_url {get;set;}
        public string photo2_name {get;set;}
        public string photo2_url {get;set;}
        public Boolean? is_register {get;set;}
        public Boolean? user_approve_pdpa {get;set;}
    }
}