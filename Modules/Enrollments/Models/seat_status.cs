using System.Collections.Generic;

public class SeatStatus
{
    public short id { get; set; }
    public string status_code {get;set;}
    public string description { get; set; }

    public SeatStatus(short id,string statusCode, string description)
    {
        this.id = id;
        this.status_code = statusCode;
        this.description = description;
    }

    public static class Status
    {
        public static SeatStatus Get(string code)
        {
            return code switch
            {
                "WAITCONFIRM" => new SeatStatus(1, "WAITCONFIRM", "รออนุมัติ"),
                "CONFIRMDATE" => new SeatStatus(2, "CONFIRMDATE", "ออกเลขที่นั่งสอบแล้ว"),
                "NOROOMAVAILABLE" => new SeatStatus(3, "NOROOMAVAILABLE", "ไม่มีห้องว่าง"),
                "WAITCONFIRMDATE" =>new SeatStatus(4, "WAITCONFIRMDATE", "รออนุมัติวันที่สอบใหม่"),
                "CONFIRM" => new SeatStatus(5, "CONFIRM", "อนุมัติ"),

            };
        }
    }
    
    public static class StatusId
    {
        public static SeatStatus Get(short? id)
        {
            return id switch
            {
                null => new SeatStatus(0, "NULL", "-"),
                1 => new SeatStatus(1, "WAITCONFIRM", "รออนุมัติ"),
                2 => new SeatStatus(2, "CONFIRMDATE", "ออกเลขที่นั่งสอบแล้ว"),
                3 => new SeatStatus(3, "NOROOMAVAILABLE", "ไม่มีห้องว่าง"),
                4 =>new SeatStatus(4, "WAITCONFIRMDATE", "รออนุมัติวันที่สอบใหม่"),
                5 =>new SeatStatus(5, "CONFIRM", "อนุมัติ")
                

            };
        }
    }
}
