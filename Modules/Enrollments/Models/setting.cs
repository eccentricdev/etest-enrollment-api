namespace etest_enrollment_api.Modules.Enrollments.Models
{
    public class Settings
    {

        public string smtp_server { get; set; }
        public string smtp_user_name { get; set; }
        public string smtp_password { get; set; }
        public int smtp_port { get; set; }
        public bool smtp_use_ssl { get; set; }
        public string document_path { get; set; }
        public string document_url { get; set; }
        public string folder_testing_manage { get; set; }
    }
}
