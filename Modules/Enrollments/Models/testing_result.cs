using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace etest_enrollment_api.Modules.Enrollments.Models
{
    public class testing_result_request
    {
        public Guid? user_uid {get;set;}
        public string academic_semester_code {get;set;}
        public string academic_year_code {get;set;}
    }
    
    public class testing_result_respone
    {
        public string examinee_card {get;set;}
        public Guid? testing_information_uid {get;set;}
        public string academic_semester_code {get;set;}
        public string academic_semester_name_th {get;set;}
        public string academic_year_name_th {get;set;}
        public string academic_year_code {get;set;}
        public Guid? subject_uid {get;set;}
        public string subject_code {get;set;}
        public string subject_name_th {get;set;}
        public int? credit {get;set;}
        public string grade {get;set;}
        [NotMapped]
        public string search { get; set; }
        
    }
}