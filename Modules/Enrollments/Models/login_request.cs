using System;

namespace etest_enrollment_api.Modules.Enrollments.Models
{
    public class login_request
    {
        public Guid? candidate_type_uid {get;set;}
        public string examinee_card {get;set;}
        public string student_code {get;set;}
        public string password {get;set;}
    }

    public class student_check
    {
        public Boolean? is_student {get;set;}
    }
}