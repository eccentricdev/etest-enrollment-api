using System;
using System.Collections.Generic;

namespace etest_enrollment_api.Modules.Enrollments.Models
{
  public class payment_detail
  {
    public Guid? user_uid {get;set;}
    public string examinee_card {get;set;}
    public Guid? user_testing_register_uid {get;set;}
    public string payment_code {get;set;}
    public string payment_name { get; set; }
    public string testing_fee {get;set;}
    public Boolean? is_pay_later {get;set;}
    public Guid? testing_information_uid {get;set;}
    public string testing_information_name {get;set;}
    public string testing_information_code {get;set;}
    public string academic_semester_code {get;set;}
    public string academic_year_code {get;set;}
    // public List<testing_detail> testing_details { get; set; }
  }

  public class testing_detail
  {
    public string testing_information_name {get;set;}
    public string testing_information_code {get;set;}
    public string subject_code {get;set;}
    public string subject_name_th {get;set;}
    public DateTime? testing_datetime {get;set;}
    public short? period_id { get; set; }
    public TimeSpan? start_time {get;set;}
    public string fee {get;set;}
    public string total_subject {get;set;}
    public string total_fee {get;set;}
    public string ref2 {get;set;}
      
  }

}
