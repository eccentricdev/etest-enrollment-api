using System;
using System.Collections.Generic;

namespace etest_enrollment_api.Modules.Enrollments.Models
{
    public class user_testing_register_respone
    {
        public Guid? user_testing_register_uid {get;set;}
        public Guid? user_uid {get;set;}
        public string full_name {get;set;}
        public string examinee_card {get;set;}
        public Guid? candidate_type_uid {get;set;}
        public string candidate_type_name_th {get;set;}
        public string examinee_mobile {get;set;}
        public Guid? testing_information_uid {get;set;}
        public string testing_information_name {get;set;}
        public string testing_information_code {get;set;}
        public string payment_code {get;set;}
        public string testing_fee {get;set;}
        public short? seat_status_id {get;set;}
        public string seat_status_name {get;set;}
        // public string total_subject {get;set;}
        public Boolean? is_cancel {get;set;}
        public Boolean? is_pay_later {get;set;}
        
        public DateTime? payment_datetime {get;set;}
        public string photo1_url { get; set; }
        public string photo1_name { get; set; }
        public string photo2_url { get; set; }
        public string photo2_name { get; set; }
        public List<user_testing_subject_selected_respone> user_testing_subject_selecteds { get; set; }
    }
}