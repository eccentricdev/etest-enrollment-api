using System;

namespace etest_enrollment_api.Modules.Enrollments.Models
{
  public class seat_detail
  {
    public Guid? subject_uid {get;set;}
    public string row_name { get; set; }
    public short? no_of_seats { get; set; }
    public short? start_seat_no {get;set;}
    public short? end_seat_no {get;set;}
    public Boolean? is_specify_subject { get; set; }
  }
}
