using System.Collections.Generic;
using etest_enrollment_api.Databases.Models;

namespace etest_enrollment_api.Modules.Commons.Initials
{
  internal class Status
  {
    public static IEnumerable<t_status> Get()
    {
      return new[]{
        new t_status(1,"A","Active","ใช้งาน"),
        new t_status(2,"I","Inactive","ไม่ใช้งาน")
      };
    }
  }
}
