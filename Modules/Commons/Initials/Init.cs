using System.Linq;
using etest_enrollment_api.Databases;

namespace etest_enrollment_api.Modules.Commons.Initials
{
  public static class CommonInit
  {
    public static void Run(Db context)
    {
      if (!context.t_status.Any())
      {
        context.t_status.AddRange(Status.Get());
        context.SaveChanges();
      }
    }
  }
}
