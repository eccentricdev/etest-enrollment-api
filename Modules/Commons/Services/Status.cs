using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SeventyOneDev.Utilities;
using etest_enrollment_api.Databases;
using etest_enrollment_api.Databases.Models;
using Db = etest_enrollment_api.Databases.Db;


namespace etest_enrollment_api.Modules.Commons.Services
{

  public class StatusService
  {
    readonly Db _context;

    public StatusService(Db context)
    {
      _context = context;
    }

    public async Task<List<v_status>> ListStatus(v_status status)
    {

      IQueryable < v_status > statuss = _context.v_status.AsNoTracking();


      if (status != null)
      {
        statuss = status.search != null ? statuss.Search(status.search) : statuss.Search(status);
      }
      statuss = statuss.OrderBy(c => c.status_id);
      return await statuss.ToListAsync();
    }

    public async Task<v_status> GetStatus(int id)
    {
      return await _context.v_status.AsNoTracking().FirstOrDefaultAsync(a => a.status_id == id);
    }

    public async Task<t_status> NewStatus(t_status status, ClaimsIdentity claimsIdentity)
    {

      await _context.t_status.AddAsync(status);
      await _context.SaveChangesAsync();
      return status;
    }

    public async Task<int> UpdateStatus(t_status status, ClaimsIdentity claimsIdentity)
    {

      _context.t_status.Update(status);
      return await _context.SaveChangesAsync();
    }

    public async Task<int> DeleteStatus(int id, ClaimsIdentity claimsIdentity)
    {
      var status = await _context.t_status.FirstOrDefaultAsync(c => c.status_id == id);
      _context.Remove(status);
      return await _context.SaveChangesAsync();

    }
  }


}
