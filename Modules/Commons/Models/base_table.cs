using System;

namespace etest_enrollment_api.Modules.Commons.Models
{
  public class base_table
  {
    public int? status_id { get; set; }
    public string created_by { get; set; }
    public DateTime? created_datetime { get; set; }
    public string updated_by { get; set; }
    public DateTime? updated_datetime { get; set; }
  }
}
