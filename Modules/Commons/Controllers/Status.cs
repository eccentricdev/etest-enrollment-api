using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SeventyOneDev.Utilities;
using etest_enrollment_api.Databases.Models;
using etest_enrollment_api.Modules.Commons.Services;

namespace etest_enrollment_api.Modules.Commons.Controllers
{
  public class StatusController : Controller
  {
    readonly StatusService _statusService;

    public StatusController(StatusService statusService)
    {
      this._statusService = statusService;
    }

    [HttpGet, Route("api/v1/common/statuss")]
    [ApiExplorerSettings(GroupName = "common")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<List<v_status>>> GetStatuss(
      [FromQuery] v_status _status = null)
    {
      if (EFExtension.IsNullOrEmpty(_status))
      {
        var listResult = await _statusService.ListStatus(null);
        return Ok(listResult);
      }
      else
      {
        var getResult = await _statusService.ListStatus(_status);
        if (getResult.Count() == 0)
        {
          return Ok(new List<v_status>());
        }
        else
        {
          return getResult.ToList();
        }

      }

    }

    [HttpGet, Route("api/v1/common/statuss/{status_id}")]
    [ApiExplorerSettings(GroupName = "common")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<v_status>> GetStatus([FromRoute] int status_id)
    {

      var getResult = await _statusService.GetStatus(status_id);
      if (getResult == null)
      {
        return NotFound();
      }
      else
      {
        return Ok(getResult);
      }
    }

    [HttpPost, Route("api/v1/common/statuss")]
    [ApiExplorerSettings(GroupName = "common")]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<t_status>> NewStatus(
      [FromBody] t_status _status)
    {
      try
      {
        var newResult =
          await _statusService.NewStatus(_status, User.Identity as ClaimsIdentity);
        return Created("/api/v1/common/statuss/" + newResult.status_id.ToString(), newResult);
      }
      // catch(DuplicateException ex)
      // {
      //     response = ApiHelper.ApiError<v_status>(ex.ErrorId,ex.Message);
      // }
      catch (Exception ex)
      {
        Console.WriteLine("Create status failed: " + ex.Message);
        return BadRequest();
      }

    }

    [HttpPut, Route("api/v1/common/statuss")]
    [ApiExplorerSettings(GroupName = "common")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult> UpdateStatus([FromBody] t_status _status)
    {
      try
      {
        var result =
          await _statusService.UpdateStatus(_status, User.Identity as ClaimsIdentity);
        return NoContent();
      }
      // catch(DuplicateException ex)
      // {
      //     response = ApiHelper.ApiError<v_status>(ex.ErrorId,ex.Message);
      // }
      catch (Exception ex)
      {
        Console.WriteLine("Update status failed: " + ex.Message);
        return BadRequest();
      }

    }

    [HttpDelete, Route("api/v1/common/statuss/{status_id}")]
    [ApiExplorerSettings(GroupName = "common")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult> DeleteStatus([FromRoute] int status_id)
    {
      try
      {
        var _deleteCount =
          await _statusService.DeleteStatus(status_id, User.Identity as ClaimsIdentity);
        if (_deleteCount == 1)
        {
          return NoContent();
        }
        else
        {
          return NotFound();
        }

      }
      // catch(DuplicateException ex)
      // {
      //     response = ApiHelper.ApiError<v_status>(ex.ErrorId,ex.Message);
      // }
      catch (Exception ex)
      {
        Console.WriteLine("Delete status failed: " + ex.Message);
        return BadRequest();
      }
    }
  }
}
