using Microsoft.EntityFrameworkCore;
using etest_enrollment_api.Databases.Models;

namespace etest_enrollment_api.Databases
{
  public partial class Db : DbContext
  {
    public DbSet<t_status> t_status { get; set; }
    public DbSet<v_status> v_status { get; set; }
    private void OnCommonModelCreating(ModelBuilder builder,string _schema)
    {
      builder.Entity<t_status>().ToTable("status", _schema);
      builder.Entity<v_status>().ToView("v_status", _schema);

    }
  }
}
