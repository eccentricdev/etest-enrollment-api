using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SeventyOneDev.Utilities.Attributes;

namespace etest_enrollment_api.Databases.Models
{
  public class status
  {
    [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public short? status_id {get;set;}
    public string status_code {get;set;}
    [Search]
    public string status_name_th {get;set;}
    [Search]
    public string status_name_en {get;set;}
    public string created_by { get; set; }
    public DateTime? created_datetime { get; set; }
    public string updated_by { get; set; }
    public DateTime? updated_datetime { get; set; }
  }

  public class t_status : status
  {
    public t_status()
    {
    }

    public t_status(short? statusId, string statusCode, string statusNameEn,
      string statusNameTh)
    {
      this.status_id = statusId;
      this.status_code = statusCode;
      this.status_name_en = statusNameEn;
      this.status_name_th = statusNameTh;
      this.created_by = "SYSTEM";
      this.created_datetime = DateTime.Now;
      this.updated_by = "SYSTEM";
      this.updated_datetime = DateTime.Now;
    }
  }
  public class v_status : status
  {
    [NotMapped]
    public string search { get; set; }
  }
}
