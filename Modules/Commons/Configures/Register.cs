using Microsoft.Extensions.DependencyInjection;
using etest_enrollment_api.Modules.Commons.Services;

namespace etest_enrollment_api.Modules.Commons.Configures
{
  public static class CommonCollectionExtension
  {
    public static IServiceCollection AddCommonServices(this IServiceCollection services)
    {
      services.AddScoped<StatusService>();

      return services;
    }
  }
}
